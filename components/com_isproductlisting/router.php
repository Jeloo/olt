<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
 + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
 ^
 + Project: 		IS Product listing
 ^
 */
defined('_JEXEC') or die('Restricted access');

function ISProductlistingBuildRoute(&$query) {
    $segments = array();
    $router = new isproductlistingRouter;
    if (isset($query['view'])) {
        $view = $query['view']; unset($query['view']);
    };
    if (isset($query['layout'])) {
        $value = $router->buildLayout($query['layout'], $view);
        $segments[] = $value;
        unset($query['layout']);
    };

    //task
    if (isset($query['task'])) {
        if (count($segments) == 0) $segments[] = 'tk';
        $segments[] = $query['task']; unset($query['task']);
    };
    // productid
    if (isset($query['pid'])) {
        $segments[] = "product-".$router->getProductTitle($query['pid'])."-".$query['pid']; unset($query['pid']);
    };
    // categoryid
    if (isset($query['catid'])) {
        $segments[] = "cat-".$router->getCategoryTitle($query['catid'])."-".$query['catid']; unset($query['catid']);
    };
    // subcategoryid
    if (isset($query['subcatid'])) {
        $segments[] = "subcat-".$router->getSubCategoryTitle($query['subcatid'])."-".$query['subcatid']; unset($query['subcatid']);
    };
    // call from
    if (isset($query['cl'])) {
        $segments[] = "nav-".$router->parseCL($query['cl']); unset($query['cl']);
    };
    // Gold Products
    if (isset($query['gp'])) {
        $segments[] = "gold-product"; unset($query['gp']);
    };
    // Featured Products
    if (isset($query['fp'])) {
        $segments[] = "featured-product"; unset($query['fp']);
    };
    if (isset($query['plink'])) {
        $segments[] = "plink" . "-" . $query['plink']; unset($query['plink']);
    };
    if (isset($query['Itemid'])) {
        $_SESSION['ITPlItemid'] = $query['Itemid'];
    }

    return $segments;
}

function ISProductlistingParseRoute($segments) {

    $vars = array();
    $count = count($segments);
    $router = new isproductlistingRouter;
    //echo '<br> count '.$count;print_r($segments);exit;
    if (isset($segments[0])) $layout = $segments[0];
    else $layout = "";
    
    if ($layout == 'tk') {
        $vars['task'] = $segments[1];
    } else {
        $lresult = $router->parseLayout($layout);
        $vars['view'] = $lresult["view"];
        $vars['layout'] = $lresult["layout"];
    }
    $i = 0;
    foreach ($segments AS $seg) {
        if ($i >= 1) {
            $array = explode(":", $seg);
            $index = $array[0];
            //unset the current index
            unset($array[0]);
            if (isset($array[1]))
                $value = implode("-", $array);

            switch ($index) {
                case "nav":$vars['cl'] = $router->parseCL($value); break;
                case "product":$vars['pid'] = $router->parseId($value); break;
                case "plink":$vars['plink'] = $router->parseId($value); break;
                case "cat":$vars['catid'] = $router->parseId($value); break;
                case "subcat":$vars['subcatid'] = $router->parseId($value); break;
                case "gold":$vars['gp'] = 1; break;
                case "featured":$vars['fp'] = 1; break;
            }
        }
        $i++;
    }

    if (isset($_SESSION['ITPlItemid'])) {
        $vars['Itemid'] = $_SESSION['ITPlItemid'];
    }
    return $vars;
}

class isproductlistingRouter {

    function buildLayout($value, $view) {
        $returnvalue = "";
        switch ($value) {
            case "controlpanel":$returnvalue = "control-panel"; break;
            case "listproduct":$returnvalue = "products-list"; break;
            case "viewproduct": $returnvalue = "product-detail"; break;
            case "compareproduct":$returnvalue = "comapre-products"; break;
            case "productcategories":$returnvalue = "product-categories"; break;
            case "productsubcategories":$returnvalue = "product-subcategories"; break;
            case "productbrands":$returnvalue = "product-brands"; break;
            case "productsearch":$returnvalue = "search-product"; break;
            case "productsearchresult":$returnvalue = "product-search-result"; break;
            case "mycart":$returnvalue = "my-cart"; break;
            case "shortlistproducts":$returnvalue = "shortlist-products";break;
            //rss
            case "products":$returnvalue = "rss-feed"; break;
        }
        return $returnvalue;
    }

    function parseLayout($value) {
        //$returnvalue = "";
        switch ($value) {
            case "control:panel":$returnvalue["layout"] = "controlpanel"; $returnvalue["view"] = "product";break;
            case "products:list":$returnvalue["layout"] = "listproduct"; $returnvalue["view"] = "product";break;
            case "product:detail": $returnvalue["layout"] = "viewproduct"; $returnvalue["view"] = "product";break;
            case "comapre:products":$returnvalue["layout"] = "compareproduct"; $returnvalue["view"] = "product";break;
            case "product:categories":$returnvalue["layout"] = "productcategories"; $returnvalue["view"] = "product";break;
            case "product:subcategories":$returnvalue["layout"] = "productsubcategories"; $returnvalue["view"] = "product";break;
            case "product:brands":$returnvalue["layout"] = "productbrands"; $returnvalue["view"] = "product";break;
            case "search:product":$returnvalue["layout"] = "productsearch"; $returnvalue["view"] = "product";break;
            case "product:search:result":$returnvalue["layout"] = "productsearchresult"; $returnvalue["view"] = "product";break;
            case "my:cart":$returnvalue["layout"] = "mycart"; $returnvalue["view"] = "product";break;
            case "shortlist:products":$returnvalue["layout"] = "shortlistproducts";$returnvalue["view"] = "product";break;
            //rss
            case "rss:feed":$returnvalue["layout"] = "products";$returnvalue["view"] = "rss";break;
        }
        
        return $returnvalue;
    }

    function buildCL($value) {
        $returnvalue = "";
        switch ($value) {
            case 1:$returnvalue = "products"; break;
            case 2:$returnvalue = "categories"; break;
            case 3:$returnvalue = "subcategories"; break;
            case 4:$returnvalue = "brands"; break;
            case 5:$returnvalue = "featured"; break;
            case 6:$returnvalue = "gold"; break;
        }
        return $returnvalue;
    }

    function parseCL($value) {
        $returnvalue = "";
        switch ($value) {
            case "products": $returnvalue = 1; break;
            case "categories":$returnvalue = 2; break;
            case "subcategories":$returnvalue = 3; break;
            case "brands":$returnvalue = 4; break;
            case "featured":$returnvalue = 5; break;
            case "gold":$returnvalue = 6; break;
            default :$returnvalue = $value; break;
        }
        return $returnvalue;
    }

    function getProductTitle($productid) {
        $db = JFactory::getDBO();
        $query = "SELECT REPLACE(LOWER(title),' ','-') AS alias FROM `#__isproductlisting_products` WHERE id = " . (int) $productid;
        $db->setQuery($query);
        $result = $db->loadResult();
        $result = str_replace('/', '', $result);
        return $result;
    }

    function getCategoryTitle($catid) {
        $db = JFactory::getDBO();
        $query = "SELECT REPLACE(LOWER(title),' ','-') AS alias FROM `#__isproductlisting_categories` WHERE id = " . (int) $catid;
        $db->setQuery($query);
        $result = $db->loadResult();
        $result = str_replace('/', '', $result);
        return $result;
    }

    function getSubCategoryTitle($subcatid) {
        $db = JFactory::getDBO();
        $query = "SELECT REPLACE(LOWER(title),' ','-') AS alias FROM `#__isproductlisting_subcategories` WHERE id = " . (int) $subcatid;
        $db->setQuery($query);
        $result = $db->loadResult();
        $result = str_replace('/', '', $result);
        return $result;
    }

    function parseId($value) {
        $id = explode("-", $value);
        $count = count($id);
        $id = (int) $id[($count - 1)];
        return $id;
    }
}
