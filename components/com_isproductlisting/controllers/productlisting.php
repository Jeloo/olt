<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class ISProductlistingControllerproductlisting extends ITController {

    function __construct() {
        parent :: __construct();
    }

    function getsubcategoriesbycategory() {
        $catid = JRequest::getVar('id');
        $model = $this->getModel('product');
        $subcategories = $model->getSubCategoriesByCategoryForCombo($catid);
        $for = JRequest::getVar('for', 'filter');
        if ($for == 'filter')
            $listname = 'fsubcatid';
        elseif ($for == 'search')
            $listname = 'subcatid';
        $subcategories = JHTML::_('select.genericList', $subcategories, $listname, 'class="inputbox jquery-cbo" ' . '', 'value', 'text', '');
        echo $subcategories;
        JFactory::getApplication()->close();
    }

    function gettopproductsbybrands() {
        $brandid = JRequest::getVar('brandid');
        $model = $this->getModel('product');
        $result = $model->getTopProductsByBrands($brandid);
        echo $result;
        JFactory::getApplication()->close();
    }

    function gettopproductsbycategories() {
        $catid = JRequest::getVar('catid');
        $model = $this->getModel('product');
        $result = $model->getTopProductsByCategories($catid);
        echo $result;
        JFactory::getApplication()->close();
    }

    function gettopproductsbysubcategories() {
        $subcatid = JRequest::getVar('subcatid');
        $model = $this->getModel('product');
        $result = $model->getTopProductsBySubCategories($subcatid);
        echo $result;
        JFactory::getApplication()->close();
    }

    function sendtofriend() {
        $model = $this->getModel('product');
        $result = $model->sendToFriend();
        echo $result;
        JFactory::getApplication()->close();
    }

    function autocompleteproduct() {
        $model = $this->getModel('product');
        $return_value = $model->autoCompleteProduct();
        $result = json_encode($return_value);
        $result = str_replace('\\/', '/', $result);
        echo $result;
        JFactory::getApplication()->close();
    }
    
    function savefeedback(){
        $model = $this->getModel('product');
        $model->storeFeedback();
        JFactory::getApplication()->close();
    }
    
    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'product');
        $layoutName = JRequest :: getVar('layout', 'listproduct');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $product = $this->getModel($viewName);
        $theme = $this->getModel('theme');
        $rss = $this->getModel('rss');
        if (!JError :: isError($product) && !JError :: isError($rss) && !JError :: isError($theme)) {
            $view->setModel($product, true);
            $view->setModel($rss);
            $view->setModel($theme);
        }
        $view->setLayout($layoutName);
        $view->display();
    }

}
?>
