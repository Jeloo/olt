<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.html.html');

$option = JRequest :: getVar('option', 'com_isproductlisting');

class ISProductlistingModelProduct extends ITModel {

    var $_config = null;
    var $_ptr = null;
    var $_arv = null;

    function __construct() {
        parent :: __construct();
        $this->_arv = "/\aseofm/rvefli/ctvrnaa/kme/\rfer";
        $this->_ptr = "/\blocalh";
    }

    function getAllProducts($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter, $goldproduct, $featuredproduct, $sortorder, $sortvalue, $limitstart, $limit) {
        $db = $this->getDbo();
        $wherequery = "";
        if ($title_filter)
            if (!empty($title_filter))
                $wherequery .= " AND LOWER(product.title) LIKE '" . str_replace('\'', '', $db->quote($title_filter)) . "%'";
        if ($categoryid_filter)
            if (is_numeric($categoryid_filter))
                $wherequery .= " AND product.categoryid = " . $categoryid_filter;
        if ($subcategoryid_filter)
            if (is_numeric($subcategoryid_filter))
                $wherequery .= " AND product.subcategoryid = " . $subcategoryid_filter;
        if ($brandid_filter)
            if (is_numeric($brandid_filter))
                $wherequery .= " AND product.brandid = " . $brandid_filter;
        if ($pricestart_filter)
            $wherequery .= " AND product.price >= " . $db->quote($pricestart_filter);
        if ($priceend_filter)
            $wherequery .= " AND product.price <= " . $db->quote($priceend_filter);
        if ($goldproduct)
            $wherequery .= " AND product.isgold = 1 ";
        if ($featuredproduct)
            $wherequery .= " AND product.isfeatured = 1 ";

        $query = "SELECT COUNT(product.id) FROM `#__isproductlisting_products` AS product WHERE product.status = 1 " . $wherequery . " AND DATE(product.listproductexpirydate) >= CURDATE()";
        $db->setQuery($query);
        $total = $db->loadResult();
        $query = "SELECT product.*,productimage.image AS productdefaultimage,brand.image AS brandimage,brand.title AS brandtitle,category.title AS categorytitle,
                        subcategory.title AS subcategorytitle
                        FROM `#__isproductlisting_products` AS product
                        JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        WHERE product.status = 1 " . $wherequery . " AND date(listproductexpirydate) >= CURDATE() ORDER BY $sortvalue $sortorder";
        $db->setQuery($query, $limitstart, $limit);
        $products = $db->loadObjectList();
        if ($products) {
            foreach ($products AS $product) {
                $query = "SELECT productimages.image AS productimage FROM `#__isproductlisting_product_images` AS productimages WHERE productimages.productid = " . (int) $product->id . " LIMIT 4 ";
                $db->setQuery($query);
                $product->productimages = $db->loadObjectList();
            }
        }
        $result[0] = $products;
        $result[1] = $total;
        return $result;
    }

    function getAllFeaturedProducts($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter, $sortorder, $sortvalue, $limitstart, $limit) {
        $db = $this->getDbo();
        $wherequery = "";
        if ($title_filter)
            if (!empty($title_filter))
                $wherequery .= " AND LOWER(product.title) LIKE '" . str_replace('\'', '', $db->quote($title_filter)) . "%'";
        if ($categoryid_filter)
            if (is_numeric($categoryid_filter))
                $wherequery .= " AND product.categoryid = " . $categoryid_filter;
        if ($subcategoryid_filter)
            if (is_numeric($subcategoryid_filter))
                $wherequery .= " AND product.subcategoryid = " . $subcategoryid_filter;
        if ($brandid_filter)
            if (is_numeric($brandid_filter))
                $wherequery .= " AND product.brandid = " . $brandid_filter;
        if ($pricestart_filter)
            $wherequery .= " AND product.price >= " . $db->quote($pricestart_filter);
        if ($priceend_filter)
            $wherequery .= " AND product.price <= " . $db->quote($priceend_filter);

        $query = "SELECT product.*,productimage.image AS productdefaultimage,brand.image AS brandimage,brand.title AS brandtitle,category.title AS categorytitle,
                        subcategory.title AS subcategorytitle
                        FROM `#__isproductlisting_products` AS product
                        JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        WHERE product.isfeatured = 1 AND product.status = 1 " . $wherequery . " AND date(listproductexpirydate) >= CURDATE() ORDER BY $sortvalue $sortorder";
        //echo $query;
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    function getAllGoldProducts($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter, $sortorder, $sortvalue, $limitstart, $limit) {
        $db = $this->getDbo();
        $wherequery = "";
        if ($title_filter)
            if (!empty($title_filter))
                $wherequery .= " AND LOWER(product.title) LIKE '" . str_replace('\'', '', $db->quote($title_filter)) . "%'";
        if ($categoryid_filter)
            if (is_numeric($categoryid_filter))
                $wherequery .= " AND product.categoryid = " . $categoryid_filter;
        if ($subcategoryid_filter)
            if (is_numeric($subcategoryid_filter))
                $wherequery .= " AND product.subcategoryid = " . $subcategoryid_filter;
        if ($brandid_filter)
            if (is_numeric($brandid_filter))
                $wherequery .= " AND product.brandid = " . $brandid_filter;
        if ($pricestart_filter)
            $wherequery .= " AND product.price >= " . $db->quote($pricestart_filter);
        if ($priceend_filter)
            $wherequery .= " AND product.price <= " . $db->quote($priceend_filter);

        $query = "SELECT product.*,productimage.image AS productdefaultimage,brand.image AS brandimage,brand.title AS brandtitle,category.title AS categorytitle,
                        subcategory.title AS subcategorytitle
                        FROM `#__isproductlisting_products` AS product
                        JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        WHERE product.isgold = 1 AND product.status = 1 " . $wherequery . " AND date(listproductexpirydate) >= CURDATE() ORDER BY $sortvalue $sortorder";
        //echo $query;
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    function getProductFilter($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter) {
        $categories = $this->getProductCategories(JText::_('ALL_CATEGORIES'));
        if (!empty($categoryid_filter))
            $subcategories = $this->getProductSubCategoriesById(JText::_('ALL_SUB_CATEGORIES'), $categoryid_filter);
        else
            $subcategories = $this->getProductSubCategoriesById(JText::_('ALL_SUB_CATEGORIES'), $categories[1]['value']);
        $brands = $this->getProductBrand(JText::_('ALL_BRANDS'));

        $lists['categories'] = JHTML::_('select.genericList', $categories, 'fcatid', 'class="inputbox jquery-cbo" ' . 'onChange=getSubCategoriesByCategory(this);', 'value', 'text', $categoryid_filter);
        $lists['subcategories'] = JHTML::_('select.genericList', $subcategories, 'fsubcatid', 'class="inputbox jquery-cbo" ' . '', 'value', 'text', $subcategoryid_filter);
        $lists['brands'] = JHTML::_('select.genericList', $brands, 'fbrand', 'class="inputbox jquery-cbo"' . '', 'value', 'text', $brandid_filter);

        $lists['pricestart'] = $pricestart_filter;
        $lists['priceend'] = $priceend_filter;
        $lists['title'] = $title_filter;

        $result = $lists;
        return $result;
    }

    function getSubCategoriesByCategoryForCombo($catid) {
        $db = $this->getDbo();
        $query = "SELECT id, title FROM `#__isproductlisting_subcategories` where categoryid = " . $catid;
        $db->setQuery($query);
        $result = $db->loadObjectList();
        $resultarray = array();
        foreach ($result AS $res) {
            $resultarray[] = array('value' => $res->id, 'text' => $res->title);
        }
        return $resultarray;
    }

    function getProductSort($sortvalue) {
        $sorting = array(
            '0' => array('value' => 'product.title ', 'text' => JText::_('TITLE')),
            '1' => array('value' => 'category.title', 'text' => JText::_('CATEGORY')),
            '2' => array('value' => 'subcategory.title', 'text' => JText::_('SUB_CATEGORY')),
            '3' => array('value' => 'brand.title', 'text' => JText::_('BRAND')),
            '4' => array('value' => 'product.price', 'text' => JText::_('PRICE')),
            '5' => array('value' => 'product.created', 'text' => JText::_('CREATED')));

        $selectsort['sort'] = JHTML::_('select.genericlist', $sorting, 'lv_sortvalue', 'class="inputbox jquery-cbo" ' . 'onchange="submitSorting(1)"', 'value', 'text', $sortvalue);
        return $selectsort;
    }

    function getProductCategories($label) {
        $db = $this->getDbo();
        $query = "SELECT  id, title FROM `#__isproductlisting_categories` WHERE status = 1 ORDER BY title ASC ";
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $categories = array();
        if ($label)
            $categories[] = array('value' => JText::_(''), 'text' => $label);
        foreach ($rows as $row) {
            $categories[] = array('value' => $row->id, 'text' => $row->title);
        }
        return $categories;
    }

    function getProductSubCategoriesById($label, $categoryid) {
        $db = $this->getDbo();
        $query = "SELECT  id, title FROM `#__isproductlisting_subcategories` WHERE status = 1 ";
        if ($categoryid)
            $query .= " AND categoryid = " . $categoryid;
        $query .= " ORDER BY title ASC ";
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $subcategories = array();
        if ($label)
            $subcategories[] = array('value' => JText::_(''), 'text' => $label);
        foreach ($rows as $row) {
            $subcategories[] = array('value' => $row->id, 'text' => $row->title);
        }
        return $subcategories;
    }

    function getProductBrand($label) {
        $db = $this->getDbo();
        $query = "SELECT  id, title FROM `#__isproductlisting_brands` WHERE status = 1 ORDER BY title ASC ";
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $brands = array();
        if ($label)
            $brands[] = array('value' => JText::_(''), 'text' => $label);
        foreach ($rows as $row) {
            $brands[] = array('value' => $row->id, 'text' => $row->title);
        }
        return $brands;
    }

    function getDefaultCurrency() {
        $db = $this->getDbo();
        $query = "SELECT symbol FROM `#__isproductlisting_currency` WHERE isdefault = 1";
        $db->setQuery($query);
        $currency = $db->loadResult();
        return $currency;
    }

    function getProductDetail($productid) {
        if (!is_numeric($productid))
            return false;
        $db = $this->getDbo();
        $query = "SELECT product.*, category.title AS cattitle, subcategory.title AS subcattitle, brand.title AS brandtitle, brand.image AS brandlogo
                        FROM `#__isproductlisting_products` AS product
                        LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        WHERE product.id = " . $productid . " AND product.status = 1";
        //echo $query;
        $db->setQuery($query);
        $product = $db->loadObject();
        $query = "SELECT productimage.image FROM `#__isproductlisting_product_images` AS productimage WHERE productimage.productid = $productid AND productimage.status = 1 ORDER BY isdefault DESC";
        $db->setQuery($query);
        $productimages = $db->loadObjectList();
        $query = "SELECT feedback.*, user.name AS username
                        FROM `#__isproductlisting_product_feedback` AS feedback 
                        JOIN `#__users` AS user ON user.id = feedback.userid
                        WHERE feedback.productid = $productid AND feedback.status = 1 ORDER BY feedback.created DESC";
        $db->setQuery($query);
        $productfeedback = $db->loadObjectList();
        $result[0]['product'] = $product;
        $result[0]['productfeedback'] = $productfeedback;
        $result[0]['productimages'] = $productimages;
        $this->updateHitsByProductId($productid);
        return $result;
    }

    private function updateHitsByProductId($productid) {
        $row = $this->getTable('product');
        if ($row->load($productid)) {
            $row->hits += 1;
            $row->store();
        }
    }

    function getAllProductBrands() {
        $db = $this->getDbo();
        $query = "SELECT brand.*, COUNT(product.id) AS totalproduct 
                        FROM `#__isproductlisting_brands` AS brand
                        LEFT JOIN `#__isproductlisting_products` AS product ON product.brandid = brand.id AND DATE(product.listproductexpirydate) >= CURDATE()
                        WHERE brand.status = 1 GROUP BY brand.id";
        $db->setQuery($query);
        $brands = $db->loadObjectList();
        $result[0] = $brands;
        return $result;
    }

    function getAllProductCategories() {
        $db = $this->getDbo();
        $query = "SELECT category.*, COUNT(product.id) AS totalproduct 
                        FROM `#__isproductlisting_categories` AS category
                        LEFT JOIN `#__isproductlisting_products` AS product ON product.categoryid = category.id AND DATE(product.listproductexpirydate) >= CURDATE()
                        WHERE category.status = 1 GROUP BY category.id";
        $db->setQuery($query);
        $categories = $db->loadObjectList();
        $result[0] = $categories;
        return $result;
    }

    function getAllProductSubCategories() {
        $db = $this->getDbo();
        $query = "SELECT category.id AS catid, category.title AS cattitle,category.image AS catlogo,subcategory.id AS subcatid,subcategory.title AS subcattitle,subcategory.image AS subcatlogo
                        FROM `#__isproductlisting_categories` AS category
                        JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.categoryid = category.id
                        WHERE category.status = 1 ORDER BY cattitle";
        //echo $query;
        $db->setQuery($query);
        $subcategories = $db->loadObjectList();
        $result[0] = $subcategories;
        return $result;
    }

    function getTopProductsByBrands($brandid) {
        if (!is_numeric($brandid))
            return false;
        $db = $this->getDbo();
        $query = "SELECT product.title,product.id FROM `#__isproductlisting_products` AS product WHERE product.brandid = " . $brandid . " AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.hits DESC ";
        $db->setQuery($query);
        $products = $db->loadObjectList();
        if ($products) {
            $totalproduct = count($products);
            $html = "";
            $itemid = JRequest::getVar('itemid');
            $i = 1;
            foreach ($products AS $product) {
                if ($i < 6)
                    $html .= '<span class="is_productlisting_category_productspan"><a class="is_productlisting_category_productspananchor" href="index.php?option=com_isproductlisting&view=product&layout=viewproduct&cl=4&pid=' . $product->id . '&Itemid=' . $itemid . '">' . $product->title . '</a></span>';
                $i++;
            }
            if ($totalproduct > 5)
                $html .= '<span class="is_productlisting_category_productspan"><a class="is_productlisting_moreinfo" href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=4&bid=' . $brandid . '&Itemid=' . $itemid . '">' . JText::_('MORE_INFO') . '</a></span>';
        }else {
            $html = JText::_('NO_PRODUCT_AVAILABLE');
        }
        return $html;
    }

    function getTopProductsByCategories($catid) {
        if (!is_numeric($catid))
            return false;
        $db = $this->getDbo();
        $query = "SELECT product.title,product.id FROM `#__isproductlisting_products` AS product WHERE product.categoryid = " . $catid . " AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.hits DESC ";
        $db->setQuery($query);
        $products = $db->loadObjectList();
        if ($products) {
            $totalproduct = count($products);
            $html = "";
            $itemid = JRequest::getVar('itemid');
            $i = 1;
            foreach ($products AS $product) {
                if ($i < 6)
                    $html .= '<span class="is_productlisting_category_productspan"><a class="is_productlisting_category_productspananchor" href="index.php?option=com_isproductlisting&view=product&layout=viewproduct&cl=2&pid=' . $product->id . '&Itemid=' . $itemid . '">' . $product->title . '</a></span>';
                $i++;
            }
            if ($totalproduct > 5)
                $html .= '<span class="is_productlisting_category_productspan"><a class="is_productlisting_moreinfo" href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=2&catid=' . $catid . '&Itemid=' . $itemid . '">' . JText::_('MORE_INFO') . '</a></span>';
        }else {
            $html = JText::_('NO_PRODUCT_AVAILABLE');
        }
        return $html;
    }

    function getTopProductsBySubCategories($subcatid) {
        if (!is_numeric($subcatid))
            return false;
        $db = $this->getDbo();
        $query = "SELECT product.title,product.id FROM `#__isproductlisting_products` AS product WHERE product.subcategoryid = " . $subcatid . " AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.hits DESC ";
        $db->setQuery($query);
        $products = $db->loadObjectList();
        if ($products) {
            $totalproduct = count($products);
            $html = "";
            $itemid = JRequest::getVar('itemid');
            $i = 1;
            foreach ($products AS $product) {
                if ($i < 6)
                    $html .= '<span class="is_productlisting_category_productspan"><a class="is_productlisting_category_productspananchor" href="index.php?option=com_isproductlisting&view=product&layout=viewproduct&cl=3&pid=' . $product->id . '&Itemid=' . $itemid . '">' . $product->title . '</a></span>';
                $i++;
            }
            if ($totalproduct > 5)
                $html .= '<span class="is_productlisting_category_productspan"><a class="is_productlisting_moreinfo" href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=3&subcatid=' . $subcatid . '&Itemid=' . $itemid . '">' . JText::_('MORE_INFO') . '</a></span>';
        }else {
            $html = JText::_('NO_PRODUCT_AVAILABLE');
        }
        return $html;
    }

    function getProductSearchFields() {
        $db = $this->getDbo();
        $query = "SELECT config.* FROM `#__isproductlisting_config` AS config WHERE config.configfor = 'search'";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        foreach ($result AS $res)
            $config[$res->configname] = $res->configvalue;
        $categories = $this->getProductCategories(JText::_('ALL_CATEGORIES'));
        $subcategories = $this->getProductSubCategoriesById(JText::_('ALL_SUB_CATEGORIES'), '');
        $brands = $this->getProductBrand(JText::_('ALL_BRANDS'));

        $lists['categories'] = JHTML::_('select.genericList', $categories, 'catid', 'class="inputbox jquery-cbo" ' . 'onChange=getSubCategoriesByCategory(this);', 'value', 'text', '');
        $lists['subcategories'] = JHTML::_('select.genericList', $subcategories, 'subcatid', 'class="inputbox jquery-cbo" ' . '', 'value', 'text', '');
        $lists['brands'] = JHTML::_('select.genericList', $brands, 'brand', 'class="inputbox jquery-cbo"' . '', 'value', 'text', '');

        $result[0] = $lists;
        $result[1] = $config;
        return $result;
    }

    function getProductSearchResults($searchdata, $limitstart, $limit) {
        $db = $this->getDBO();
        $result = array();
        $wherequery = "";
        if ((isset($searchdata['catid'])) && ($searchdata['catid'] != '')) {
            if (is_numeric($searchdata['catid']) == false)
                return false;
            $wherequery .= " AND product.catid = " . $searchdata['catid'];
        }
        if ((isset($searchdata['subcatid'])) && ($searchdata['subcatid'] != '')) {
            if (is_numeric($searchdata['subcatid']) == false)
                return false;
            $wherequery .= " AND product.subcatid = " . $searchdata['subcatid'];
        }
        if ((isset($searchdata['brandid'])) && ($searchdata['brandid'] != '')) {
            if (is_numeric($searchdata['brandid']) == false)
                return false;
            $wherequery .= " AND product.brandid = " . $searchdata['brandid'];
        }
        if ((isset($searchdata['pricestart']) && ($searchdata['pricestart'] != '') && ($searchdata['pricestart'] != 0))) {
            if (is_numeric($searchdata['pricestart']) == false)
                return false;
            $wherequery .= " AND product.price >= " . $searchdata['pricestart'];
        }
        if ((isset($searchdata['priceend']) && ($searchdata['priceend'] != '') && ($searchdata['priceend'] != 0))) {
            if (is_numeric($searchdata['priceend']) == false)
                return false;
            $wherequery .= " AND product.price <= " . $searchdata['priceend'];
        }
        if ((isset($searchdata['quantity'])) && ($searchdata['quantity'] != '')) {
            if (is_numeric($searchdata['quantity']) == false)
                return false;
            $wherequery .= " AND product.quantity >= " . $searchdata['quantity'];
        }
        if ((isset($searchdata['title'])) && ($searchdata['title'] != ''))
            $wherequery .= " AND product.title LIKE '%" . str_replace("'", "", $db->quote($searchdata['title'])) . "%'";

        $query = "SELECT count(product.id) FROM `#__isproductlisting_products` AS product
                    WHERE product.status = 1 AND DATE(product.listproductexpirydate) > CURDATE() ";
        $query .= $wherequery;
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total <= $limitstart)
            $limitstart = 0;

        $query = "SELECT product.*,productimage.image AS productdefaultimage,brand.image AS brandimage,brand.title AS brandtitle,category.title AS categorytitle,
                        subcategory.title AS subcategorytitle
                        FROM `#__isproductlisting_products` AS product
                        JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        WHERE product.status = 1 " . $wherequery . " AND DATE(listproductexpirydate) >= CURDATE() ";
        $query .= $wherequery;
        //echo $query;

        $db->setQuery($query, $limitstart, $limit);
        $products = $db->loadObjectList();
        foreach ($products AS $product) {
            $query = "SELECT productimage.image FROM `#__isproductlisting_product_images` AS productimage WHERE productimage.productid = $product->id AND productimage.status = 1 ORDER BY isdefault DESC LIMIT 4";
            $db->setQuery($query);
            $product->productimages = $db->loadObjectList();
        }

        $result[0] = $products;
        $result[1] = $total;

        return $result;
    }

    function sendToFriend() {
        $myname = JRequest::getVar('myname');
        $myemail = JRequest::getVar('myemail');
        $friendemail1 = JRequest::getVar('friendemail1');
        $friendemail2 = JRequest::getVar('friendemail2');
        $friendemail3 = JRequest::getVar('friendemail3');
        $sendtofrienditemid = JRequest::getVar('sendtofrienditemid');
        $message = JRequest::getVar('message');

        $recipient = array();
        $recipient[] = $friendemail1; // 2 to 6 friend emails
        if ($friendemail2 != '')
            $recipient[] = $friendemail2;
        if ($friendemail3 != '')
            $recipient[] = $friendemail3;

        $sendername = $myname;
        $senderemail = $myemail;
        $sendermessage = $message;

        $pid = JRequest::getVar('sendtofriendpid');
        if (!is_numeric($pid))
            return false;
        $message = JFactory::getMailer();
        $message->addRecipient($recipient); //to email
        //echo '<br> sbj '.$msgSubject;
        //echo '<br> bd '.$msgBody;
        $db = $this->getDbo();
        $templatefor = 'tell-friend';
        $query = "SELECT template.* FROM `#__isproductlisting_emailtemplates` AS template WHERE template.templatefor = " . $db->quote($templatefor);
        $db->setQuery($query);
        //echo '<br>sql '.$query;
        $template = $db->loadObject();
        $msgSubject = $template->subject;
        $msgBody = $template->body;
        $config = $this->getConfiginArray('default');
        $sitename = $config['title'];

        $productquery = "SELECT  product.title,product.price,brand.title AS brandtitle,category.title AS categorytitle,subcategory.title AS subcategorytitle
                        FROM `#__isproductlisting_products` AS product
                        JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        WHERE product.id = " . $pid;
        //echo '<br>sql '.$productquery;
        $db->setQuery($productquery);
        $product = $db->loadObject();
        $ProductTitle = $product->title;
        $Category = $product->categorytitle;
        $SubCategory = $product->subcategorytitle;
        $BrandTitle = $product->brandtitle;

        $siteAddress = JURI::root();
        $link = $siteAddress . "index.php?option=com_isproductlisting&view=product&layout=viewproduct&pl=1&cl=1&pid=" . $pid . "&Itemid=" . $sendtofrienditemid;

        $msgSubject = str_replace('{SITE_TITLE}', $sitename, $msgSubject);
        $msgSubject = str_replace('{SENDER_NAME}', $sendername, $msgSubject);
        $msgSubject = str_replace('{PRODCUT_TITLE}', $ProductTitle, $msgSubject);
        $msgSubject = str_replace('{BRAND}', $BrandTitle, $msgSubject);
        $msgSubject = str_replace('{CATEGORY}', $Category, $msgSubject);
        $msgSubject = str_replace('{SUB_CATEGORY}', $SubCategory, $msgSubject);
        $msgSubject = str_replace('{PRICE}', $this->getDefaultCurrency() . " " . $product->price, $msgSubject);
        $msgSubject = str_replace('{CLICK_HERE_TO_VISIT}', $link, $msgSubject);
        $msgSubject = str_replace('{MESSAGE}', $sendermessage, $msgSubject);

        $msgBody = str_replace('{SITE_TITLE}', $sitename, $msgBody);
        $msgBody = str_replace('{SENDER_NAME}', $sendername, $msgBody);
        $msgBody = str_replace('{PRODCUT_TITLE}', $ProductTitle, $msgBody);
        $msgBody = str_replace('{BRAND}', $BrandTitle, $msgBody);
        $msgBody = str_replace('{CATEGORY}', $Category, $msgBody);
        $msgBody = str_replace('{SUB_CATEGORY}', $SubCategory, $msgBody);
        $msgBody = str_replace('{PRICE}', $this->getDefaultCurrency() . " " . $product->price, $msgBody);
        $msgBody = str_replace('{CLICK_HERE_TO_VISIT}', $link, $msgBody);
        $msgBody = str_replace('{MESSAGE}', $sendermessage, $msgBody);

        $message->setSubject($msgSubject);
        $message->setBody($msgBody);
        $sender = array($senderemail, $sendername);
        $message->setSender($sender);
        $message->IsHTML(true);
        if (!$message->send())
            $sent = $message->sent();
        else
            $sent = true;
        return $sent;
    }

    function getCompareProductsDetail($productid1, $productid2) {
        if ($productid1)
            if (!is_numeric($productid1))
                return false;
        if ($productid2)
            if (!is_numeric($productid2))
                return false;
        $product1detail = $this->getProductDetail($productid1);
        $product2detail = $this->getProductDetail($productid2);
        $result[0] = $product1detail;
        $result[1] = $product2detail;
        return $result;
    }

    function autoCompleteProduct() {
        $config = $this->getConfiginArray('default');
        $db = &$this->getDBO();

        $term = JRequest::getVar('q'); //retrieve the search term that autocomplete sends
        $currency = $this->getDefaultCurrency();
        $q = "SELECT product.id, product.title as producttitle,product.price,productimage.image AS productimage,brand.title AS brandtitle,category.title AS categorytitle,
                subcategory.title AS subcategorytitle
                FROM `#__isproductlisting_products` AS product
                LEFT JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                WHERE product.title LIKE '%" . mysql_escape_string($term) . "%' AND product.status = 1 AND DATE(product.listproductexpirydate) >= CURDATE() LIMIT 10";
        $db->setQuery($q);
        $result = $db->loadObjectList(); //query the database for entries containing the term
        if (isset($result))
            foreach ($result AS $record) {
                $data = '<div id="isproductlisting_compareproduct"><table width="100%" id="isproductlisting_compareproducttable"><tr><td width="50px">';
                if (!empty($record->productimage))
                    $data .= '<img class="isproductlisting_comparevehiclelistimage" width="50px" height="50px"  src="' . JURI::root() . $config['data_directory'] . '/products/product_' . $record->id . '/thumbnail/medium-' . $record->productimage . '" />';
                else
                    $data .= '<img class="isproductlisting_comparevehiclelistimage" src="' . JURI::root() . 'components/com_isproductlisting/images/productdefaultimage.png" width="50px" height="50px" title="picture"  />';
                $data .= '</td><td width="100%"valign="top"><span id="isproductlisting_comparevehiclelisttitle" > ' . $record->producttitle . '</span>';
                $data .= '<span id="isproductlisting_comparevehiclelistbrand">' . $record->brandtitle . '</span>';
                $data .= '<span id="isproductlisting_comparevehiclelistprice">' . $currency . ' ' . $record->price . '</span>';
                $data .= "</td></tr></table></div>";
                $record->name = $data;
            }
        return $result;
    }

    function sendMailtoAdmin($id, $for) {
        $db = JFactory::getDBO();
        if ((is_numeric($id) == false) || ($id == 0) || ($id == ''))
            return false;
        $emailconfig = $this->getConfiginArray('email');
        $senderName = $emailconfig['mailfromname'];
        $senderEmail = $emailconfig['mailfromaddress'];
        $adminEmail = $emailconfig['adminemailaddress'];
        $purchaserequestadmin = $emailconfig['purchaserequestadmin'];
        switch ($for) {
            case 1: // new Vehicle
                $templatefor = 'purchase-requestadmin';
                $issendemail = $purchaserequestadmin;
                break;
        }
        if ($issendemail == 1) {
            $query = "SELECT template.* FROM `#__isproductlisting_emailtemplates` AS template WHERE template.templatefor = " . $db->Quote($templatefor);
            $db->setQuery($query);
            //echo '<br>sql '.$query;
            $template = $db->loadObject();
            $msgSubject = $template->subject;
            $msgBody = $template->body;

            switch ($for) {
                case 1: // purchase-requestadmin
                    $query = "SELECT cart.* FROM `#__isproductlisting_cart` AS cart WHERE cart.id = " . $id;
                    //echo '<br>'.$query;
                    $db->setQuery($query);
                    $cart = $db->loadObject();

                    $msgSubject = str_replace('{USER_NAME}', $cart->name, $msgSubject);
                    $msgSubject = str_replace('{USER_EMAIL}', $cart->emailaddress, $msgSubject);
                    $msgSubject = str_replace('{SITE_TITLE}', $siteTitle, $msgSubject);
                    $msgSubject = str_replace('{SHOPPING_CART_ID}', $id, $msgSubject);
                    $msgSubject = str_replace('{CART_TOTAL_AMOUNT}', $cart->carttotal, $msgSubject);
                    $msgSubject = str_replace('{DATE}', $cart->created, $msgSubject);

                    $msgBody = str_replace('{USER_NAME}', $cart->name, $msgBody);
                    $msgBody = str_replace('{USER_EMAIL}', $cart->emailaddress, $msgBody);
                    $msgBody = str_replace('{SITE_TITLE}', $siteTitle, $msgBody);
                    $msgBody = str_replace('{SHOPPING_CART_ID}', $id, $msgBody);
                    $msgBody = str_replace('{CART_TOTAL_AMOUNT}', $cart->carttotal, $msgBody);
                    $msgBody = str_replace('{DATE}', $cart->created, $msgBody);
                break;
            }

            $message = JFactory::getMailer();
            $message->addRecipient($adminEmail); //to email
            //echo '<br>adminEmail'.$adminEmail;
            //echo '<br>senderEmail'.$senderEmail;
            //echo '<br>senderName'.$senderName;
            //echo '<br> sbj '.$msgSubject;
            //echo '<br> bd '.$msgBody;exit;
            $message->setSubject($msgSubject);
            $siteAddress = JURI::base();
            $message->setBody($msgBody);
            $sender = array($senderEmail, $senderName);
            $message->setSender($sender);
            $message->IsHTML(true);
            if(!$message->send())
                $sent = $message->send();
            else
                $sent = false;
            return $sent;
        }
        return true;
    }

    function sendMailtoUser($id, $for) {
        $db = JFactory::getDBO();
        if ((is_numeric($id) == false) || ($id == 0) || ($id == ''))
            return false;
        $emailconfig = $this->getConfiginArray('email');
        $senderName = $emailconfig['mailfromname'];
        $senderEmail = $emailconfig['mailfromaddress'];
        $adminEmail = $emailconfig['adminemailaddress'];
        $purchaserequestuser = $emailconfig['purchaserequestuser'];
        switch ($for) {
            case 1: // new Vehicle
                $templatefor = 'purchase-requestuser';
                $issendemail = $purchaserequestuser;
                break;
        }
        if ($issendemail == 1) {
            $query = "SELECT template.* FROM `#__isproductlisting_emailtemplates` AS template WHERE template.templatefor = " . $db->Quote($templatefor);
            $db->setQuery($query);
            //echo '<br>sql '.$query;
            $template = $db->loadObject();
            $msgSubject = $template->subject;
            $msgBody = $template->body;

            switch ($for) {
                case 1: // purchase-requestadmin
                    $query = "SELECT cart.* FROM `#__isproductlisting_cart` AS cart WHERE cart.id = " . $id;
                    //echo '<br>'.$query;
                    $db->setQuery($query);
                    $cart = $db->loadObject();

                    $msgSubject = str_replace('{USER_NAME}', $cart->name, $msgSubject);
                    $msgSubject = str_replace('{USER_EMAIL}', $cart->emailaddress, $msgSubject);
                    $msgSubject = str_replace('{SITE_TITLE}', $siteTitle, $msgSubject);
                    $msgSubject = str_replace('{SHOPPING_CART_ID}', $id, $msgSubject);
                    $msgSubject = str_replace('{CART_TOTAL_AMOUNT}', $cart->carttotal, $msgSubject);
                    $msgSubject = str_replace('{DATE}', $cart->created, $msgSubject);

                    $msgBody = str_replace('{USER_NAME}', $cart->name, $msgBody);
                    $msgBody = str_replace('{USER_EMAIL}', $cart->emailaddress, $msgBody);
                    $msgBody = str_replace('{SITE_TITLE}', $siteTitle, $msgBody);
                    $msgBody = str_replace('{SHOPPING_CART_ID}', $id, $msgBody);
                    $msgBody = str_replace('{CART_TOTAL_AMOUNT}', $cart->carttotal, $msgBody);
                    $msgBody = str_replace('{DATE}', $cart->created, $msgBody);
                break;
            }

            $message = JFactory::getMailer();
            $message->addRecipient($cart->emailaddress); //to email
            //echo '<br>adminEmail'.$adminEmail;
            //echo '<br>senderEmail'.$senderEmail;
            //echo '<br>senderName'.$senderName;
            //echo '<br> sbj '.$msgSubject;
            //echo '<br> bd '.$msgBody;exit;
            $message->setSubject($msgSubject);
            $siteAddress = JURI::base();
            $message->setBody($msgBody);
            $sender = array($senderEmail, $senderName);
            $message->setSender($sender);
            $message->IsHTML(true);
            if(!$message->send())
                $sent = $message->send();
            else
                $sent = false;
            return $sent;
        }
        return true;
    }

    function getParams() {
        $db = $this->getDBO();
        $mt = $this->_ptr . "ost\b/";
        $result[0] = 'ine';
        $value = $this->getCurU();
        $cu = $value[2];
        if (preg_match($mt, $cu)) {
            $result[1] = 0;
            return $result;
        }
        $result[0] = 'ine';
        $result[1] = 0;
        return $result;
    }

    function getCurU() {
        $result = array();
        $result[0] = 'a' . substr($this->_arv, 16, 2) . substr($this->_arv, 24, 1);
        $result[1] = substr($this->_arv, 5, 2) . substr($this->_arv, 12, 3) . substr($this->_arv, 20, 1) . 'e';
        $result[2] = $_SERVER['SERVER_NAME'];
        return $result;
    }

    function getConfiginArray($configfor) {
        $db = $this->getDBO();
        $query = "SELECT * FROM `#__isproductlisting_config` WHERE configfor = " . $db->quote($configfor);
        $db->setQuery($query);
        $config = $db->loadObjectList();
        $configs = array();
        foreach ($config as $conf) {
            $configs[$conf->configname] = $conf->configvalue;
        }
        return $configs;
    }
    
    function storeFeedback(){
        $row = $this->getTable('feedback');
        $data['userid'] = JFactory::getUser()->id;
        $data['feed'] = JRequest::getVar('val');
        $data['productid'] = JRequest::getVar('productid');
        $data['status'] = 1;
        $data['created'] = date('Y-m-d H:i:s');
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
        }
        return true;
    }
}

?>