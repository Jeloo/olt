<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
jimport('joomla.html.html');

class ISProductlistingModelTheme extends ITModel {
    static function getThemeColor(){
        $theme = parse_ini_file(JPATH_ADMINISTRATOR.'/components/com_isproductlisting/include/theme.ini');
        $css = "
        div#is_toppanel{background:".$theme['color1'].";}
        div#is_sitetitle{color:".$theme['color4'].";border-bottom:1px solid ".$theme['color4'].";}
        div#is_topcurloc{color:".$theme['color4'].";}
        div#is_top_links a{color:".$theme['color4'].";background:".$theme['color2'].";}
        div#is_top_links a:hover{color:".$theme['color4'].";background:".$theme['color3'].";}
        div#is_topheading,
        div#isproductlisting_sendtofriends_heading,
        span#is_productpopup_title{color:".$theme['color4'].";background:".$theme['color2'].";}
        div#is_productfilter{border:1px solid ".$theme['color2'].";}
        div#filter_button button{color:".$theme['color4'].";background:".$theme['color2'].";}
        div#filter_button button:hover{background:".$theme['color3'].";}
        div#is_product_topbar{background:".$theme['color2'].";color:".$theme['color4'].";}
        span#is_product_title a,span#is_product_title a:hover{color:".$theme['color4'].";}
        div#is_product_image img{border:2px solid ".$theme['color2'].";}
        div#is_product_image_bottom img{border:1px solid ".$theme['color2'].";}
        span#is_product_price{border: 1px solid ".$theme['color3'].";color:".$theme['color3'].";}
        div#is_product_listing_bottomicons{border:1px solid ".$theme['color2'].";}
        div#is_product_listing_bottomicons a{color:".$theme['color2'].";}
        div#is_product_listing_bottomicons a:hover{color:".$theme['color3'].";}
        div#is_pagination{background:".$theme['color1'].";color:".$theme['color4'].";}
        div.is_product_category_wrapper{border:1px solid ".$theme['color2'].";}
        div.is_product_category_wrapper span.is_product_category_title{background:".$theme['color2'].";}
        div.is_product_category_wrapper span.is_product_category_title:hover{background:".$theme['color3'].";}
        div.is_product_category_wrapper span.is_product_category_title a{color:".$theme['color4'].";}
        div.is_product_subcategories_categorytitle_left{color:".$theme['color2'].";border-bottom:1px solid ".$theme['color2'].";}
        span#is_product_upper_producttitle{background:".$theme['color2'].";color:".$theme['color4'].";}
        span#is_product_upper_title{border-bottom:1px solid ".$theme['color2'].";color:".$theme['color3'].";}
        span#is_product_upper_value{color:".$theme['color5'].";}
        div#is_product_share_panel{background:".$theme['color1'].";}
        span#product_fbcommentheading{background:".$theme['color2'].";color:".$theme['color4'].";}
        div#is_product_lower_part span#is_product_upper_title{background:".$theme['color2'].";color:".$theme['color4'].";}
        div#is_form_submit_button input,input#is_productpopup_submit{background:".$theme['color2'].";color:".$theme['color4'].";}
        div#is_form_submit_button input:hover,input#is_productpopup_submit:hover{background:".$theme['color3'].";color:".$theme['color4'].";}
        div#productlisting_no_record_found{color:".$theme['color3'].";}
        div#tell_a_friend,div#popup,div#shortlist_product{background:".$theme['color1'].";border:2px solid ".$theme['color2'].";}
        span#is_product_upper_value.it_datasheet{border-bottom:1px solid ".$theme['color2'].";}
        span.it_product_feedback_not_found{color:".$theme['color3'].";}
        div.is_product_feedback{border:2px solid ".$theme['color2'].";color:".$theme['color5'].";}
        div#is-productimages-thumbnail div.thumbnail{border:1px solid ".$theme['color2']." !important;}
        div#is-productimages-thumbnail-2 div.thumbnail{border:1px solid ".$theme['color2']." !important;}
        div#is_modWrapperDiv div.is_modproduct_tablerow div.is_modproductdata span.is_modproduct_data span.is_modproduct_title{border-bottom:1px solid ".$theme['color2'].";}
        div#is_modWrapperDiv div#is_mod_uppertitle_logo span#is_modproducttitle a{color:".$theme['color2'].";}
        div#is_modWrapperDiv div#is_mod_uppertitle_logo span#is_modproducttitle a:hover{color:".$theme['color3'].";}
        div#is_modWrapperDiv div#is_moddatadiv span.is_modnumberlistspan a{color:".$theme['color5'].";background:none;}
        div#is_modWrapperDiv div#is_moddatadiv span.is_modnumberlistspan span.is_modnumberspan{color:".$theme['color4'].";background:".$theme['color2'].";}
        span#is_product_item_title{color:".$theme['color5'].";}
        span#is_product_item_value{color:".$theme['color5'].";}
        div#is_form_wraper div.is_form_fields_wrapper div.is_form_fields_title{color:".$theme['color5'].";}
        div.is_product_feedback span.is_product_feedback_personname{color:".$theme['color3'].";}
        div#is_pagination_pageslink ul li a{background:".$theme['color2'].";border:0px;color:".$theme['color4'].";}
        div#is_pagination_pageslink ul li a:hover{background:".$theme['color3'].";border:0px;color:".$theme['color4'].";}
        div#isproductlisting_cp_icon_portion a.cp_linkanchor{background:".$theme['color1'].";color:".$theme['color4'].";}
        div#isproductlisting_cp_icon_portion a.cp_linkanchor:hover{background:".$theme['color2'].";color:".$theme['color4'].";}
        div#popupinner{color:".$theme['color4'].";}
        div#is_productlisting_wrapper div#productlist_cart_wrapper div.productlisting_item_wrapper.heading{background:".$theme['color3'].";color:".$theme['color4'].";}
        div#is_productlisting_wrapper div#productlist_cart_wrapper div.productlisting_item_wrapper{border-bottom:1px solid ".$theme['color3'].";color:".$theme['color5'].";}
        div#productlist_cart_item_total_wrapper{color:".$theme['color5'].";}
        div.productlisting_paymentbuynow_wrapper span.productlisting_payment_title{color:".$theme['color5'].";}
        div.productlisting_paymentbuynow_wrapper input#productlisting_paymentbutton{background:".$theme['color2'].";color:".$theme['color4'].";outline:0px;border:0px;}
        div.productlisting_paymentbuynow_wrapper input#productlisting_paymentbutton:hover{background:".$theme['color3'].";}
        div.productlisting_paymentbuynow_wrapper{border-bottom: 1px solid ".$theme['color2'].";}
        div#is_modWrapperDiv div.is_modproduct_tablerow div.is_modproductdata span.is_modproduct_data{color:".$theme['color5'].";}
        ";
        return $css;
    }
}

?>
