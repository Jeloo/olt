<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
jimport('joomla.html.html');

class ISProductlistingModelRss extends ITModel {

    var $_config = null;

    function __construct() {
        parent :: __construct();
        $user = JFactory::getUser();
        $this->_arv = "/\aseofm/rvefli/ctvrnaa/kme/\rfer";
        $this->_ptr = "/\blocalh";
    }

    function getConfiginArray($configfor) {
        $db = $this->getDBO();
        $query = "SELECT * FROM `#__isproductlisting_config` WHERE configfor = " . $db->quote($configfor);
        $db->setQuery($query);
        $config = $db->loadObjectList();
        $configs = array();
        foreach ($config as $conf) {
            $configs[$conf->configname] = $conf->configvalue;
        }
        return $configs;
    }

    function getDataDirectory() {
        $db = $this->getDBO();
        $data_directory = 'data_directory';
        $query = "SELECT configvalue FROM `#__isproductlisting_config` WHERE configname = " . $db->quote($data_directory);
        $db->setQuery($query);
        $datadirectory = $db->loadObject();
        return $datadirectory;
    }

    function getRssProducts() {
        $db = $this->getDBO();
        $result = array();
        $query = "SELECT product.*,productimage.image AS productdefaultimage,brand.image AS brandimage,brand.title AS brandtitle,category.title AS categorytitle,
                        subcategory.title AS subcategorytitle
                        FROM `#__isproductlisting_products` AS product
                        JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                        LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                        LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                        LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                        WHERE product.status = 1  AND date(listproductexpirydate) >= CURDATE() ";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }
    function getDefaultCurrency() {
        $db = $this->getDbo();
        $query = "SELECT symbol FROM `#__isproductlisting_currency` WHERE isdefault = 1";
        $db->setQuery($query);
        $currency = $db->loadResult();
        return $currency;
    }    
}

?>
