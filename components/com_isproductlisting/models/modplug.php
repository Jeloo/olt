<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
jimport('joomla.html.html');

class ISProductlistingModelModplug extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function mpGetBrands($showbrandhaveproduct = 0, $noofrecord = 20) {
        $db = $this->getDBO();
        $joinquery = ($showbrandhaveproduct == 1) ? " JOIN `#__isproductlisting_products` AS product ON product.brandid = brand.id AND DATE(product.listproductexpirydate) >= CURDATE()":"LEFT JOIN `#__isproductlisting_products` AS product ON product.brandid = brand.id AND DATE(product.listproductexpirydate) >= CURDATE()";
        
        $query = "SELECT brand.id AS brandid, brand.title AS brandtitle, COUNT(product.id) AS totalproduct
                    FROM `#__isproductlisting_brands` AS brand
                    $joinquery                            
                    WHERE brand.status = 1 GROUP BY brandtitle
                    ORDER BY totalproduct DESC, brandtitle ASC";
        $db->setQuery($query, 0, $noofrecord);
        $brands = $db->loadObjectList();
        return $brands;
    }
    function mpGetCategories($showcategoryhaveproduct = 0, $noofrecord = 20) {
        $db = $this->getDBO();
        $joinquery = ($showcategoryhaveproduct == 1) ? " JOIN `#__isproductlisting_products` AS product ON product.categoryid = category.id AND DATE(product.listproductexpirydate) >= CURDATE()":"LEFT JOIN `#__isproductlisting_products` AS product ON product.categoryid = category.id AND DATE(product.listproductexpirydate) >= CURDATE()";
        
        $query = "SELECT category.id AS categoryid, category.title AS categorytitle, COUNT(product.id) AS totalproduct
                    FROM `#__isproductlisting_categories` AS category
                    $joinquery                            
                    WHERE category.status = 1 GROUP BY categorytitle
                    ORDER BY totalproduct DESC, categorytitle ASC";

        $db->setQuery($query, 0, $noofrecord);
        $category = $db->loadObjectList();
        return $category;
    }
    function mpGetHotProducts($noofrecord = 20) {
        $db = $this->getDBO();
        $query = "SELECT product.*,product.title AS producttitle, category.title AS categorytitle, subcategory.title AS subcategorytitle, brand.title AS brandtitle, productimage.image AS productdefaultimage,
                    brand.id AS brandid, brand.image AS productbrandimage
                    FROM `#__isproductlisting_products` AS product
                    LEFT JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                    LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                    LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                    LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                    WHERE product.status = 1 AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.title ASC";

        $db->setQuery($query, 0, $noofrecord);
        $hotproducts = $db->loadObjectList();
        return $hotproducts;
    }
    function mpGetFeaturedProducts($noofrecord = 20) {
        $db = $this->getDBO();
        $query = "SELECT product.*,product.title AS producttitle, category.title AS categorytitle, subcategory.title AS subcategorytitle, brand.title AS brandtitle, productimage.image AS productdefaultimage,
                    brand.id AS brandid, brand.image AS productbrandimage
                    FROM `#__isproductlisting_products` AS product
                    LEFT JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                    LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                    LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                    LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                    WHERE product.isfeatured = 1 AND product.status = 1 AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.title ASC";

        $db->setQuery($query, 0, $noofrecord);
        $hotproducts = $db->loadObjectList();
        return $hotproducts;
    }
    function mpGetGoldProducts($noofrecord = 20) {
        $db = $this->getDBO();
        $query = "SELECT product.*,product.title AS producttitle, category.title AS categorytitle, subcategory.title AS subcategorytitle, brand.title AS brandtitle, productimage.image AS productdefaultimage,
                    brand.id AS brandid, brand.image AS productbrandimage
                    FROM `#__isproductlisting_products` AS product
                    LEFT JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                    LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid
                    LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                    LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                    WHERE product.isgold = 1 AND product.status = 1 AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.title ASC";

        $db->setQuery($query, 0, $noofrecord);
        $hotproducts = $db->loadObjectList();
        return $hotproducts;
    }
    function mpGetProductsForSlideShow($showproducttype = 3,$noofrecord = 20) {
        $db = $this->getDBO();
        switch($showproducttype){
            case "1": // Gold product
                $producttypequery = " AND product.isgold = 1 ";
            break;
            case "2": // Featured product
                $producttypequery = " AND product.isfeatured = 1 ";
            break;
            case "3": // Latest product
                $producttypequery = " ";
            break;
        }
        $query = "SELECT product.id, productimage.image AS productdefaultimage                    
                    FROM `#__isproductlisting_products` AS product
                    LEFT JOIN `#__isproductlisting_product_images` AS productimage ON productimage.productid = product.id AND productimage.isdefault = 1
                    WHERE product.status = 1 $producttypequery AND DATE(product.listproductexpirydate) >= CURDATE() ORDER BY product.created DESC";

        $db->setQuery($query, 0, $noofrecord);
        $hotproducts = $db->loadObjectList();
        return $hotproducts;
    }
    function getConfig($configfor) {
        $db = $this->getDBO();
        $query = "SELECT * FROM `#__isproductlisting_config` WHERE configfor = " . $db->quote($configfor);
        $db->setQuery($query);
        $config = $db->loadObjectList();
        $configs = array();
        foreach ($config as $conf) {
            $configs[$conf->configname] = $conf->configvalue;
        }
        return $configs;
    }
    
    function getDefaultCurrency(){
        $db = $this->getDbo();
        $query = "SELECT currency.symbol AS symbol FROM `#__isproductlisting_currency` AS currency WHERE currency.isdefault = 1";
        $db->setQuery($query);
        $currency = $db->loadResult();
        return $currency;
    }
}
?>

