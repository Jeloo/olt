<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */

defined('_JEXEC') or die('Restricted access');
$version = new JVersion;
$joomla = $version->getShortVersion();
$jversion = substr($joomla, 0, 3);
if (!defined('JVERSION')) {
    define('JVERSION', $jversion);
}
// requires the default controller 
require_once (JPATH_COMPONENT . '/ITApplication.php');
require_once (JPATH_COMPONENT . '/controller.php');
$task = JRequest::getVar('task');
$c = '';
if (strstr($task, '.')) {
    $array = explode('.', $task);
    $c = $array[0];
    $task = $array[1];
} else {
    $task = JRequest::getVar('task', 'display');
    $c = JRequest::getVar('c', 'productlisting');
}
if ($c != '') {
    $path = JPATH_COMPONENT . '/controllers/' . $c . '.php';
    jimport('joomla.filesystem.file');
    if (JFile :: exists($path)) {
        require_once ($path);
    } else {
        JError :: raiseError('500', JText :: _('Unknown controller: <br>' . $c . ':' . $path));
    }
}
/*
 * Define the name of the controller class we're going to use
 * Instantiate a new instance of the controller class
 * Execute the task being called (default to 'display')
 * If it's set, redirect to the URI
 */
$c = 'ISProductlistingController'.  ucfirst($c);
$controller = new $c ();
$controller->execute($task);
$controller->redirect();
?>
