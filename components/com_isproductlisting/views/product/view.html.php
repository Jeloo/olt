<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewproduct extends ITView {

    function display($tpl = null) {        
        $user = JFactory::getUser();
        if (!$user->guest)
            $uid = $user->id;
        $Itemid = JRequest::getVar('Itemid');
        $layout = JRequest::getVar('layout');

        $model = $this->getModel('product');

        $option = 'com_isproductlisting';
        $type = 'offl';

        $mainframe = JFactory::getApplication();

        // get configurations
        $config = Array();
        $session = JFactory::getSession();
        $config = $session->get('isproductlistingconfig_deft');
        $config = Array();
        if (sizeof($config) == 0) {
            $results = $model->getConfiginArray('default');
            if ($results) { //not empty
                $config = $results;
                $result = $model->getParams();
                $type .= $result[0];
                $value = $result[1];
                if ($config[$type] != 1)
                    $config[$type] = $value;
                $session->set('isproductlistingconfig_deft', $config);
            }
        }
        $needlogin = false;
        switch ($layout) {
            case 'abc': if ($config['compare_requiredlogin'] == 1) $needlogin = true; break;
            default : $needlogin = false; break;
        }
        if ($user->guest) { // redirect user if not login
            if ($needlogin) {
                $uri = JFactory::getURI();
                $return = $uri->toString();
                $url = 'index.php?option=com_users&view=login';
                $url .= '&return=' . base64_encode($return);
                $msg = JText::_('LOGIN_DESCRIPTION');
                $mainframe->redirect($url, $msg);
            }
        }

        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0);
        $viewtype = 'html';
        $params = $mainframe->getPageParameters('com_isproductlisting');

        if ($config['toplink_products'] == 1) {
            $link = 'index.php?option=com_isproductlisting&view=product&layout=listproduct&plink=1&Itemid=' . $Itemid;
            $links [] = array($link, JText::_('PRODUCTS'), 0);
        }
        if ($config['toplink_categories'] == 1) {
            $link = 'index.php?option=com_isproductlisting&view=product&layout=productcategories&plink=1&Itemid=' . $Itemid;
            $links [] = array($link, JText::_('CATEGORIES'), 0);
        }
        if ($config['toplink_subcategories'] == 1) {
            $link = 'index.php?option=com_isproductlisting&view=product&layout=productsubcategories&plink=1&Itemid=' . $Itemid;
            $links [] = array($link, JText::_('SUB_CATEGORIES'), 0);
        }
        if ($config['toplink_brands'] == 1) {
            $link = 'index.php?option=com_isproductlisting&view=product&layout=productbrands&plink=1&Itemid=' . $Itemid;
            $links [] = array($link, JText::_('BRANDS'), 0);
        }
        if ($config['toplink_search'] == 1) {
            $link = 'index.php?option=com_isproductlisting&view=product&layout=productsearch&Itemid=' . $Itemid;
            $links [] = array($link, JText::_('SEARCH'), 0);
        }

        $socailsharing = $model->getConfiginArray('socailsharing');
        $defaultcurrency = $model->getDefaultCurrency();        
        if ($layout == 'controlpanel') { // Control panel            
            $results = $model->getConfiginArray('link');
            $this->assignRef('cplinks', $results);
        } elseif ($layout == 'listproduct') { // list product
            $data = JRequest::get('post');
            $limitstart = JRequest::getVar('limitstart', '', 'get');
            $start = JRequest::getVar('start', '', 'get');
            $link = JRequest::getVar('plink');

            if (($limitstart == '') && ($start == ''))
                if ($link) { //first time call
                    //VARIABLES
                    $categoryid = JRequest::getVar('catid', '');
                    $subcategoryid = JRequest::getVar('subcatid', '');
                    $brandid = JRequest::getVar('bid', '');
                    $pricestart = JRequest::getVar('prs', '');
                    $priceend = JRequest::getVar('pre', '');

                    //filter reset
                    $mainframe->setUserState($option . 'fcatid', $categoryid);
                    $mainframe->setUserState($option . 'fsubcatid', $subcategoryid);
                    $mainframe->setUserState($option . 'fbid', $brandid);
                    $mainframe->setUserState($option . 'fprs', $pricestart);
                    $mainframe->setUserState($option . 'fpre', $priceend);
                    $mainframe->setUserState($option . 'ftitle', '');
                    $mainframe->setUserState($option . 'lv_sortvalue', 'product.created');
                    $mainframe->setUserState($option . 'lv_sortorder', 'desc');
                }

            $sproductfilter = JRequest::getVar('isfilter');
            $cl = JRequest::getVar('cl');
            $categoryid_filter = $mainframe->getUserStateFromRequest($option . 'fcatid', 'fcatid', '', 'int');
            $subcategoryid_filter = $mainframe->getUserStateFromRequest($option . 'fsubcatid', 'fsubcatid', '', 'int');
            $brandid_filter = $mainframe->getUserStateFromRequest($option . 'fbid', 'fbid', '', 'int');
            $pricestart_filter = $mainframe->getUserStateFromRequest($option . 'fprs', 'fprs', '', 'int');
            $priceend_filter = $mainframe->getUserStateFromRequest($option . 'fpre', 'fpre', '', 'int');
            $title_filter = $mainframe->getUserStateFromRequest($option . 'ftitle', 'ftitle', '', 'string');

            $sortvalue = $mainframe->getUserStateFromRequest($option . 'lv_sortvalue', 'lv_sortvalue', 'product.created', 'string');
            $sortorder = $mainframe->getUserStateFromRequest($option . 'lv_sortorder', 'lv_sortorder', 'desc', 'string');
            $filterconfig = $model->getConfiginArray('filter');
            $rss = $model->getConfiginArray('rss');
            $productfilter = $model->getProductFilter($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter);
            $sort = $model->getProductSort($sortvalue);
            $goldproduct = JRequest::getVar('gp');
            $featuredproduct = JRequest::getVar('fp');
            $result = $model->getAllProducts($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter, $goldproduct, $featuredproduct, $sortorder, $sortvalue, $limitstart, $limit);
            if(!$featuredproduct)
                $featuredproduct = $model->getAllFeaturedProducts($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter, $sortorder, $sortvalue, $limitstart, $limit);
            else $featuredproduct = false;
            if(!$goldproduct)
                $goldproduct = $model->getAllGoldProducts($title_filter, $categoryid_filter, $subcategoryid_filter, $brandid_filter, $pricestart_filter, $priceend_filter, $sortorder, $sortvalue, $limitstart, $limit);
            else $goldproduct = false;
            $totalresult = $result[1];
            $pagination = new JPagination($totalresult, $limitstart, $limit);
            $this->assignRef('pagination', $pagination);
            $this->assignRef('sortvalue', $sortvalue);
            $this->assignRef('sortorder', $sortorder);
            $this->assignRef('sort', $sort);
            $this->assignRef('products', $result[0]);
            $this->assignRef('featuredproducts', $featuredproduct);
            $this->assignRef('goldproducts', $goldproduct);
            $this->assignRef('categoryid', $categoryid_filter);
            $this->assignRef('subcategoryid', $subcategoryid_filter);
            $this->assignRef('brandid', $brandid_filter);
            $this->assignRef('prs', $pricestart_filter);
            $this->assignRef('pre', $priceend_filter);
            $this->assignRef('filterconfig', $filterconfig);
            $this->assignRef('filter', $productfilter);
            $this->assignRef('isfilter', $sproductfilter);
            $this->assignRef('cl', $cl);
            $this->assignRef('rss', $rss);
        } elseif ($layout == 'viewproduct') { // View product
            $productid = JRequest::getVar('pid');
            $result = $model->getProductDetail($productid);
            $this->assignRef('product', $result[0]['product']);
            $this->assignRef('productfeedback', $result[0]['productfeedback']);
            $this->assignRef('productimages', $result[0]['productimages']);
            $this->assignRef('productfields', $result[2]);
            $this->assignRef('productid', $productid);
        } elseif ($layout == 'compareproduct') { // compare product
          $pid1 = JRequest::getVar('pid1','0');
            $productid1 = JRequest::getVar('compare_search1', $pid1);
            $productid2 = JRequest::getVar('compare_search2', '0');
            $result = $model->getCompareProductsDetail($productid1, $productid2);
            $this->assignRef('product1', $result[0][0]['product']);
            $this->assignRef('productfeedback1', $result[0][0]['productfeedback']);
            $this->assignRef('productimages1', $result[0][0]['productimages']);
            $this->assignRef('productfields1', $result[0][2]);
            $this->assignRef('productid1', $productid1);
            $this->assignRef('product2', $result[1][0]['product']);
            $this->assignRef('productfeedback2', $result[1][0]['productfeedback']);
            $this->assignRef('productimages2', $result[1][0]['productimages']);
            $this->assignRef('productfields2', $result[1][2]);
            $this->assignRef('productid2', $productid2);
        }elseif ($layout == 'productcategories') { // Product Categories
            $result = $model->getAllProductCategories();
            $this->assignRef('categories', $result[0]);
        } elseif ($layout == 'productsubcategories') { // Product Sub Categories
            $result = $model->getAllProductSubCategories();
            $this->assignRef('subcategories', $result[0]);
        } elseif ($layout == 'productbrands') { // Product Brands
            $result = $model->getAllProductBrands();
            $this->assignRef('brands', $result[0]);
        } elseif ($layout == 'productsearch') { //Product search
            $result = $model->getProductSearchFields();
            $this->assignRef('lists', $result[0]);
            $this->assignRef('searchconfig', $result[1]);
        } elseif ($layout == 'productsearchresult') { //Product search result
            $data = JRequest :: get('post');
            $session = JFactory::getSession();
            if (isset($data['isproductsearch'])) {
                if ($data['isproductsearch'] == 1) {
                    $session->set('isproductlisting_psearchdata', $data);
                }
            }
            $searchdata = $session->get('isproductlisting_psearchdata');
            if (isset($searchdata)) {
                $result = $model->getProductSearchResults($searchdata, $limitstart, $limit);
                $this->assignRef('productsearchresult', $result[0]);
                $totalresult = $result[1];
                $pagination = new JPagination($totalresult, $limitstart, $limit);
                $this->assignRef('pagination', $pagination);
            }
        } elseif ($layout == 'mycart') {
            $cartitems = $model->getMyCartItems();
            $paymentconfig = $model->getPaymentMethodsConfig();
            $this->assignRef('paymentconfig', $paymentconfig);
            $this->assignRef('cartitems', $cartitems);
        } elseif ($layout == 'shortlistproducts') {
            $sortvalue = JRequest::getVar('lv_sortvalue', 'created');
            $sortorder = JRequest::getVar('lv_sortorder', 'desc');
            //COOKIES
            $shortlistarray = array();
            if (isset($_COOKIE['isproductlistingshortlistid']))
                $shortlistarray = $_COOKIE['isproductlistingshortlistid'];
            $result = $model->getShortListProducts($shortlistarray, $sortvalue, $sortorder, $limitstart, $limit);
            $this->assignRef('products', $result[0]);
            $totalresult = $result[1];

            $sort = $model->getProductSort($sortvalue);
            $pagination = new JPagination($totalresult, $limitstart, $limit);
            $this->assignRef('pagination', $pagination);
            $this->assignRef('sortvalue', $sortvalue);
            $this->assignRef('sortorder', $sortorder);

            $this->assignRef('sort', $sort);
            $this->assignRef('list', $quicklink);
        }

        $css = $this->getModel('theme')->getThemeColor();
        $this->assignRef('css', $css);
        $this->assignRef('config', $config);
        $this->assignRef('option', $option);
        $this->assignRef('params', $params);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('uid', $uid);
        $this->assignRef('Itemid', $Itemid);
        $this->assignRef('links', $links);
        $this->assignRef('socailsharing', $socailsharing);
        $this->assignRef('defaultcurrency', $defaultcurrency);
        $this->assignRef('user', $user);

        parent :: display($tpl);
    }

}

?>