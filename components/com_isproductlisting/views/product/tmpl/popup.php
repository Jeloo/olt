<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.formvalidation');

$document = JFactory::getDocument();
$document->addScript('components/com_isproductlisting/views/product/tmpl/popup.js');
?>
<span id="jquery-messagefor-maxquantity" style="display:none;"><?php echo JText::_('REQUIRED_QUANTITY_OUT_OF_STOCK'); ?></span>
<span id="jquery-messagefor-zeroquantity" style="display:none;"><?php echo JText::_('QUANTITY_MUST_BE_GREATOR_THAN_ZERO'); ?></span>
<div id="popup_overlaydiv"></div>
<div id="popup" style="display:none;">
	<span id="is_productpopup_close"></span>
	<div id="popupinner"></div>
</div>
<div id="tell_a_friend" style="display:none;">
	<form class="productlisting_form" action="index.php" id="adminForm" name="adminForm">
        <div id="isproductlisting_sendtofriends_heading"><?php echo JText::_('SEND_TO_FRIENDS');?></div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('YOUR_NAME'); ?>" type="text" name="myname" id="myname" />
	    </div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('YOUR_EMAIL_ADDRESS'); ?>" type="text" name="myemailaddress" id="myemailaddress" />
	    </div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('FRIEND_EMAIL_ADDRESS'); ?>" type="text" name="friendemail1" id="friendemail1" />
	    </div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('FRIEND_EMAIL_ADDRESS'); ?>" type="text" name="friendemail2" id="friendemail2" />
	    </div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('FRIEND_EMAIL_ADDRESS'); ?>" type="text" name="friendemail3" id="friendemail3" />
	    </div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('PRODUCT_TITLE'); ?>" type="text" name="producttitle" id="producttitle" readonly="readonly"/>
	    </div>
	    <div class="is_form_fields_wrapper">
            <textarea placeholder="<?php echo JText::_('MESSAGE'); ?>" cols="50" style="width:100%;padding:5px 3% !important;"rows="5" maxlength="250" name="message" id="message"></textarea>
	    </div>
        <div id="is_form_submit_button">
        	<input type="hidden" name="sendtofrienditemid" id="sendtofrienditemid" value="<?php echo $this->Itemid; ?>"/>
        	<input type="hidden" name="sendtofriendpid" id="sendtofriendpid" value=""/>
            <input class="jquery-button" type="button" id="sendtofriend" value="<?php echo JText::_('SUBMIT'); ?>" />
            <input class="jquery-button" type="button" id="sendtofriendclose" value="<?php echo JText::_('CLOSE'); ?>" />
        </div>        
	</form>
</div>
<div id="shortlist_product" style="display:none;">
	<form class="productlisting_form" action="index.php" id="adminForm" name="adminForm">
	<span id="is_productpopup_close"></span>
		<div id="isproductlisting_sendtofriends_heading"><?php echo JText::_('SHORTLIST_PRODUCT');?></div>
	    <div class="is_form_fields_wrapper halfwidth">
            <input placeholder="<?php echo JText::_('PRODUCT_TITLE'); ?>" type="text" name="shortlistproducttitle" id="shortlistproducttitle" readonly="readonly" />
	    </div>
	    <div class="is_form_fields_wrapper">
            <textarea placeholder="<?php echo JText::_('COMMENT'); ?>" cols="50" style="width:100%;"rows="5" maxlength="250" name="comment" id="comment"></textarea>
	    </div>
        <div id="is_form_submit_button">
        	<input type="hidden" name="shortlistitemid" id="shortlistitemid" value="<?php echo $this->Itemid; ?>"/>
        	<input type="hidden" name="shortlistpid" id="shortlistpid" value=""/>
            <input class="jquery-button" type="button" id="shortlist" value="<?php echo JText::_('SUBMIT'); ?>" />
            <input class="jquery-button" type="button" id="shortlistclose" value="<?php echo JText::_('CLOSE'); ?>" />
        </div>        
	</form>
</div>