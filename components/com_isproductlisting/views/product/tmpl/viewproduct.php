<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.zoom.js');
$document->addScript('components/com_isproductlisting/views/product/tmpl/viewproduct.js');

$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');

$calfrm = JRequest::getVar('cl');
$productlist = JRequest::getVar('pl');
?>
<?php require_once 'popup.php'; ?>
<style>
    .zoom {display:inline-block;position: relative;}
    .zoom:after {content:'';display:block; width:33px; height:33px; position:absolute; top:0;right:0;background:url(icon.png);}
    .zoom img {display: block;}
    .zoom img::selection { background-color: transparent; }
    .zoomThumbActive{border: 1px solid #cdcaca;}
</style>
<?php if ($this->config['offline'] == '1') { ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="is_topsection">
        <div id="is_sitetitle">
            <?php echo $this->config['offline_text']; ?>
        </div>
    </div>
<?php } else { 
    if($this->config['topsection_show'] == 1):
    ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
            <?php if ($this->config['current_location'] == 1) { ?>
                <div id="is_topcurloc">
                    <?php echo JText::_('CURRENT_LOCATION');?>:&nbsp;
                    <?php if ($calfrm == 1) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=<?php echo $calfrm; ?>&Itemid=<?php echo $this->Itemid; ?>" ><?php echo JText::_('PRODUCTS');?></a> >
                    <?php } 
                        if ($calfrm == 2) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productcategories&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('PRODUCT_CATEGORIES');?></a> >
                    <?php } elseif ($calfrm == 3) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productsubcategories&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('PRODUCT_SUB_CATEGORIES');?></a> >
                    <?php } elseif ($calfrm == 4) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productbrands&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('BRANDS');?></a> >
                    <?php } ?>
                    <?php echo JText::_('VIEW_PRODUCT'); ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if($this->config['toplink_show'] == 1)
        if (sizeof($this->links) != 0) {
            echo '<div id="is_top_links">';
            foreach ($this->links as $lnk) {
                ?>
                <a class="<?php if ($lnk[2] == 1) echo 'first'; elseif ($lnk[2] == -1) echo 'last'; ?>" href="<?php echo $lnk[0]; ?>"><?php echo $lnk[1]; ?></a>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php 
    endif;
    if($this->config['headingbar_show'] == 1): ?>
        <div id="is_topheading">
            <span id="is_topheading_text">
                <span id="is_topheading_text_left"></span>
                <span id="is_topheading_text_center"><?php echo JText::_('PRODUCT_DETAIL');?></span>
                <span id="is_topheading_text_right"></span>
            </span>
        </div>
    <?php endif; ?>
    <form class="productlisting_form" action="index.php" name="adminvForm" id="adminvForm" method="post">
        <div id="is_productdetail_wrapper">
            <div id="is_product_upperpart">
                <span id="is_product_upper_producttitle" calss="<?php if($this->product->isgold == 1 && $this->product->isfeatured == 1) echo 'goldandfeatureproduct'; elseif($this->product->isgold == 1) echo 'goldproduct'; elseif($this->product->isfeatured == 1) echo 'featuredproduct'; ?>" ><?php echo $this->product->title; ?></span>
                <div id="is_product_upper_imagepart">
                    <?php if(!empty($this->productimages)) { ?>
                            <span class='zoom' id='ex1'>
                                <img width="300px" height="400px" src="<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product->id.'/thumbnail/zoom-'.$this->productimages[0]->image;?>" >
                            </span>
                            <div id="is-productimages-thumbnail" >
                            <?php $i = 0;
                                foreach($this->productimages AS $image){ ?>
                        		<div class="thumbnail"><a class="<?php if($i == 0) echo 'zoomThumbActive'; $i++;?> thumbnailimages" data-largeimage="<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product->id.'/thumbnail/zoom-'.$image->image;?>"><img width="75px" height="75px" src='<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product->id.'/thumbnail/medium-'.$image->image;?>'></a></div>
                            <?php } ?>
                            </div>
                    <?php } ?>
                </div>
                <div id="is_product_upper_datapart">
                    <span id="is_product_upper_title"><?php echo JText::_('CATEGORY'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product->cattitle; ?></span>
                    <span id="is_product_upper_title"><?php echo JText::_('SUB_CATEGORY'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product->subcattitle; ?></span>
                    <span id="is_product_upper_title"><?php echo JText::_('BRAND'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product->brandtitle; ?></span>
                    <span id="is_product_upper_title"><?php echo JText::_('QUANTITY'); ?></span>
                    <span style="display:none;" data-for="quantity_<?php echo $this->product->id; ?>"><?php echo $this->product->quantity;?></span>
                    <?php if($this->product->quantity > 0) { ?>
                        <span id="is_product_upper_value"><?php echo $this->product->quantity; ?></span>
                    <?php }else{ ?>
                        <span id="is_product_upper_value"><?php echo JText::_('NOT_AVAILABLE'); ?></span>
                    <?php } ?>
                    <?php if($this->config['showbrandlogo'] == 1) { ?>
                            <img id="is_product_imglogo" class="productdetailpage" src="<?php echo JURI::root().$this->config['data_directory']."/brands/brand_".$this->product->brandid."/logo/".$this->product->brandlogo;?>" width="50px" height="50px" />
                    <?php } ?>
                    <span id="is_product_price">
                        <?php
                        $price = $this->product->price;
                        if (($price != "") && ($price != 0)) {
                            if ($this->config['pricenotation'] == 'french')
                                $price = number_format($price, $this->config['pricedecimalpoint'], ",", " ");
                            else
                                $price = number_format($price, $this->config['pricedecimalpoint']);
                            echo '<span id="is_product_price_value">'.$this->defaultcurrency . "&nbsp;&nbsp;" . $price.'</span>';
                        }else {
                            echo '<span id="is_product_price_value">'.JText::_('PRICE_NOT_GIVEN').'</span>';
                        } ?>
                    </span>
                </div>
            </div>
            <div id="is_product_listing_bottomicons">
                <a class="is_product_listing_icons" data-for="compareproduct" data-jquery="<?php echo $this->product->id; ?>" href="index.php?option=com_isproductlisting&view=product&layout=compareproduct&pid1=<?php echo $this->product->id;?>&Itemid=<?php echo $this->Itemid;?>" title="<?php echo JText::_('COMPARE_PRODUCT'); ?>" ><img title="<?php echo JText::_('COMPARE_PRODUCT'); ?>" src="components/com_isproductlisting/images/compare.png" /><?php echo JText::_('COMPARE_PRODUCT'); ?></a>
                <a class="is_product_listing_icons" data-for="tellafriend" data-jquery="<?php echo $this->product->id; ?>" data-title="<?php echo $this->product->title; ?>" href="" title="<?php echo JText::_('TELL_A_FRIEND'); ?>" ><img title="<?php echo JText::_('TELL_A_FRIEND'); ?>" src="components/com_isproductlisting/images/tellafriend.png" /><?php echo JText::_('TELL_A_FRIEND'); ?></a>
            </div>
            <div id="is_product_lower_part">
                <span id="is_product_upper_title"><?php echo JText::_('DESCRIPTION'); ?></span>
                <span id="is_product_upper_value"><?php echo $this->product->description; ?></span>
            </div>
            <?php if(!empty($this->product->datasheet)): ?>
            <div id="is_product_lower_part">
                <span id="is_product_upper_title"><?php echo JText::_('DATA_SHEET'); ?></span>
                    <?php 
                    $array = json_decode($this->product->datasheet);
                    foreach($array AS $object){
                        echo '<span id="is_product_upper_value" class="it_datasheet">';
                        echo '<span class="is_product_datasheet_title">'.$object->title.'</span>';
                        echo '<span class="is_product_datasheet_value">'.$object->value.'</span>';
                        echo '</span>';
                    }
                    ?>
            </div>
            <?php endif; ?>
        </div>
        <div id="is_product_lower_part">
            <span id="is_product_upper_title"><?php echo JText::_('FEED_BACKS'); ?></span>
        <?php if(!$this->user->guest){ ?>
            <textarea id="feedback" name="feedback"></textarea>
            <input type="hidden" id="productid" name="productid" value="<?php echo $this->product->id; ?>" />
            <div id="is_form_submit_button">
                <input type="submit" class="jquery-button" name="<?php echo JText::_('SAVE'); ?>" onclick="submitfeedback();"/>
            </div>
        <?php } ?>
        <?php if(!empty($this->productfeedback)){ ?>
                <?php foreach($this->productfeedback AS $feedback){ ?>
                        <div class="is_product_feedback">
                            <span class="is_product_feedback_personname"><?php echo $feedback->username; ?><span class="is_product_feedback_date"><?php echo date('D, d M Y',  strtotime($feedback->created)); ?></span></span>
                            <?php echo $feedback->feed; ?>
                        </div>
                <?php } ?>
        <?php }else{ ?>
                <span class="it_product_feedback_not_found"><?php echo JText::_('NO_PRODUCT_FEEDBACK_FOUND'); ?></span>
        <?php } ?>
            </div>
            <div id="is_product_share_panel">
                <?php //Product Sharing Options
                    if($this->socailsharing['product_google_share'] == 1){?>
                          <span class="product_share_content">
                                  <a href="#" onclick="window.open('https://m.google.com/app/plus/x/?v=compose&content='+location.href,'gplusshare','width=450,height=300,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_google.png";?>" alt="Share on Google+" /></a>
                          </span>
                <?php }
                          if($this->socailsharing['product_friendfeed_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a target="_blank" href="http://www.friendfeed.com/share?title=<?php echo $document->title;?> - <?php echo JURI::current();?>" title="Share to FriendFeed"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_ff.png";?>" alt="Friend Feed" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_blog_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a href="#" onclick="window.open('http://www.blogger.com/blog_this.pyra?t&u='+location.href+'&n='+document.title, '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=550, height=440, toolbar=0, status=0');return false" title="BlogThis!"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_blog.png";?>" alt="Share on Blog" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_linkedin_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo JURI::current();?>&title=<?php echo $document->title;?>" title="Share to Linkedin"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_linkedin.png";?>" alt="Linkedid" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_myspace_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a target="_blank" href="http://www.myspace.com/Modules/PostTo/Pages/?u=<?php echo JURI::current();?>&t=<?php echo $document->title;?>" title="Share to MySpace"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_myspace.png";?>" alt="MySpace" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_twiiter_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a href="#" onclick="window.open('http://twitter.com/share?text=<?php echo $document->title;?>&url=<?php echo JURI::current();?>', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=550, height=440, toolbar=0, status=0');return false" title="Share to Twitter"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_twitter.png";?>" alt="Twitter" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_yahoo_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a target="_blank" href="http://bookmarks.yahoo.com/toolbar/savebm?u=<?php echo JURI::current();?>&t=<?php echo $document->title;?>" title="Save to Yahoo! Bookmarks"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_yahoo.png";?>" alt="Yahoo" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_digg_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a href="#" onclick="window.open('http://digg.com/submit?url='+location.href);return false" title="Share to Digg" ><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_digg.png";?>" alt="Share on Digg" /></a>
                                </span>
                <?php }
                          if($this->socailsharing['product_fb_share'] == 1){ ?>
                                <span class="product_share_content">
                                        <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u='+location.href+'&t='+document.title, '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=550, height=440, toolbar=0, status=0');return false"><img src="<?php echo JURI::root()."components/com_isproductlisting/images/share_fb.png";?>" alt="Share on facebook" /></a>
                                </span>
                <?php } ?>
            </div>
            <?php if($this->socailsharing['product_fb_comments'] == 1){ ?>				
                            <div id="product_fbcommentparent">
                                    <span id="product_fbcommentheading"><?php echo JText::_('FACEBOOK_COMMENTS');?></span>
                                    <div id="product_fbcomment">
                                            <iframe id="product_fb_comments" src="" scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:100%; max-height:400px;" allowTransparency="true"></iframe>
                                            <script>
                                                window.onload = function() {
                                                    if(document.getElementById('product_fb_comments') != null){
                                                        var myFrame = document.getElementById('product_fb_comments');
                                                        if(myFrame != null)
                                                        myFrame.src='http://www.facebook.com/plugins/comments.php?href='+location.href;
                                                    }
                                                }
                                            </script>
                                    </div>
                            </div>
            <?php } ?>
    </form>
<?php } 
    include_once JPATH_ADMINISTRATOR."/components/com_isproductlisting/views/jscr.php";
?>
