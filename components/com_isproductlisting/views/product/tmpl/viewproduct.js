jQuery(document).ready(function(){
    jQuery('a.thumbnailimages').each(function(){
        jQuery(this).click(function(e){
            e.preventDefault();
            var imgloc = jQuery(this).attr('data-largeimage');
            jQuery("span#ex1 img").each(function(){                
                jQuery(this).fadeOut(300,function(){
                  jQuery(this).attr('src', imgloc);  
              }).fadeIn(300);
            });
        });
    });
    var imagewidth = 0;
    $div = jQuery("div#is-productimages-thumbnail");
    var orgwidth = jQuery($div).width();
    localStorage.setItem("orgwidth", orgwidth);
    jQuery("div#is-productimages-thumbnail div").each(function(){
        imagewidth += jQuery(this).outerWidth(true);
    });
    jQuery($div).css({'width':imagewidth+'px'});
    localStorage.setItem("imagewidth", imagewidth);
    if(orgwidth < imagewidth){
        var html = '<span id="productimage-left"><</span><span id="productimage-right">></span>';
        jQuery("div#is-productimages-thumbnail").append(html);
        $lefthandle = jQuery("span#productimage-left");
        $righthandle = jQuery("span#productimage-right");
        jQuery($righthandle).css({'left':(orgwidth-20)+"px"});
        jQuery($lefthandle).click(function(){
            var marginleft = jQuery($div).css('margin-left');
            marginleft = parseInt(marginleft);
            var cmpmarginleft = Math.abs(marginleft);
            if(cmpmarginleft > 0){
                jQuery($div).animate({'margin-left':(marginleft+85)+"px"});
                jQuery(this).css({'left':(parseInt(jQuery(this).css('left'))-85)+"px"});
                jQuery($righthandle).css({'left':(parseInt(jQuery($righthandle).css('left'))-85)+"px"});
            }
        }).mouseenter(function(){jQuery(this).animate({'opacity':'1'})}).mouseleave(function(){jQuery(this).animate({'opacity':'0.5'})});
        jQuery($righthandle).click(function(){
            var marginleft = jQuery($div).css('margin-left');
            marginleft = parseInt(marginleft);
            var cmpmarginleft = Math.abs(marginleft);
            orgwidth = localStorage.getItem('orgwidth');
            imagewidth = localStorage.getItem('imagewidth');
            if(cmpmarginleft < (imagewidth-orgwidth)){
                jQuery($div).animate({'margin-left':(marginleft-85)+"px"});
                jQuery(this).css({'left':(parseInt(jQuery(this).css('left'))+85)+"px"});
                jQuery($lefthandle).css({'left':(parseInt(jQuery($lefthandle).css('left'))+85)+"px"});
            }
        }).mouseenter(function(){jQuery(this).animate({'opacity':'1'})}).mouseleave(function(){jQuery(this).animate({'opacity':'0.5'})});
    }
    jQuery('#ex1').zoom();


    jQuery('a.thumbnailimages-2').each(function(){
        jQuery(this).click(function(e){
            e.preventDefault();
            var imgloc = jQuery(this).attr('data-largeimage');
            jQuery("span#ex2 img").each(function(){                
                jQuery(this).fadeOut(300,function(){
                  jQuery(this).attr('src', imgloc);  
              }).fadeIn(300);
            });
        });
    });
    imagewidth = 0;
    $div2 = jQuery("div#is-productimages-thumbnail-2");
    orgwidth = jQuery($div2).width();
    localStorage.setItem("orgwidth1", orgwidth);
    jQuery("div#is-productimages-thumbnail-2 div").each(function(){
        imagewidth += jQuery(this).outerWidth(true);
    });
    jQuery($div2).css({'width':imagewidth+'px'});
    localStorage.setItem("imagewidth1", imagewidth);
    if(orgwidth < imagewidth){
        var html = '<span id="productimage-left-2"><</span><span id="productimage-right-2">></span>';
        jQuery("div#is-productimages-thumbnail-2").append(html);
        $lefthandle2 = jQuery("span#productimage-left-2");
        $righthandle2 = jQuery("span#productimage-right-2");
        jQuery($righthandle2).css({'left':(orgwidth-20)+"px"});
        jQuery($lefthandle2).click(function(){
            var marginleft = jQuery($div2).css('margin-left');
            marginleft = parseInt(marginleft);
            var cmpmarginleft = Math.abs(marginleft);
            if(cmpmarginleft > 0){
                jQuery($div2).animate({'margin-left':(marginleft+85)+"px"});
                jQuery(this).css({'left':(parseInt(jQuery(this).css('left'))-85)+"px"});
                jQuery($righthandle2).css({'left':(parseInt(jQuery($righthandle2).css('left'))-85)+"px"});
            }
        }).mouseenter(function(){jQuery(this).animate({'opacity':'1'})}).mouseleave(function(){jQuery(this).animate({'opacity':'0.5'})});
        jQuery($righthandle2).click(function(){
            var marginleft = jQuery($div2).css('margin-left');
            marginleft = parseInt(marginleft);
            var cmpmarginleft = Math.abs(marginleft);
            orgwidth = localStorage.getItem('orgwidth1');
            imagewidth = localStorage.getItem('imagewidth1');
            if(cmpmarginleft < (imagewidth-orgwidth)){
                jQuery($div2).animate({'margin-left':(marginleft-85)+"px"});
                jQuery(this).css({'left':(parseInt(jQuery(this).css('left'))+85)+"px"});
                jQuery($lefthandle2).css({'left':(parseInt(jQuery($lefthandle2).css('left'))+85)+"px"});
            }
        }).mouseenter(function(){jQuery(this).animate({'opacity':'1'})}).mouseleave(function(){jQuery(this).animate({'opacity':'0.5'})});
    }
    jQuery('#ex2').zoom();

});
function submitfeedback(){
    var feedback = jQuery("#feedback").val();
    var productid = jQuery("#productid").val();
    jQuery.post("index.php?option=com_isproductlisting&task=productlisting.savefeedback",{val:feedback,productid:productid},function(data){
        if(data){
            window.location.reload();
        }
    });
}