<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
 + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
 ^
 + Project: 		IS Product listing
 ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
JHTML::_('behavior.modal');
?>
<?php if ($this->config['offline'] == '1') { ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="is_topsection">
        <div id="is_sitetitle">
            <?php echo $this->config['offline_text']; ?>
        </div>
    </div>
<?php } else { 
    if($this->config['topsection_show'] == 1):
    ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
            <?php if ($this->config['current_location'] == 1) { ?>
                <div id="is_topcurloc">
                    <?php echo JText::_('CURRENT_LOCATION');?>:&nbsp;
                    <?php echo JText::_('NO_RECORD_FOUND'); ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if($this->config['toplink_show'] == 1)
        if (sizeof($this->links) != 0) {
            echo '<div id="is_top_links">';
            foreach ($this->links as $lnk) {
                ?>
                <a class="<?php if ($lnk[2] == 1) echo 'first'; elseif ($lnk[2] == -1) echo 'last'; ?>" href="<?php echo $lnk[0]; ?>"><?php echo $lnk[1]; ?></a>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php 
    endif;
    if($this->config['headingbar_show'] == 1): ?>
        <div id="is_topheading">
            <span id="is_topheading_text">
                <span id="is_topheading_text_left"></span>
                <span id="is_topheading_text_center"> 
                    <?php
                        echo JText::_('NO_RECORD_FOUND');
                    ?>
                </span>
                <span id="is_topheading_text_right"></span>
            </span>
        </div>
    <?php endif; ?>
    <div id="isproductlisting_fullwrapper">
        <span id="isproductlisting_norecordfound"><?php echo JText::_("NO_RECORD_FOUND"); ?></span>
    </div>
<?php } 
    include_once JPATH_ADMINISTRATOR."/components/com_isproductlisting/views/jscr.php";
?>