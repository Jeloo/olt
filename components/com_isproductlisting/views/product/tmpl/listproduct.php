<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');

$calfrm = JRequest::getVar('cl','1');
?>
<?php if ($this->config['offline'] == '1') { ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="is_topsection">
        <div id="is_sitetitle">
            <?php echo $this->config['offline_text']; ?>
        </div>
    </div>
<?php } else { 
    if($this->config['topsection_show'] == 1):
    ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
            <?php if ($this->config['current_location'] == 1) { ?>
                <div id="is_topcurloc">
                    <?php echo JText::_('CURRENT_LOCATION');?>:&nbsp;
                    <?php if ($calfrm == 1) { ?>
                    <?php } elseif ($calfrm == 2) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productcategories&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('PRODUCT_CATEGORIES');?></a> >
                    <?php } elseif ($calfrm == 3) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productsubcategories&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('PRODUCT_SUB_CATEGORIES');?></a> >
                    <?php } elseif ($calfrm == 4) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productbrands&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('BRANDS');?></a> >
                    <?php } elseif ($calfrm == 5) { ?>
                        <?php echo JText::_('FEATURED'); ?>&nbsp;
                    <?php } elseif ($calfrm == 6) { ?>
                        <?php echo JText::_('GOLD'); ?>&nbsp;
                    <?php } ?>
                    <?php echo JText::_('PRODUCTS'); ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if($this->config['toplink_show'] == 1)
        if (sizeof($this->links) != 0) {
            echo '<div id="is_top_links">';
            foreach ($this->links as $lnk) {
                ?>
                <a class="<?php if ($lnk[2] == 1) echo 'first'; elseif ($lnk[2] == -1) echo 'last'; ?>" href="<?php echo $lnk[0]; ?>"><?php echo $lnk[1]; ?></a>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php 
    endif;
    if($this->config['headingbar_show'] == 1): ?>
        <div id="is_topheading">
            <span id="is_topheading_text">
                <span id="is_topheading_text_left"></span>
                <span id="is_topheading_text_center"> 
                    <?php
                        if($calfrm == 6)
                            echo JText::_('GOLD_PRODUCTS');
                        elseif($calfrm == 5)
                            echo JText::_('FEATURED_PRODUCTS');
                        else
                            echo JText::_('PRODUCTS');
                    ?>
                </span>
                <span id="is_topheading_text_right"></span>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($this->config['productfilterenable'] == 1) { ?>
        <div id="is_productfilter">
            <?php require_once( 'product_filter.php' ); ?>
        </div>
    <?php } ?>

    <?php
    if(JVERSION < 3)
        $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
    else{
        JHtml::_('behavior.framework');
        JHtml::_('jquery.framework');    
    }
    $document->addScript('components/com_isproductlisting/views/product/tmpl/listproduct.js');
    
    $querystring = "";
    if ($this->categoryid)
        $querystring = '&catid=' . $this->categoryid;
    if ($this->subcategoryid)
        $querystring .= '&subcatid=' . $this->subcategoryid;
    if ($this->brandid)
        $querystring .= '&bid=' . $this->brandid;
    if ($this->prs)
        $querystring .= '&prs=' . $this->prs;
    if ($this->pre) 
        $querystring .= '&pre=' . $this->pre;
    if ($this->cl)
        $querystring .= '&cl=' . $this->cl;
    if ($this->isfilter)
        $querystring .= '&isfilter=' . $this->svehiclefilter;
    $querystring .= '&Itemid=' . $this->Itemid;
    $link_sort_pagination = 'index.php?option=com_isproductlisting&view=product&layout=listproduct' . $querystring;
    //echo 'list vehicle '.$querystring;
    ?>

    <?php require_once 'popup.php'; ?>
    <form class="productlisting_form" action="index.php" name="adminvForm" id="adminvForm" method="post">
        <div id="is_productlisting_wrapper">
        <?php 
            if(!empty($this->products)){
                $a = 1;
        ?>
            <?php foreach($this->products AS $product){ ?>
                    <div id="is_product_wrapper">
                        <div id="is_product_topbar">
                            <span id="is_product_title"><a href="index.php?option=com_isproductlisting&view=product&layout=viewproduct&cl=<?php echo $calfrm; ?>&pid=<?php echo $product->id;?>&Itemid=<?php echo $this->Itemid; ?>"><?php echo $product->title; ?></a></span>
                        </div>
                        <div id="is_product_centerbar">
                            <div id="is_product_image">
                                <img id="is_product_image" src="<?php echo JURI::root().$this->config['data_directory']."/products/product_".$product->id."/thumbnail/medium-".$product->productdefaultimage; ?>" />
                            </div>
                            <div id="is_product_image_right">
                                <?php if($this->config['showbrandlogo'] == 1) { ?>
                                    <img id="is_product_imglogo" src="<?php echo JURI::root().$this->config['data_directory']."/brands/brand_".$product->brandid."/logo/".$product->brandimage;?>" width="50px" height="50px" />
                                <?php } ?>
                                <div id="is_product_image_bottom">
                                    <?php 
                                        foreach($product->productimages AS $productimage){
                                            echo '<img class="is_product_productsmallimage" src="'.JURI::root().$this->config['data_directory'].'/products/product_'.$product->id.'/thumbnail/medium-'.$productimage->productimage.'" />';
                                        }
                                    ?>
                                </div>
                                <span id="is_product_price">
                                    <?php
                                    $price = $product->price;
                                    if (($price != "") && ($price != 0)) {
                                        if ($this->config['pricenotation'] == 'french')
                                            $price = number_format($price, $this->config['pricedecimalpoint'], ",", " ");
                                        else
                                            $price = number_format($price, $this->config['pricedecimalpoint']);
                                        echo '<span id="is_product_price_value">'.$this->defaultcurrency . "&nbsp;&nbsp;" . $price.'</span>';
                                    }else {
                                        echo '<span id="is_product_price_value">'.JText::_('PRICE_NOT_GIVEN').'</span>';
                                    } ?>
                                </span>
                            </div>
                        </div>
                        <div id="is_product_data_pair">
                            <span id="is_product_item_title" ><?php echo JText::_('BRAND'); ?></span>
                            <span id="is_product_item_value"><?php echo $product->brandtitle;?></span>
                        </div>
                        <div id="is_product_data_pair">
                            <span id="is_product_item_title" ><?php echo JText::_('CATEGORY'); ?></span>
                            <span id="is_product_item_value"><?php echo $product->categorytitle;?></span>
                        </div>
                        <div id="is_product_data_pair">
                            <span id="is_product_item_title" ><?php echo JText::_('SUB_CATEGORY'); ?></span>
                            <span id="is_product_item_value"><?php echo $product->subcategorytitle;?></span>
                        </div>
                        <div id="is_product_data_pair">
                            <span id="is_product_item_title" ><?php echo JText::_('QUANTITY'); ?></span>
                            <span id="is_product_item_value" data-for="quantity_<?php echo $product->id; ?>"><?php echo $product->quantity;?></span>
                        </div>
                        <div id="is_product_listing_bottomicons">
                            <a class="is_product_listing_icons" data-for="compareproduct" data-jquery="<?php echo $product->id; ?>" href="index.php?option=com_isproductlisting&view=product&layout=compareproduct&pid1=<?php echo $product->id;?>&Itemid=<?php echo $this->Itemid;?>" title="<?php echo JText::_('COMPARE_PRODUCT'); ?>" ><img title="<?php echo JText::_('COMPARE_PRODUCT'); ?>" src="components/com_isproductlisting/images/compare.png" /><?php echo JText::_('COMPARE_PRODUCT'); ?></a>
                            <a class="is_product_listing_icons" data-for="tellafriend" data-jquery="<?php echo $product->id; ?>" data-title="<?php echo $product->title; ?>" href="" title="<?php echo JText::_('TELL_A_FRIEND'); ?>" ><img title="<?php echo JText::_('TELL_A_FRIEND'); ?>" src="components/com_isproductlisting/images/tellafriend.png" /><?php echo JText::_('TELL_A_FRIEND'); ?></a>
                        </div>
                    </div>
            <?php 
                } ?>
        <?php } ?>
        <input type="hidden" name="option" value="<?php echo $this->option; ?>">
        <input type="hidden" name="vehicleshortlistcomments" id="vehicleshortlistcomments" value="">
        <input type="hidden" name="vehicleshortlistrating" id="vehicleshortlistrating" value="">
        <input type="hidden" name="vehicleshortlistid" id="vehicleshortlistid" value="">
        <input type="hidden" name="vehicleshortlistuid" id="vehicleshortlistuid" value="">
        <input type="hidden" name="vehicleshortlistrd" id="vehicleshortlistrd" value="">
        <input type="hidden" name="task" id="task" value="">
        </div>
    </form>
    <div id="is_sortby">
        <form class="productlisting_form" action="<?php echo JRoute::_($link_sort_pagination); ?>" name="sortbyFrom" id="sortbyFrom" method="post">
            <span id="sortby_text"><?php echo JText::_('SORT_BY'); ?> :
                <?php
                echo $this->sort['sort'];
                if ($this->sortorder == 'asc')
                    $img = "components/com_isproductlisting/images/sort0.png";
                else
                    $img = "components/com_isproductlisting/images/sort1.png";
                ?>
                <a href="#" onclick="submitSorting(2)"><img alt="" src="<?php echo $img ?>"></a>
            </span>
            <input type="hidden" name="lv_sortorder" id="lv_sortorder" value="<?php if ($this->sortorder == 'asc') echo 'asc'; else echo 'desc'; ?>">
        </form>
    </div>
    <div class="is_pagination">
        <form class="productlisting_form"  action="<?php echo JRoute::_($link_sort_pagination); ?>" method="post"> 
            <div id="is_pagination">
                <div id="is_pagination_pageslink">
                    <?php 
                    echo $this->pagination->getPagesLinks();
                    ?>
                </div>
                <div id="is_pagination_box">
                    <?php
                    echo JText::_('DISPLAY_#');
                    echo $this->pagination->getLimitBox();
                    ?>
                </div>
                <div id="is_pagination_counter">
                    <?php echo $this->pagination->getResultsCounter(); ?>
                </div>
            </div>
            <input type="hidden" name="lv_sortorder" id="lv_sortorder" value="<?php if ($this->sortorder == 'asc') echo 'asc'; else echo 'desc'; ?>">
        </form>
    </div>
<?php if ($this->config['showrssfeeding'] == 1) { ?>
    <div style="float:left; width:100%;text-align:right">
        <a target="_blank" href="index.php?option=com_isproductlisting&view=rss&layout=products&format=rss">
            <img src="components/com_isproductlisting/images/rss.png" />
        </a>
    </div>                        

<?php } 
} 
    include_once JPATH_ADMINISTRATOR."/components/com_isproductlisting/views/jscr.php";
?>
