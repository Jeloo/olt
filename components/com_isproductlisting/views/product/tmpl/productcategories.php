<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
JHTML::_('behavior.modal');
$calfrm = JRequest::getVar('cl');
?>
<?php if ($this->config['offline'] == '1') { ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="is_topsection">
        <div id="is_sitetitle">
            <?php echo $this->config['offline_text']; ?>
        </div>
    </div>
<?php } else { 
    if($this->config['topsection_show'] == 1):
    ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
            <?php if ($this->config['current_location'] == 1) { ?>
                <div id="is_topcurloc">
                    <?php echo JText::_('CURRENT_LOCATION');?>:&nbsp;
                    <?php if ($calfrm == 1) { ?>
                            <a href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=<?php echo $calfrm; ?>&Itemid=<?php echo $this->Itemid; ?>" ><?php echo JText::_('PRODUCTS');?></a> > 
                    <?php } elseif ($calfrm == 2) { ?>
                    <?php } elseif ($calfrm == 3) { ?>
                    <?php } elseif ($calfrm == 4) { ?>
                    <?php } elseif ($calfrm == 5) { ?>
                    <?php } elseif ($calfrm == 6) { ?>
                    <?php } elseif ($calfrm == 7) { ?>
                    <?php } elseif ($calfrm == 8) { ?>
                    <?php } ?>
                    <?php echo JText::_('PRODUCT_CATEGORIES'); ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if($this->config['toplink_show'] == 1)
        if (sizeof($this->links) != 0) {
            echo '<div id="is_top_links">';
            foreach ($this->links as $lnk) {
                ?>
                <a class="<?php if ($lnk[2] == 1) echo 'first'; elseif ($lnk[2] == -1) echo 'last'; ?>" href="<?php echo $lnk[0]; ?>"><?php echo $lnk[1]; ?></a>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php 
    endif;
    if($this->config['headingbar_show'] == 1): ?>
        <div id="is_topheading">
            <span id="is_topheading_text">
                <span id="is_topheading_text_left"></span>
                <span id="is_topheading_text_center"> 
                    <?php
                        echo JText::_('PRODUCT_CATEGORIES');
                    ?>
                </span>
                <span id="is_topheading_text_right"></span>
            </span>
        </div>
    <?php endif; ?>
    <form class="productlisting_form" action="index.php" name="adminvForm" id="adminvForm" method="post">
        <div id="is_product_wrapper" class="full_width text_center">
            <?php 
            if(!empty($this->categories))
            foreach($this->categories AS $category){?>
                    <div class="is_product_category_wrapper" data-categoryid="<?php echo $category->id; ?>">
                        <?php 
                            if(!empty($category->image)){
                                $image = JURI::root().$this->config['data_directory'].'/categories/category_'.$category->id.'/logo/'.$category->image;
                                if(@fopen($image, r) == false) 
                                    $image = JURI::root().'components/com_isproductlisting/images/noimage.png';
                            }else{
                                $image = JURI::root().'components/com_isproductlisting/images/noimage.png';
                            }
                        ?>
                        <img class="is_product_category_image" data-jquery-class="is_product_categories_images" data-categoryid="<?php echo $category->id;?>" data-categorytitle="<?php echo $category->title;?>" src="<?php echo $image; ?>" width="100px" height="100px"/>
                        <span class="is_product_category_title" ><a href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=2&plink=1&catid=<?php echo $category->id; ?>&Itemid=<?php echo $this->Itemid; ?>" ><?php echo $category->title; ?><?php /*<span class="is_product_category_totalproduct"><?php echo $category->totalproduct; ?></span> */ ?></a></span>
                    </div>
            <?php } ?>
        </div>
        <input type="hidden" id="Itemid" name="Itemid" value="<?php echo $this->Itemid; ?>" />
    </form>
<?php } 
    include_once JPATH_ADMINISTRATOR."/components/com_isproductlisting/views/jscr.php";
?>
<script>
    /*
    jQuery(document).ready(function(){
        jQuery('img[data-jquery-class="is_product_categories_images"]').each(function(){
            var mouseover = false;
            var maindiv = false;
            var timeout = false;
            var itemid = jQuery("input#Itemid").val();
           jQuery(this).mouseover(function(){
                var catid = jQuery(this).attr('data-categoryid');
                var cattitle = jQuery(this).attr('data-categorytitle');
                if(jQuery("div#is_product_category_data_"+catid).length == 0){
                    timeout = setTimeout(function(){
                       mouseover = true;
                       maindiv = jQuery('<div>').attr('id','is_product_category_data_'+catid).addClass('is_productlisting_category_topproduct').css({'position':'absolute','left':'10px','bottom':'8px'});
                       var spantitle = jQuery('<span>').addClass('is_productlisting_category_topproductheading').html(cattitle);
                       var spantitlecorner = jQuery('<span>').addClass('is_productlisting_category_topproductheading_corner');
                       jQuery(maindiv).append(spantitle);
                       jQuery(maindiv).append(spantitlecorner);
                       jQuery('div.is_product_category_wrapper[data-categoryid="'+catid+'"]').append(maindiv);
                       jQuery.post('index.php?option=com_isproductlisting&task=gettopproductsbycategories',{catid:catid,itemid:itemid},function(data){
                           if(data){
                               jQuery(maindiv).append(data).css({'bottom':"-"+(jQuery(maindiv).outerHeight()-40)+"px"});
                           }
                       });
                   },800);
                }
           }).mouseleave(function(){
               if(mouseover == false)
                   clearTimeout(timeout);
               if(jQuery(maindiv).length > 0){
                    if(!jQuery(maindiv).is(":hover")){
                        jQuery(maindiv).remove();
                        mouseover = false;
                    }
                    jQuery(maindiv).mouseleave(function(){
                        jQuery(maindiv).remove();
                        mouseover = false;
                    });
               }
           }); 
        });
    });
    */
</script>