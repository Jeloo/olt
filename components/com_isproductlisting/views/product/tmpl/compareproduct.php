<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.zoom.js');
$document->addScript('components/com_isproductlisting/views/product/tmpl/viewproduct.js');
$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.tokeninput.js');

$calfrm = JRequest::getVar('cl');
$productlist = JRequest::getVar('pl');
?>
<?php require_once 'popup.php'; ?>
<style>
    .zoom {display:inline-block;position: relative;}
    .zoom:after {content:'';display:block; width:33px; height:33px; position:absolute; top:0;right:0;background:url(icon.png);}
    .zoom img {display: block;}
    .zoom img::selection { background-color: transparent; }
/*    .zoomThumbActive{border: 1px solid #cdcaca;} */
</style>
<?php if ($this->config['offline'] == '1') { ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="is_topsection">
        <div id="is_sitetitle">
            <?php echo $this->config['offline_text']; ?>
        </div>
    </div>
<?php } else { 
    if($this->config['topsection_show'] == 1):
    ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
            <?php if ($this->config['current_location'] == 1) { ?>
                <div id="is_topcurloc">
                    <?php echo JText::_('CURRENT_LOCATION');?>:&nbsp;
                    <?php if ($calfrm == 1 || $productlist == 1) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=<?php echo $calfrm; ?>&Itemid=<?php echo $this->Itemid; ?>" ><?php echo JText::_('PRODUCTS');?></a> >
                    <?php } 
                        if ($calfrm == 2) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productcategories&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('PRODUCT_CATEGORIES');?></a> >
                    <?php } elseif ($calfrm == 3) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productsubcategories&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('PRODUCT_SUB_CATEGORIES');?></a> >
                    <?php } elseif ($calfrm == 4) { ?>
                        <a href="index.php?option=com_isproductlisting&view=product&layout=productbrands&Itemid=<?php echo $this->Itemid;?>"><?php echo JText::_('BRANDS');?></a> >
                    <?php } elseif ($calfrm == 5) { ?>
                    <?php } elseif ($calfrm == 6) { ?>
                    <?php } elseif ($calfrm == 7) { ?>
                    <?php } elseif ($calfrm == 8) { ?>
                    <?php } ?>
                    <?php echo JText::_('COMPARE_PRODUCT'); ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if($this->config['toplink_show'] == 1)
        if (sizeof($this->links) != 0) {
            echo '<div id="is_top_links">';
            foreach ($this->links as $lnk) {
                ?>
                <a class="<?php if ($lnk[2] == 1) echo 'first'; elseif ($lnk[2] == -1) echo 'last'; ?>" href="<?php echo $lnk[0]; ?>"><?php echo $lnk[1]; ?></a>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php 
    endif;
    if($this->config['headingbar_show'] == 1): ?>
        <div id="is_topheading">
            <span id="is_topheading_text">
                <span id="is_topheading_text_left"></span>
                <span id="is_topheading_text_center"><?php echo JText::_('COMPARE_PRODUCT');?></span>
                <span id="is_topheading_text_right"></span>
            </span>
        </div>
    <?php endif; ?>
    <form class="productlisting_form" action="index.php" name="adminForm" id="adminForm" method="post">
        <div id="is_productdetail_wrapper" class="comparefield">
            <div id="is_productdetail_comparefield_wrapper">
                <div id="is_productdetail_comaprefield_left">
                    <input type="hidden" id="pid1" name="pid1" value="<?php if(isset($this->product1->id)) echo $this->product1->id; ?>" />
                    <input type="text" id="compare_search1" name="compare_search1" value="" />
                </div>
                <div id="is_productdetail_comaprefield_right">
                    <input type="hidden" id="pid2" name="pid2" value="<?php if(isset($this->product2->id)) echo $this->product2->id; ?>" />
                    <input type="text" id="compare_search2" name="compare_search2" value="" />
                </div>
            </div>
            <div id="is_form_submit_button">
                <input type="submit" class="jquery-button" value="<?php echo JText::_('COMPARE'); ?>" />
            </div>
        </div>
      <?php /***** Product One ***********/ ?>
        <div id="is_productdetail_wrapper" class="compare_product">
            <?php if(!empty($this->product1)){ ?>
            <div id="is_product_upperpart" class="compare_product">
                <span id="is_product_upper_producttitle" calss="<?php if($this->product1->isgold == 1 && $this->product1->isfeatured == 1) echo 'goldandfeatureproduct'; elseif($this->product1->isgold == 1) echo 'goldproduct'; elseif($this->product1->isfeatured == 1) echo 'featuredproduct'; ?>" ><?php echo $this->product1->title; ?></span>
                <?php if($this->config['showbrandlogo'] == 1) { ?>
                        <img id="is_product_imglogo" class="productdetailpage" src="<?php echo JURI::root().$this->config['data_directory']."/brands/brand_".$this->product1->brandid."/logo/".$this->product1->brandlogo;?>" width="50px" height="50px" />
                <?php } ?>
                <div id="is_product_upper_imagepart" class="compare_product">
                    <?php if(!empty($this->productimages1)) { ?>
                            <span class='zoom' id='ex1'>
                                <img width="300px" height="400px" src="<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product1->id.'/thumbnail/zoom-'.$this->productimages1[0]->image;?>" >
                            </span>
                            <div id="is-productimages-thumbnail" >
                            <?php $i = 0;
                                foreach($this->productimages1 AS $image){ ?>
                        		<div class="thumbnail"><a class="<?php if($i == 0) echo 'zoomThumbActive'; $i++;?> thumbnailimages" data-largeimage="<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product1->id.'/thumbnail/zoom-'.$image->image;?>"><img width="75px" height="75px" src='<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product1->id.'/thumbnail/medium-'.$image->image;?>'></a></div>
                            <?php } ?>
                            </div>
                    <?php } ?>
                </div>
                <div id="is_product_listing_bottomicons">
                    <a class="is_product_listing_icons" data-for="tellafriend" data-jquery="<?php echo $this->product->id; ?>" data-title="<?php echo $this->product1->title; ?>" href="" title="<?php echo JText::_('TELL_A_FRIEND'); ?>" ><img title="<?php echo JText::_('TELL_A_FRIEND'); ?>" src="components/com_isproductlisting/images/tellafriend.png" /></a>
                </div>
                <div id="is_product_upper_datapart" class="compare_product">
                    <span id="is_product_upper_title"><?php echo JText::_('CATEGORY'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product1->cattitle; ?></span>
                    <span id="is_product_upper_title"><?php echo JText::_('SUB_CATEGORY'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product1->subcattitle; ?></span>
                    <span id="is_product_upper_title"><?php echo JText::_('BRAND'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product1->brandtitle; ?></span>
                    <span id="is_product_upper_title"><?php echo JText::_('QUANTITY'); ?></span>
                    <span style="display:none;" data-for="quantity_<?php echo $this->product1->id; ?>"><?php echo $this->product1->quantity;?></span>
                    <?php if($this->product1->quantity > 0) { ?>
                    <span id="is_product_upper_value"><?php echo $this->product1->quantity; ?></span>
                    <?php }else{ ?>
                        <span id="is_product_upper_value"><?php echo JText::_('NOT_AVAILABLE'); ?></span>
                    <?php } ?>
                </div>
            </div>
            <div id="is_product_lower_part" class="compare_product">
                <span id="is_product_upper_title"><?php echo JText::_('DESCRIPTION'); ?></span>
                <span id="is_product_upper_value"><?php echo $this->product1->description; ?></span>
                <?php if(!empty($this->product1->datasheet)): ?>
                <div id="is_product_lower_part">
                    <span id="is_product_upper_title"><?php echo JText::_('DATA_SHEET'); ?></span>
                        <?php 
                        $array = json_decode($this->product1->datasheet);
                        foreach($array AS $object){
                            echo '<span id="is_product_upper_value" class="it_datasheet">';
                            echo '<span class="is_product_datasheet_title">'.$object->title.'</span>';
                            echo '<span class="is_product_datasheet_value">'.$object->value.'</span>';
                            echo '</span>';
                        }
                        ?>
                </div>
                <?php endif; ?>
            </div>
            <?php /*if(!empty($this->productfeedback1)){ ?>
                <div id="is_product_wrapper_feedback" >
                    <span class="is_product_feedback_title"><?php echo JText::_('FEED_BACKS'); ?></span>
                    <?php foreach($this->productfeedback1 AS $feedback){ ?>
                            <div class="is_product_feedback">
                                <span class="is_product_feedback_personname"><?php echo $feedback->username; ?>&nbsp;:</span>
                                <?php echo $feedback->feed; ?>
                            </div>
                    <?php } ?>
                </div>
            <?php }*/ ?>
        <?php } ?>
        </div>
      <?php /***** Product One End ***********/ ?>
      <?php /***** Product Two ***********/ ?>
        <div id="is_productdetail_wrapper" class="compare_product left">
            <?php if(!empty($this->product2)) { ?>
            <div id="is_product_upperpart" class="compare_product">
                    <span id="is_product_upper_producttitle" calss="<?php if($this->product2->isgold == 1 && $this->product2->isfeatured == 1) echo 'goldandfeatureproduct'; elseif($this->product2->isgold == 1) echo 'goldproduct'; elseif($this->product2->isfeatured == 1) echo 'featuredproduct'; ?>" ><?php echo $this->product2->title; ?></span>
                <?php if($this->config['showbrandlogo'] == 1) { ?>
                        <img id="is_product_imglogo" class="productdetailpage" src="<?php echo JURI::root().$this->config['data_directory']."/brands/brand_".$this->product2->brandid."/logo/".$this->product2->brandlogo;?>" width="50px" height="50px" />
                <?php } ?>
                <div id="is_product_upper_imagepart" class="compare_product">
                    <?php if(!empty($this->productimages2)) { ?>
                            <span class='zoom' id='ex2'>
                                <img width="300px" height="400px" src="<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product2->id.'/thumbnail/zoom-'.$this->productimages2[0]->image;?>" >
                            </span>
                            <div id="is-productimages-thumbnail-2" >
                            <?php $i = 0;
                                foreach($this->productimages2 AS $image){ ?>
                            <div class="thumbnail"><a class="<?php if($i == 0) echo 'zoomThumbActive'; $i++;?> thumbnailimages-2" data-largeimage="<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product2->id.'/thumbnail/zoom-'.$image->image;?>"><img width="75px" height="75px" src='<?php  echo JURI::root().$this->config['data_directory'].'/products/product_'.$this->product2->id.'/thumbnail/medium-'.$image->image;?>'></a></div>
                            <?php } ?>
                            </div>
                    <?php } ?>
                </div>
                <div id="is_product_listing_bottomicons">
                    <a class="is_product_listing_icons" data-for="tellafriend" data-jquery="<?php echo $this->product2->id; ?>" data-title="<?php echo $this->product2->title; ?>" href="" title="<?php echo JText::_('TELL_A_FRIEND'); ?>" ><img title="<?php echo JText::_('TELL_A_FRIEND'); ?>" src="components/com_isproductlisting/images/tellafriend.png" /></a>
                </div>
                <div id="is_product_upper_datapart" class="compare_product">
                        <span id="is_product_upper_title"><?php echo JText::_('CATEGORY'); ?></span>
                        <span id="is_product_upper_value"><?php echo $this->product2->cattitle; ?></span>
                        <span id="is_product_upper_title"><?php echo JText::_('SUB_CATEGORY'); ?></span>
                        <span id="is_product_upper_value"><?php echo $this->product2->subcattitle; ?></span>
                        <span id="is_product_upper_title"><?php echo JText::_('BRAND'); ?></span>
                        <span id="is_product_upper_value"><?php echo $this->product2->brandtitle; ?></span>
                        <span id="is_product_upper_title"><?php echo JText::_('QUANTITY'); ?></span>
                        <span style="display:none;" data-for="quantity_<?php echo $this->product2->id; ?>"><?php echo $this->product2->quantity;?></span>
                        <?php if($this->product2->quantity > 0) { ?>
                        <span id="is_product_upper_value"><?php echo $this->product2->quantity; ?></span>
                        <?php }else{ ?>
                            <span id="is_product_upper_value"><?php echo JText::_('NOT_AVAILABLE'); ?></span>
                        <?php } ?>
                </div>
            </div>
            <div id="is_product_lower_part" class="compare_product">
                    <span id="is_product_upper_title"><?php echo JText::_('DESCRIPTION'); ?></span>
                    <span id="is_product_upper_value"><?php echo $this->product2->description; ?></span>
                    <?php if(!empty($this->product2->datasheet)): ?>
                    <div id="is_product_lower_part">
                        <span id="is_product_upper_title"><?php echo JText::_('DATA_SHEET'); ?></span>
                            <?php 
                            $array = json_decode($this->product2->datasheet);
                            foreach($array AS $object){
                                echo '<span id="is_product_upper_value" class="it_datasheet">';
                                echo '<span class="is_product_datasheet_title">'.$object->title.'</span>';
                                echo '<span class="is_product_datasheet_value">'.$object->value.'</span>';
                                echo '</span>';
                            }
                            ?>
                    </div>
                    <?php endif; ?>
            </div>
            <?php /*if(!empty($this->productfeedback2)){ ?>
                <div id="is_product_wrapper_feedback" >
                    <span class="is_product_feedback_title"><?php echo JText::_('FEED_BACKS'); ?></span>
                    <?php foreach($this->productfeedback2 AS $feedback){ ?>
                            <div class="is_product_feedback">
                                <span class="is_product_feedback_personname"><?php echo $feedback->username; ?>&nbsp;:</span>
                                <?php echo $feedback->feed; ?>
                            </div>
                    <?php } ?>
                </div>
            <?php }*/ ?>
        <?php } ?>
        </div>
        <input type="hidden" name="option" id="option" value="com_isproductlisting" />
        <input type="hidden" name="Itemid" id="Itemid" value="<?php echo $this->Itemid; ?>" />
        <input type="hidden" name="view" id="view" value="product" />
        <input type="hidden" name="layout" id="layout" value="compareproduct" />
    </form>
<?php } 
    include_once JPATH_ADMINISTRATOR."/components/com_isproductlisting/views/jscr.php";
?>
<script type="text/javascript">
jQuery(document).ready(function() {
    var value = jQuery("#pid1").val();
    if(value != ""){
        jQuery("#compare_search1").tokenInput("<?php echo JURI::root()."index.php?option=com_isproductlisting&task=autocompleteproduct";?>", {
            theme: "isproductlisting",
            width:jQuery("div#is_productdetail_comaprefield_left").width(),
            preventDuplicates: true,
            hintText: "<?php echo JText::_('TYPE_IN_A_SEARCH_TERM'); ?>",
            noResultsText: "<?php echo JText::_('NO_RESULTS'); ?>",
            searchingText: "<?php echo JText::_('SEARCHING...');?>",
            tokenLimit: 1,
            prePopulate: [
                {
                    id: "<?php if(isset($this->product1->id)) echo $this->product1->id; ?>", 
                    name: "<?php 
                                if(isset($this->product1->id)){
                                    $data = '<div id=\"isproductlisting_compareproduct\"><table width=\"100%\" id=\"isproductlisting_compareproducttable\"><tr><td width=\"50px\">';
                                    if(!empty($this->productimages1[0]->image)) $data .= '<img class=\"isproductlisting_comparevehiclelistimage\" width=\"50px\" height=\"50px\"  src=\"'.JURI::root().$this->config['data_directory'].'/products/product_'.$this->product1->id.'/thumbnail/medium-'.$this->productimages1[0]->image.'\" />';
                                    else $data .= '<img class=\"isproductlisting_comparevehiclelistimage\" src=\"'.JURI::root().'components/com_isproductlisting/images/productdefaultimage.png\" width=\"50px\" height=\"50px\" title=\"picture\"  />';
                                    $data .= '</td><td width=\"100%\" valign=\"top\"><span id=\"isproductlisting_comparevehiclelisttitle\" > '.$this->product1->title.'</span>';
                                    $data .= '<span id=\"isproductlisting_comparevehiclelistbrand\">'.$this->product1->brandtitle.'</span>';
                                    $data .= '<span id=\"isproductlisting_comparevehiclelistprice\" >'.$this->defaultcurrency.' '.$this->product1->price.'</span>';
                                    $data .= '</td></tr></table></div>';
                                    echo $data;
                                }
                            ?>"
                }
            ]
                                            
        });
        
    }else{
        jQuery("#compare_search1").tokenInput("<?php echo JURI::root()."index.php?option=com_isproductlisting&task=autocompleteproduct";?>", {
            theme: "isproductlisting",
            width:jQuery("div#is_productdetail_comaprefield_left").width(),
            preventDuplicates: true,
            hintText: "<?php echo JText::_('TYPE_IN_A_SEARCH_TERM'); ?>",
            noResultsText: "<?php echo JText::_('NO_RESULTS'); ?>",
            searchingText: "<?php echo JText::_('SEARCHING...');?>",
            tokenLimit: 1

        });
    }
    var value = jQuery("#pid2").val();
    if(value != ""){
        jQuery("#compare_search2").tokenInput("<?php echo JURI::root()."index.php?option=com_isproductlisting&task=autocompleteproduct";?>", {
            theme: "isproductlisting",
            width:jQuery("div#is_productdetail_comaprefield_right").width(),
            preventDuplicates: true,
            hintText: "<?php echo JText::_('TYPE_IN_A_SEARCH_TERM'); ?>",
            noResultsText: "<?php echo JText::_('NO_RESULTS'); ?>",
            searchingText: "<?php echo JText::_('SEARCHING...');?>",
            tokenLimit: 1,
            prePopulate: [
                {
                    id: "<?php if(isset($this->product2->id)) echo $this->product2->id; ?>", 
                    name: "<?php 
                                if(isset($this->product2->id)){
                                    $data = '<div id=\"isproductlisting_compareproduct\"><table width=\"100%\" id=\"isproductlisting_compareproducttable\"><tr><td width=\"50px\">';
                                    if(!empty($this->productimages2[0]->image)) $data .= '<img class=\"isproductlisting_comparevehiclelistimage\" width=\"50px\" height=\"50px\"  src=\"'.JURI::root().$this->config['data_directory'].'/products/product_'.$this->product2->id.'/thumbnail/medium-'.$this->productimages2[0]->image.'\" />';
                                    else $data .= '<img class=\"isproductlisting_comparevehiclelistimage\" src=\"'.JURI::root().'components/com_isproductlisting/images/productdefaultimage.png\" width=\"50px\" height=\"50px\" title=\"picture\"  />';
                                    $data .= '</td><td width=\"100%\" valign=\"top\"><span id=\"isproductlisting_comparevehiclelisttitle\" > '.$this->product2->title.'</span>';
                                    $data .= '<span id=\"isproductlisting_comparevehiclelistbrand\">'.$this->product2->brandtitle.'</span>';
                                    $data .= '<span id=\"isproductlisting_comparevehiclelistprice\" >'.$this->defaultcurrency.' '.$this->product2->price.'</span>';
                                    $data .= '</td></tr></table></div>';
                                    echo $data;
                                }
                            ?>"
                }
            ]
                                            
        });
        
    }else{
        jQuery("#compare_search2").tokenInput("<?php echo JURI::root()."index.php?option=com_isproductlisting&task=autocompleteproduct";?>", {
            theme: "isproductlisting",
            width:jQuery("div#is_productdetail_comaprefield_right").width(),
            preventDuplicates: true,
            hintText: "<?php echo JText::_('TYPE_IN_A_SEARCH_TERM'); ?>",
            noResultsText: "<?php echo JText::_('NO_RESULTS'); ?>",
            searchingText: "<?php echo JText::_('SEARCHING...');?>",
            tokenLimit: 1

        });
    }
    jQuery("div.isproductlisting-input-dropdown-isproductlisting").each(function(){        
        var width = parseInt(jQuery(this).width()) - 2;
        jQuery(this).width(width);
    });
});

</script>
