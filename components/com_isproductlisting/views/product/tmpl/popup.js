jQuery(document).ready(function(){
	jQuery('a[data-for="tellafriend"]').each(function(){
		var anchor_object = jQuery(this);
		jQuery(anchor_object).click(function(e){
			e.preventDefault();
			jQuery("div#popup_overlaydiv").fadeIn("slow");
			jQuery("div#tell_a_friend").slideDown("slow");
			jQuery("input#sendtofriendpid").val(jQuery(this).attr('data-jquery'));
			jQuery("input#producttitle").val(jQuery(this).attr('data-title'));
		});
	});
	jQuery("input#sendtofriendclose").click(function(){
		jQuery("div#tell_a_friend").slideUp("slow");
		jQuery("div#popup_overlaydiv").fadeOut("slow");
	});
	jQuery("input#sendtofriend").click(function(){		
		var error = false;
		var myname = jQuery("input#myname").val();
		if(jQuery("input#myname").val().length == 0){
			jQuery("input#myname").addClass("invalid");
			error = true;
		}			
		var myemailaddress = jQuery("input#myemailaddress").val();
		if(jQuery("input#myemailaddress").val().length == 0 || !IsEmail(jQuery("input#myemailaddress").val())){
			jQuery("input#myemailaddress").addClass("invalid");
			error = true;
		}			
		var friendemail1 = jQuery("input#friendemail1").val();
		if(jQuery("input#friendemail1").val().length == 0 || !IsEmail(jQuery("input#friendemail1").val())){
			jQuery("input#friendemail1").addClass("invalid");
			error = true;
		}			
		var friendemail2 = jQuery("input#friendemail2").val();
		var friendemail3 = jQuery("input#friendemail3").val();
		var sendtofrienditemid = jQuery("input#sendtofrienditemid").val();
		var sendtofriendpid = jQuery("input#sendtofriendpid").val();
		var message = jQuery("textarea#message").val();
		if(jQuery("textarea#message").val().length == 0){
			jQuery("textarea#message").addClass("invalid");
			error = true;
		}
		if(error == true)
			return false;

		jQuery("div#tell_a_friend").animate({"opacity":"0.7"});
		jQuery.post("index.php?option=com_isproductlisting&task=sendtofriend",{sendtofriendpid:sendtofriendpid,sendtofrienditemid:sendtofrienditemid,myname:myname,myemail:myemailaddress,friendemail1:friendemail1,friendemail2:friendemail2,friendemail3:friendemail3,message:message},function(data){
			if(data){
				jQuery("div#tell_a_friend").fadeOut("slow");
				jQuery("div#tell_a_friend").animate({"opacity":"1"});
				jQuery("div#popup_overlaydiv").fadeOut("slow");
			}
		});
	});
    function IsEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
	jQuery("span#is_productpopup_close").click(function(){
		jQuery("div#popup").slideUp("slow");
		jQuery("div#popup_overlaydiv").fadeOut("slow");
	});

	jQuery('a[data-for="addtoshortlist"]').each(function(){
		var anchor_object = jQuery(this);
		jQuery(anchor_object).click(function(e){
			e.preventDefault();
			jQuery("div#popup_overlaydiv").fadeIn("slow");
			jQuery("div#shortlist_product").slideDown("slow");
			jQuery("input#shortlistpid").val(jQuery(this).attr('data-jquery'));
			jQuery("input#shortlistproducttitle").val(jQuery(this).attr('data-title'));
		});
	});
	jQuery("input#shortlistclose").click(function(){
		jQuery("div#shortlist_product").slideUp("slow");
		jQuery("div#popup_overlaydiv").fadeOut("slow");
	});
	jQuery("input#shortlist").click(function(){		
		var error = false;
		var comment = jQuery("textarea#comment").val();
		if(jQuery("textarea#comment").val().length == 0){
			jQuery("textarea#comment").addClass("invalid");
			error = true;
		}			
		var shortlistitemid = jQuery("input#shortlistitemid").val();
		var shortlistpid = jQuery("input#shortlistpid").val();
		if(error == true)
			return false;

		jQuery("div#shortlist_product").animate({"opacity":"0.7"});
		jQuery.post("index.php?option=com_isproductlisting&task=shortlistproduct",{shortlistpid:shortlistpid,shortlistitemid:shortlistitemid,comment:comment},function(data){
			if(data){
				jQuery("div#shortlist_product").fadeOut("slow");
				jQuery("div#shortlist_product").animate({"opacity":"1"});
				jQuery("div#popup_overlaydiv").fadeOut("slow");
			}
		});
	});

});