<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/jquery-ui.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/chosen.cbo.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/prism.cbo.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/style.cbo.css');
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery-ui.js');
$document->addScript('administrator/components/com_isproductlisting/include/js/chosen.cbo.js');
$document->addScript('administrator/components/com_isproductlisting/include/js/prism.cbo.js');

$style = '';

?>
<form class="productlisting_form" action=""  name="adminFormproductfilter" id="adminFormproductfilter" method="post">
    <div id="filter_outer">
        <div id="filter_inner">
            <?php if ($this->filterconfig['show_title_filter'] == 1) { ?>
                <div id="is_filter_object">
                    <div class="object_item_title"><input type="text" name="ftitle" id="ftitle" value="<?php echo ($this->filter['title'])? $this->filter['title']: ''; ?>" placeholder="<?php echo JText::_('TYPE_TITLE_FOR_SEARCH'); ?>" /></div>
                </div>
            <?php } ?>  
            <?php if ($this->filterconfig['show_category_filter'] == 1) { ?>
                <div id="is_filter_object">
                    <div class="object_item_title"><?php echo $this->filter['categories']; ?></div>
                </div>
            <?php } ?>  
            <?php if ($this->filterconfig['show_subcategory_filter'] == 1) { ?>
                <div id="is_filter_object">
                    <div id="is_filter_subcategories" class="object_item_title"><?php echo $this->filter['subcategories']; ?></div>
                </div>
            <?php } ?>  
            <?php if ($this->filterconfig['show_brand_filter'] == 1) { ?>
                <div id="is_filter_object">
                    <div class="object_item_title"><?php echo $this->filter['brands']; ?></div>
                </div>
            <?php } ?>  
        </div>
    </div>
    <div id="filter_outer">
        <div id="filter_inner">
            <div id="filter_button">
                <button class="jquery-button" onclick="return formSubmit()" ><?php echo JText::_('GO'); ?></button>
                <button class="jquery-button" onclick="return myReset();"><?php echo JText::_('RESET'); ?></button>
            </div>
        </div>
    </div>

    <input type="hidden" name="option" value="<?php echo $this->option; ?>">
    <input type="hidden" name="view" value="product">
    <input type="hidden" name="layout" value="listproduct">
    <input type="hidden" name="cl" value="<?php echo $this->cl; ?>">
</form>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("button.jquery-button").each(function(){
            jQuery(this).button();
        });
        jQuery("input#ftitle:text").addClass("ui-corner-all")
                                   .button()
                                   .css({
                                        'font' : 'inherit',
                                        'color' : 'inherit',
                                        'text-align' : 'left',
                                        'outline' : 'none',
                                        'cursor' : 'text',
                                        'padding': '1px 4px',
                                        'height' : '25px'
                                    });
        jQuery("select.jquery-cbo").each(function(){
            jQuery(this).chosen();
        });
    });
    function myReset() {
        jQuery("input#ftitle").val('');
        jQuery("select#fcatid").val('');
        jQuery("select#fsubcatid").val('');
        jQuery("select#fbid").val('');

        document.adminFormvehiclefilter.submit(); 
    }
    function getSubCategoriesByCategory(object){        
        jQuery.post('index.php?option=com_isproductlisting&task=getsubcategoriesbycategory',{id:object.value},function(data){
            if(data){
                jQuery("div#is_filter_subcategories").html(data);
                jQuery("select#fsubcatid").chosen();
            }
        });
    }
</script>
