<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleDeclaration($this->css);
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/jquery-ui.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/chosen.cbo.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/prism.cbo.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/style.cbo.css');
$document->addStyleSheet('administrator/components/com_isproductlisting/include/css/nouislider.fox.css');
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.nouislider.js');
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery-ui.js');
$document->addScript('administrator/components/com_isproductlisting/include/js/chosen.cbo.js');
$document->addScript('administrator/components/com_isproductlisting/include/js/prism.cbo.js');

?>
<?php if ($this->config['offline'] == '1') { ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="is_topsection">
        <div id="is_sitetitle">
            <?php echo $this->config['offline_text']; ?>
        </div>
    </div>
<?php } else { 
    if($this->config['topsection_show'] == 1):
    ?>
    <div id="is_toppanel">
        <div id="is_topsection">
            <?php if ($this->config['showtitle'] == 1) { ?>
                <div id="is_sitetitle">
                    <?php echo $this->config['title']; ?>
                </div>
            <?php } ?>
            <?php if ($this->config['current_location'] == 1) { ?>
                <div id="is_topcurloc">
                    <?php echo JText::_('CURRENT_LOCATION');?>:&nbsp;
                    <?php echo JText::_('PRODUCT_SEARCH'); ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if($this->config['toplink_show'] == 1)
        if (sizeof($this->links) != 0) {
            echo '<div id="is_top_links">';
            foreach ($this->links as $lnk) {
                ?>
                <a class="<?php if ($lnk[2] == 1) echo 'first'; elseif ($lnk[2] == -1) echo 'last'; ?>" href="<?php echo $lnk[0]; ?>"><?php echo $lnk[1]; ?></a>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php 
    endif;
    if($this->config['headingbar_show'] == 1): ?>
        <div id="is_topheading">
            <span id="is_topheading_text">
                <span id="is_topheading_text_left"></span>
                <span id="is_topheading_text_center"><?php echo JText::_('PRODUCT_SEARCH'); ?></span>
                <span id="is_topheading_text_right"></span>
            </span>
        </div>
    <?php endif; ?>
<form class="productlisting_form" action="index.php"  name="adminForm" id="adminForm" method="post">
    <div id="is_form_wraper">
            <?php if ($this->searchconfig['search_title'] == 1) { ?>
                <div class="is_form_fields_wrapper">
                    <div class="is_form_fields_title">
                        <?php echo JText::_('TITLE'); ?>
                    </div>
                    <div class="is_form_fields_value">
                        <input type="text" name="title" id="title" value="" />
                    </div>
                </div>
            <?php } ?>  
            <?php if ($this->searchconfig['search_category'] == 1) { ?>
                <div class="is_form_fields_wrapper">
                    <div class="is_form_fields_title">
                        <?php echo JText::_('CATEGORY'); ?>
                    </div>
                    <div class="is_form_fields_value">
                        <?php echo $this->lists['categories']; ?>
                    </div>
                </div>
            <?php } ?>  
            <?php if ($this->searchconfig['search_subcategory'] == 1) { ?>
                <div class="is_form_fields_wrapper">
                    <div class="is_form_fields_title">
                        <?php echo JText::_('SUB_CATEGORY'); ?>
                    </div>
                    <div class="is_form_fields_value" id="is_form_subcategories">
                        <?php echo $this->lists['subcategories']; ?>
                    </div>
                </div>
            <?php } ?>  
            <?php if ($this->searchconfig['search_brand'] == 1) { ?>
                <div class="is_form_fields_wrapper">
                    <div class="is_form_fields_title">
                        <?php echo JText::_('BRAND'); ?>
                    </div>
                    <div class="is_form_fields_value">
                        <?php echo $this->lists['brands']; ?>
                    </div>
                </div>
            <?php } ?>  
            <?php if ($this->searchconfig['search_price'] == 1) { ?>
                <div class="is_form_fields_wrapper">
                    <div class="is_form_fields_title">
                        <?php echo JText::_('PRICE'); ?>
                    </div>
                    <div class="is_form_fields_value">
                        <div id="productlisting_slider-range"></div>
                        <span id="priceamountlabel"></span>
                    </div>
                </div>
            <?php } ?>  
            <?php if ($this->searchconfig['search_quantity'] == 1) { ?>
                <div class="is_form_fields_wrapper">
                    <div class="is_form_fields_title">
                        <?php echo JText::_('QUANTITY'); ?>
                    </div>
                    <div class="is_form_fields_value">
                        <input type="text" name="quantity" id="quantity" value="" />
                    </div>
                </div>
            <?php } ?>  
        <div id="is_form_submit_button">
            <input class="jquery-button" type="submit" value="<?php echo JText::_('SUBMIT'); ?>" />
        </div>
    </div>
    <input type="hidden" name="option" value="<?php echo $this->option; ?>">
    <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>">
    <input type="hidden" name="view" value="product">
    <input type="hidden" name="isproductsearch" value="1">
    <input type="hidden" name="layout" value="productsearchresult">
</form>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("input.jquery-button").each(function(){
            jQuery(this).button();
        });
        jQuery("input:text").addClass("ui-corner-all")
                            .button()
                            .css({
                                 'font' : 'inherit',
                                 'color' : 'inherit',
                                 'text-align' : 'left',
                                 'outline' : 'none',
                                 'cursor' : 'text',
                                 'padding': '1px 4px'
                             });
        jQuery("select.jquery-cbo").each(function(){
            jQuery(this).chosen();
        });
        jQuery("div#productlisting_slider-range").noUiSlider({
                range: [ <?php echo $this->config['search_pricerangestart']; ?>, <?php echo $this->config['search_pricerangeend']?> ]
               ,start: [ <?php echo $this->config['search_pricerangestart']; ?>, <?php echo $this->config['search_pricerangeend']?> ]
               ,step: <?php echo $this->config['search_pricegap']; ?>
               ,serialization: {
                     /* Write the slider values to 
                            two newly created input 
                            fields with the names 
                       "priceFrom" and "priceTo". */
                            to: ["pricestart", "priceend"]
                     /* Round to 2 decimals. */
                       ,resolution: 1
                    }
               ,slide: function(){
                  var values = jQuery(this).val();
                  jQuery("span#priceamountlabel").text(
                     values[0] +
                     " - " +
                     values[1]
                  );
               }
        });
    });
    function getSubCategoriesByCategory(object){        
        jQuery.post('index.php?option=com_isproductlisting&task=getsubcategoriesbycategory',{id:object.value,for:'search'},function(data){
            if(data){
                jQuery("div#is_form_subcategories").html(data);
                jQuery("select#subcatid").chosen();
            }
        });
    }
</script>
<?php } 
    include_once JPATH_ADMINISTRATOR."/components/com_isproductlisting/views/jscr.php";
?>
