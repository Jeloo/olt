<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class ISProductListingViewRss extends ITView {

    /**
     * Displays a generic page
     * (for when there are no actions or selected registers)
     *
     * @param string $template  Optional name of the template to use
     */
    function display($tpl = NULL) {
        $document = JFactory::getDocument();
        $model = $this->getModel('rss');
        $cur_layout = JRequest::getVar('layout', 'products');
        $config = Array();
        if (sizeof($config) == 0) {
            $results = $model->getConfiginArray('rss');
            if ($results) { //not empty
                $config = $results;
            }
        }
        $defaultconfig = $model->getConfiginArray('default');
        $data_directory = $model->getDataDirectory();
        if ($cur_layout == 'products') {
            if ($defaultconfig['showrssfeeding'] == 1) {
                $document->setTitle(JText::_('SUBSCRIBE_FOR_PRODUCTS'));
                $dataproduct = $model->getRssProducts();
                $this->assignRef('result', $dataproduct);
            }
        }
        $defaultcurrency = $model->getDefaultCurrency();
        $this->assignRef('config', $config);
        $this->assignRef('defaultconfig', $defaultconfig);
        $this->assignRef('defaultcurrency', $defaultcurrency);
        $this->assignRef('data_directory', $data_directory);
        parent :: display();
        JFactory::getApplication()->close();
    }

}
