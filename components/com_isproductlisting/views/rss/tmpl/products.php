<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
if($this->defaultconfig['showrssfeeding'] == 1){
	header("Content-Type: text/xml;");
	$link = JURI::root();
	$results = $this->result;
	$datadirectory=$this->data_directory;
	$directory=$datadirectory->configvalue;
	/*echo'<?xml version="1.0" ?>';*/
	echo'<rss version="2.0">';
	echo'<channel>';
	echo'<title>'.$this->config['rss_title'].'</title>';
	echo'<link>'.$link.'</link>';
	echo'<ttl>'.$this->config['rss_ttl'].'</ttl>';
	echo'<description>'.$this->config['rss_description'].'</description>';
	if($this->config['rss_copyright'] != ''){
	echo'<copyright>'.$this->config['rss_copyright'].'</copyright>';
	}
	if($this->config['rss_webmaster'] != ''){
	echo'<webmaster>'.$this->config['rss_webmaster'].'</webmaster>';
	}
	if($this->config['rss_editor'] != ''){
	echo'<editor>'.$this->config['rss_editor'].'</editor>';
	}
	foreach($results AS $result) {
		if($result->productdefaultimage!=="") {
			$urlimage = $link. $directory.'/products/product_'.$result->id.'/thumbnail/medium-'.$result->productdefaultimage;
		} else {
			$urlimage = $link.'administrator/components/com_isproductlisting/include/images/isproductlisting.png';
		}
		$item ='<item><title>';
		$item .= $result->title.'</title>';
		$item .= '<description><![CDATA['." <img src=\"".$urlimage."\" width=\"150\" /> <br/><strong>Category :</strong> ".' '.$result->categorytitle.'<br>  '."<strong>Subcategory : </strong>".' '.$result->subcategorytitle.'<br>  '
										.' <strong>Brand : </strong>'. ' '.$result->brandtitle.'<br>  '.'<strong>Price : </strong>'. ' '.$this->defaultcurrency.' '.$result->price.'<br> '.
										'<strong>Quantity : </strong>'.' '. $result->quantity.']]></description>';
		$itemlink = $link.'index.php?option=com_isproductlisting&view=product&layout=viewproduct&pl=1&pid='.$result->id;
		$item .= '<link>'.htmlspecialchars($itemlink).'</link>';
		$item .='<image>';
		$item .='<url>'.$urlimage;
		$item .='</url>';
		$item .='<title>'.$result->title;
		$item .='</title>';
		$item .='<link>'.htmlspecialchars($itemlink);
		$item .='</link>';
		$item .='</image>';

		$item .='</item>';
		echo $item;
	}
	echo'</channel>';
	echo'</rss>';
}
