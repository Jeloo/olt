This is package of extension Zh YandexMap component, and it contains both component versions for Joomla! - 2.5 and 3.0

Unzip this archive and install appropriate version.

* for 2.5 version use zip file: com_zhyandexmap-j25-x.x.x.x-final.zip
* for 3.0 version use zip file: com_zhyandexmap-j30-x.x.x.x-final.zip
where x.x.x.x - the latest version of extension for correspondent joomla's release

The extension supports autoupdate feature. 

If you install last version of component, check whether exist fresh version of plugin or module (if you use it).
And don't forget - plugin is installed disabled, therefore go to plugin manager, find it and enable.

JED links:
* component:	http://extensions.joomla.org/extensions/maps-a-weather/maps-a-locations/maps/16781
* plugin:		http://extensions.joomla.org/extensions/extension-specific/extensions-specific-non-sorted/16912
* module:		http://extensions.joomla.org/extensions/extension-specific/extensions-specific-non-sorted/20225

Extension forum:	http://forum.zhuk.cc 
Documentation:		http://wiki.zhuk.cc 
Main author site:	http://zhuk.cc 
