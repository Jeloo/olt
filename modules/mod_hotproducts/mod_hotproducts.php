<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

$title = $params->get('title');
$showtitle = $params->get('showtitle');
$showbrand = $params->get('showbrand');
$brandoption = $params->get('brandoption');
$showcategory = $params->get('showcategory');
$showsubcategory = $params->get('showsubcategory');
//$showshortsummary = $params->get('showshortsummary');
$showshortsummary = 0;
$showprice = $params->get('showprice');
$listingstyle= $params->get('listingstyle');
$defaultheight = $params->get('defaultheight');

$moduleclass_sfx = $params->get('moduleclass_sfx');

$sliding= $params->get('sliding','1');
$consecutivesliding= $params->get('consecutivesliding','3');
$noofrecord = $params->get('noofrecord','20');
if($noofrecord>100) $noofrecord=100;
$itemid= $params->get('itemid');
if($itemid) $itemid= $params->get('itemid');
else $itemid =  JRequest::getVar('Itemid');

$componentPath =  'components/com_isproductlisting';
require_once $componentPath . '/ITApplication.php';
$model = ITModel::getITModel('modplug');
$config = $model->getConfig('default');

$lang = JFactory :: getLanguage();
$lang->load('com_isproductlisting');
$hotproducts = $model->mpGethotProducts($noofrecord);
$config = $model->getConfig('default');
$document = JFactory::getDocument();
if(JVERSION < 3)
    $document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$currencysymbol = $model->getDefaultCurrency();
if ($hotproducts) { 

	   $contents = '<div id="is_modWrapperDiv" class="is_modWrapperDiv hotproduct" style="height:'.$defaultheight.'px;">';
		$top="";
		if ($showtitle == 1){
                    $divclass = (!empty($moduleclass_sfx) || $moduleclass_sfx != '') ? "class=\"$moduleclass_sfx\"":"id=\"is_modheadingWrapper\" class=\"is_modheadingWrapper\"";
                    $top .= '<div '.$divclass.' >
                                <h3>
                                    <span id="is_modtitle">'.$title.'</span>
                                </h3>
                            </div>';
		}	
		$i=1;
                $datacontent = '';
                foreach ($hotproducts as $hotproduct) {
                    $datacontent .= " 
                                    <div id=\"is_moddatadiv\" >
                                        <div id=\"is_mod_uppertitle_logo\">
                                            <span id=\"is_modproducttitle\" ><a href=\"index.php?option=com_isproductlisting&view=product&layout=viewproduct&pid=$hotproduct->id&Itemid=$itemid\">$hotproduct->producttitle</a></span>";
                                            if($showbrand == 1 && ($brandoption == 0 || $brandoption == 2))
                                                $datacontent .= "<img id=\"is_modproductbrandlogo\" src=\"".JURI::root().$config['data_directory']."/brands/brand_".$hotproduct->brandid."/logo/".$hotproduct->productbrandimage."\" width=\"30px\" height=\"30px\"  />";
                    $datacontent .= "   </div>
                                        <div class=\"is_modproduct_tablerow\">
                                        <div class=\"is_modproductimagediv\">
                                            <img id=\"is_modproductimage\" src=\"".JURI::root().$config['data_directory']."/products/product_".$hotproduct->id."/thumbnail/medium-".$hotproduct->productdefaultimage."\" width=\"75px\" height=\"75px\" />
                                        </div>
                                        <div class=\"is_modproductdata\" > ";
                                            if($showcategory == 1)
                                                $datacontent .= "<span class=\"is_modproduct_data\"><span class=\"is_modproduct_title\">".JText::_('CATEGORY')."</span>".$hotproduct->categorytitle."</span>";
                                            if($showsubcategory == 1)
                                                $datacontent .= "<span class=\"is_modproduct_data\"><span class=\"is_modproduct_title\">".JText::_('SUB_CATEGORY')."</span>".$hotproduct->subcategorytitle."</span>";
                                            if($showbrand == 1 && ($brandoption == 1 || $brandoption == 2)){ // show brand text
                                                $datacontent .= "<span class=\"is_modproduct_data\" ><span class=\"is_modproduct_title\">".JText::_('BRAND')."</span>".$hotproduct->brandtitle."</span>";
                                            }
                                            if($showprice == 1){
                                                $datacontent .= "<span class=\"is_modproduct_data price\" ><span class=\"is_modproduct_title\">".JText::_('PRICE')."</span>".$currencysymbol.' '.$hotproduct->price."</span>";
                                            }
                    $datacontent .= "   </div> 
                                        </div>";
                                            if($showshortsummary == 1){
                                                //$datacontent .= "<div class=\"is_modproduct_shortsummary\"><span class=\"is_modproduct_shortsummary_title\">".JText::_('SHORT_SUMMARY')."</span>".$hotproduct->shortsummary."</div>";
                                            }
                    $datacontent .= "
                                    </div> ";
                }
                if ($sliding == 1) {
                    if($listingstyle==2){
                        $tcontents = '<table cellpadding="0" cellspacing="0" border="1" width="100%" id="is_modTable" class="is_modTable"> <tr>';
                        $scontents="";
                        for ($a = 0; $a < $consecutivesliding; $a++){
                            $scontents .= '<td>'.$datacontent.'</td>';
                        }                                                                            
                        $datacontent = $tcontents.$scontents.'</tr></table>';
                        $contents .= $top .'<marquee id="is_marqueemodhotproduct" style="height:'.$defaultheight.'px;" direction="left"  scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">' .$scontents . '</marquee></div>';
                        echo $contents;
                    }elseif($listingstyle==1){
						$scontents = "";
                        for ($a = 0; $a < $consecutivesliding; $a++){
                                $scontents .= $datacontent;
                        }
                        $contents .= $top . '<marquee id="is_marqueemodhotproduct" style="height:'.$defaultheight.'px;" direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">'.$scontents.'</marquee></div>';
						echo $contents;
                    }
				}elseif($sliding == 0){
                        echo  $contents.$top.$datacontent."</div>";
				}
				echo '
				<script>
					jQuery(document).ready(function(){
						var div = jQuery("div#is_modWrapperDiv.hotproduct div");
						var marqueeHeight = jQuery("marquee#is_marqueemodhotproduct").height() - div.height();
						if(jQuery.isNumeric(marqueeHeight))
							jQuery("div#is_modWrapperDiv marqueeis_marqueemodhotproduct").css({height:marqueeHeight});
					});
				</script>';
 } ?>


