<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

$showproducttype = $params->get('showproducttype');
$noofrecord = $params->get('noofrecored');
$height = $params->get('height');
$width = $params->get('width');

$itemid= $params->get('itemid');
if($itemid) $itemid= $params->get('itemid');
else $itemid =  JRequest::getVar('Itemid');

$componentAdminPath = JPATH_ADMINISTRATOR . '/components/com_isproductlisting';
$componentPath =  'components/com_isproductlisting';
$trclass=array('odd','even');
require_once $componentPath . '/ITApplication.php';
require_once $componentPath . '/models/modplug.php';
$model = new ISProductlistingModelModplug();
$config = $model->getConfig('default');

$lang = JFactory :: getLanguage();
$lang->load('com_isproductlisting');
$products = $model->mpGetProductsForSlideShow($showproducttype,$noofrecord);
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
$document->addStyleDeclaration(ITModel::getITModel('theme')->getThemeColor());
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
?>
<style type="text/css">

/*** set the width and height to match your images **/

#slideshow {position:relative;height:<?php echo $height;?>px;}
#slideshow DIV {position:absolute;top:0;left:0;z-index:8;opacity:0.0;height: <?php echo $height;?>px;background-color: #FFF;}
#slideshow DIV.active {z-index:10;opacity:1.0;}
#slideshow DIV.last-active {z-index:9;}
#slideshow DIV IMG {height: <?php echo $height;?>px;width:<?php echo $width;?>px;display: block;border: 0;margin-bottom: 10px;}

</style>

<?php
$contents = "";
if ($products) {
	
	$contents = '<div id="slideshow">';
	$firstcar = 1;
	foreach ($products as $product) {
		if($firstcar == 1){
			$contents.= '<div class="active" >';
			$firstcar++;
		}else $contents.= '<div>';
			
		$contents.='<a href="index.php?option=com_isproductlisting&view=product&layout=viewproduct&cl=8&pid='.$product->id.'&Itemid='.$itemid.'">';
                $largeimagsrc = $config['data_directory'].'/products/product_'. $product->id.'/thumbnail/large-'.$product->productdefaultimage;
		$contents .= '<img alt="" src="'. $largeimagsrc .'"/>';
		$contents .= '</a></div>';
	}
	$contents .= '</div>';

 }
 echo $contents;
 ?>
<script type="text/javascript">
	function slideSwitch() {
		var active = jQuery('#slideshow DIV.active');

		if ( active.length == 0 ) active = jQuery('#slideshow DIV:last');

		// use this to pull the divs in the order they appear in the markup
		var next =  active.next().length ? active.next()
			: jQuery('#slideshow DIV:first');

		// uncomment below to pull the divs randomly
		// var $sibs  = $active.siblings();
		// var rndNum = Math.floor(Math.random() * $sibs.length );
		// var $next  = $( $sibs[ rndNum ] );


		active.addClass('last-active');

		next.css({opacity: 0.0})
			.addClass('active')
			.animate({opacity: 1.0}, 1500, function() {
				active.removeClass('active last-active');
			});
	}

	jQuery(function() {
		setInterval( "slideSwitch()", 5000 );
	});
	
</script>
