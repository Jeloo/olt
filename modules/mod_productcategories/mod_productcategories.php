<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$title = $params->get('title');
$showtitle = $params->get('showtitle');
$listingstyle = $params->get('listingstyle');

$showcategoryhaveproduct = $params->get('showcategoryhaveproduct');
$moduleclass_sfx = $params->get('moduleclass_sfx');
$defaultheight = $params->get('defaultheight');

$sliding = $params->get('sliding','1');
$consecutivesliding = $params->get('consecutivesliding','3');
$noofrecord = $params->get('noofrecord');
if($noofrecord>100) $noofrecord=100;
$itemid = $params->get('itemid');
if($itemid) $itemid = $params->get('itemid');
else $itemid =  JRequest::getVar('Itemid');

$componentAdminPath = JPATH_ADMINISTRATOR . '/components/com_isproductlisting';
$componentPath =  'components/com_isproductlisting';
$trclass=array('odd','even');
require_once $componentPath . '/ITApplication.php';
require_once $componentPath . '/models/modplug.php';
$model = new ISProductlistingModelModplug();
$config = $model->getConfig('default');

$lang = JFactory :: getLanguage();
$lang->load('com_isproductlisting');
$categories = $model->mpGetCategories($showcategoryhaveproduct,$noofrecord);
$config = $model->getConfig('default');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/css/default.css');
$document->addStyleDeclaration(ITModel::getITModel('theme')->getThemeColor());
$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');

if ($categories){ 
	   $contents = '<div id="is_modWrapperDiv" class="is_modWrapperDiv productcategory" style="height:'.$defaultheight.'px;">';
		$top="";
		if ($showtitle == 1){
                    $divclass = (!empty($moduleclass_sfx) || $moduleclass_sfx != '') ? "class=\"$moduleclass_sfx\"":"id=\"is_modheadingWrapper\" class=\"is_modheadingWrapper\"";
                    $top .= '<div '.$divclass.' >
                                <h3>
                                    <span id="is_modtitle">'.$title.'</span>
                                </h3>
                            </div>';
		}	
		$i=1;
                $datacontent = '<div id="is_moddatadiv" class="productcategory">';
                foreach ($categories as $category) {
                    $datacontent .= " <span class=\"is_modnumberlistspan\" ><a href=\"index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=2&catid=$category->categoryid&Itemid=$itemid\">$category->categorytitle</a><span class=\"is_modnumberspan\" >$category->totalproduct</span></span>";
                }
                $datacontent .= '</div>';
                if ($sliding == 1) {
                    if($listingstyle==2){
                        $tcontents = '<table cellpadding="0" cellspacing="0" border="1" width="100%" id="is_modTable" class="is_modTable"> <tr>';
                        $scontents="";
                        for ($a = 0; $a < $consecutivesliding; $a++){
                            $scontents .= '<td>'.$datacontent.'</td>';
                        }                                                                            
                        $datacontent = $tcontents.$scontents.'</tr></table>';
                        $contents .= $top .'<marquee id="is_marqueemodproductcategory" style="height:'.$defaultheight.'px;" direction="left"  scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">' .$scontents . '</marquee></div>';
                        echo $contents;
                    }elseif($listingstyle==1){
						$scontents = "";
                        for ($a = 0; $a < $consecutivesliding; $a++){
                                $scontents .= $datacontent;
                        }
                        $contents .= $top . '<marquee id="is_marqueemodproductcategory" style="height:'.$defaultheight.'px;" direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">'.$scontents.'</marquee></div>';
						echo $contents;
                    }
				}elseif($sliding == 0){
                        echo  $contents.$top.$datacontent."</div>";
				}
				echo '
				<script>
					jQuery(document).ready(function(){
						var div = jQuery("div#is_modWrapperDiv.productcategory div");
						var marqueeHeight = jQuery("marquee#is_marqueemodproductcategory").height() - div.height();
						if(jQuery.isNumeric(marqueeHeight))
							jQuery("div#is_modWrapperDiv marquee#is_marqueemodproductcategory").css({height:marqueeHeight});
					});
				</script>';
 } ?>
