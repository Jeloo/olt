<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerEmailtemplate extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function saveemailtemplate() {
        $emailtemplate_model = $this->getModel('emailtemplate');
        $data = JRequest :: get('post');
        $templatefor = $data['templatefor'];
        $return_value = $emailtemplate_model->storeEmailTemplate();
        switch ($templatefor) {
            case 'purchase-requestadmin' : $tempfor = 'pd-prad'; break;
            case 'purchase-verifiedadmin' : $tempfor = 'pd-vfad'; break;
            case 'purchase-requestuser' : $tempfor = 'pd-prus'; break;
            case 'purchase-verifieduser' : $tempfor = 'pd-vfus'; break;
            case 'tell-friend' : $tempfor = 'tl-frnd'; break;
        }
        if ($return_value == 1) $msg = JText::_('EMAIL_TEMPATE_SAVED');
        else $msg = JText::_('ERROR_SAVING_EMAIL_TEMPLATE');
        $link = 'index.php?option=com_isproductlisting&emailtemplate&view=emailtemplate&layout=emailtemplate&tf=' . $tempfor;
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'emailtemplate');
        $layoutName = JRequest :: getVar('layout', 'emailtemplate');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>