<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerProduct extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function publishproduct() {
        $product_model = $this->getModel('Product');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $product_model->changeStatusProduct($cid, 1);
        if ($result == 3)
            $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1)
            $msg = JText::_('PUBLISHED');
        else
            $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=products';
        $this->setRedirect($link, $msg);
    }

    function unpublishproduct() {
        $product_model = $this->getModel('Product');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $product_model->changeStatusProduct($cid, 0);
        if ($result == 3)
            $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1)
            $msg = JText::_('UNPUBLISHED');
        else
            $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=product&view=products&layout=products';
        $this->setRedirect($link, $msg);
    }

    function makeproductfeatured() {
        $msg = JText::_('FEATURE_AVAILABLE_IN_PRO_VERSION');
        $link = 'index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion';
        $this->setRedirect($link, $msg);
    }

    function makeproductunfeatured() {
        $msg = JText::_('FEATURE_AVAILABLE_IN_PRO_VERSION');
        $link = 'index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion';
        $this->setRedirect($link, $msg);
    }

    function makeproductgold() {
        $msg = JText::_('FEATURE_AVAILABLE_IN_PRO_VERSION');
        $link = 'index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion';
        $this->setRedirect($link, $msg);
    }

    function makeproductungold() {
        $msg = JText::_('FEATURE_AVAILABLE_IN_PRO_VERSION');
        $link = 'index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion';
        $this->setRedirect($link, $msg);
    }

    function editproduct() { //edit product and new product
        JRequest :: setVar('layout', 'formproduct');
        JRequest :: setVar('view', 'product');
        JRequest :: setVar('c', 'product');
        $this->display();
    }

    function saveproduct() { // product
        $redirect = $this->storeproduct('saveclose');
    }

    function saveproductandnew() { // saveandnew product
        $redirect = $this->storeproduct('saveandnew');
    }

    function saveproductsave() { // save product
        $redirect = $this->storeproduct('save');
    }

    function storeproduct($callfrom) {
        $product_model = $this->getModel('Product');
        $return_value = $product_model->storeProduct();
        if ($return_value[0] == 1) {
            $msg = JText::_('PRODUCT_SAVED');
            if ($callfrom == 'saveclose') {
                $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproductimage&product=' . $return_value[1];
            } elseif ($callfrom == 'save') {
                $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproduct&cid[]=' . $return_value[1];
            } elseif ($callfrom == 'saveandnew') {
                $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproductimage&product=' . $return_value[1];
            }
        } else if ($return_value[0] == 2) {
            $msg = JText::_('ALL_FIELD_MUST_BE_ENTERD');
            $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproduct';
        } else if ($return_value[0] == 3) {
            $msg = JText::_('SUB_CATEGORY_ALREADY_EXIST');
            $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproduct';
        } else {
            $msg = JText::_('ERROR_SAVING_PRODUCT');
            $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=products';
        }
        $this->setRedirect($link, $msg);
    }

    function removeproductimage() {
        $imageid = JRequest::getVar('imageid');
        $productid = JRequest::getVar('productid');
        $productimage_model = $this->getModel('Productimage');
        $result = $productimage_model->removeProductImage($imageid, $productid);
        if ($result == 1)
            $msg = JText::_('IMAGE_REMOVE_SUCCESSFULLY');
        else
            $msg = JText::_('ERROR_REMOVING_IMAGE');
        $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproductimage&product=' . $productid;
        $this->setRedirect($link, $msg);
    }

    function makeproductimagedefault() {
        $imageid = JRequest::getVar('imageid');
        $productid = JRequest::getVar('productid');
        $productimage_model = $this->getModel('Productimage');
        $result = $productimage_model->makeProductImageDefault($imageid, $productid);
        if ($result == 1)
            $msg = JText::_('IMAGE_SET_AS_DEFAULT_SUCCESSFULLY');
        else
            $msg = JText::_('ERROR_SETTING_DEFAULT_IMAGE');
        $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=formproductimage&product=' . $productid;
        $this->setRedirect($link, $msg);
    }

    function saveproductimages() {
        $mainframe = JFactory::getApplication();
        $productid = JRequest::getVar('productid');
        $productimage_model = $this->getModel('Productimage');
        $result = $productimage_model->storeProductImage($productid);
        echo $result;
        $mainframe->close();
    }

    function removeproduct() { //Remove product
        $product_model = $this->getModel('Product');
        $return_value = $product_model->deleteProduct();
        if ($return_value == 1)
            $msg = JText::_('PRODUCT_DELETED');
        else
            $msg = $return_value - 1 . ' ' . JText::_('ERROR_PRODUCT_COULD_NOT_DELETE');

        $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=products';
        $this->setRedirect($link, $msg);
    }

    function cancelproduct() { //cancel product
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=product&view=product&layout=products';
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'product');
        $layoutName = JRequest :: getVar('layout', 'products');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>