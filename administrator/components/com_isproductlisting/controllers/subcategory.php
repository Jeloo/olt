<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerSubcategory extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function editsubcategory() { //edit subcategory and new subcategory
        JRequest :: setVar('layout', 'formsubcategory');
        JRequest :: setVar('view', 'subcategory');
        JRequest :: setVar('c', 'subcategory');
        $this->display();
    }

    function savesubcategory() { // subcategory
        $redirect = $this->storesubcategory('saveclose');
    }

    function savesubcategoryandnew() { // saveandnew subcategory
        $redirect = $this->storesubcategory('saveandnew');
    }

    function savesubcategorysave() { // save subcategory
        $redirect = $this->storesubcategory('save');
    }

    function storesubcategory($callfrom) {
        $subcategory_model = $this->getModel('Subcategory');
        $return_value = $subcategory_model->storeSubCategory();
        if ($return_value[0] == 1) {
            $msg = JText::_('SUB_CATEGORY_SAVED');
            if ($callfrom == 'saveclose') {
                $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories';
            } elseif ($callfrom == 'save') {
                $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=formsubcategory&cid[]=' . $return_value[1];
            } elseif ($callfrom == 'saveandnew') {
                $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=formsubcategory';
            }
        } else if ($return_value[0] == 2) {
            $msg = JText::_('ALL_FIELD_MUST_BE_ENTERD');
            $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=formsubcategory';
        } else if ($return_value[0] == 3) {
            $msg = JText::_('SUB_CATEGORY_ALREADY_EXIST');
            $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=formsubcategory';
        } else {
            $msg = JText::_('ERROR_SAVING_SUB_CATEGORY');
            $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories';
        }
        $this->setRedirect($link, $msg);
    }

    function removesubcategory() { //Remove subcategory
        $subcategory_model = $this->getModel('Subcategory');
        $return_value = $subcategory_model->deleteSubCategory();
        if ($return_value == 1)
            $msg = JText::_('SUB_CATEGORY_DELETED');
        else
            $msg = $return_value - 1 . ' ' . JText::_('ERROR_SUB_CATEGORY_COULD_NOT_DELETE');

        $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories';
        $this->setRedirect($link, $msg);
    }

    function cancelsubcategory() { //cancelcategory
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories';
        $this->setRedirect($link, $msg);
    }

    function getsubcategorybycategoryid() {
        $categoryid = JRequest::getVar('categoryid');
        if ($categoryid != '') {
            $subcategory_model = $this->getModel('Subcategory');
            $subcategories = $subcategory_model->getAllSubCategoriesForCombo(JText::_('SELECT_SUB_CATEGORY'), $categoryid);
            $subcategories = JHtml::_('select.genericList', $subcategories, 'subcategoryid', 'class="inputbox" ' . '', 'value', 'text', '');
            echo $subcategories;
        } else {
            echo JText::_('PLEASE_SELECT_CATEGORY_TO_GET_SUBCATEGORIES');
        }
        JFactory::getApplication()->close();
    }

    function publishsubcategory() {
        $subcategory_model = $this->getModel('subcategory');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $subcategory_model->changeStatusSubCategory($cid, 1);
        if ($result == 3)
            $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1)
            $msg = JText::_('PUBLISHED');
        else
            $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories';
        $this->setRedirect($link, $msg);
    }

    function unpublishsubcategory() {
        $subcategory_model = $this->getModel('subcategory');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $subcategory_model->changeStatusSubCategory($cid, 0);
        if ($result == 3)
            $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1)
            $msg = JText::_('UNPUBLISHED');
        else
            $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories';
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'subcategory');
        $layoutName = JRequest :: getVar('layout', 'subcategories');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>