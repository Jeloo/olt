<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerFeedback extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function saveproductfeedback() {
        $feedback_model = $this->getModel('feedback');
        $return_value = $feedback_model->storeProductFeedBack();
        $link = 'index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=' . JRequest::getVar('layout', 'productfeedbacks');
        if ($return_value == 1) $msg = JText :: _('PRODUCT_FEEDBACK_SAVED');
        else $msg = JText :: _('ERROR_SAVING_PRODUCT_FEEDBACK');
        $this->setRedirect($link, $msg);
    }

    function editproductfeedback() { //editproductfeedback
        JRequest :: setVar('layout', 'formproductfeedback');
        JRequest :: setVar('view', 'feedback');
        JRequest :: setVar('c', 'feedback');
        $this->display();
    }

    function removeproductfeedback() { //removeproductfeedback
        $feedback_model = $this->getmodel('feedback');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $return_value = $feedback_model->deleteProductFeedBack($cid);
        if ($return_value >= 1) $msg = JText::_('PRODUCT_FEEDBACK_DELETED');
        elseif ($return_value == false) $msg = $return_value - 1 . '' . JText::_('ERROR_PRODUCT_FEEDBACK_COULD_NOT_DELETED');
        $link = 'index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=' . JRequest::getVar('layout', 'productfeedbacks');
        $this->setredirect($link, $msg);
    }

    function cancelfeedback() { //cancel productfeedback
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=productfeedbacks';
        $this->setRedirect($link, $msg);
    }

    function cancelfeedbackapprovalque() { //cancelfeedbackapprovalque
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=feedbackapprovalqueue';
        $this->setRedirect($link, $msg);
    }

    function publishfeedback() {
        $feedback_model = $this->getModel('feedback');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $return_value = $feedback_model->feedbackPublished($cid, 1); // published
        $link = 'index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=feedbackapprovalqueue';
        $this->setRedirect($link, $msg);
    }

    function unpublishfeedback() {
        $feedback_model = $this->getModel('feedback');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $return_value = $feedback_model->feedbackPublished($cid, -1); // unpublished
        $link = 'index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=feedbackapprovalqueue';
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'feedback');
        $layoutName = JRequest :: getVar('layout', 'feedback');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>