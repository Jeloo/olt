<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerCurrency extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function editcurrency() { //edit currency and new currency
        JRequest :: setVar('layout', 'formcurrency');
        JRequest :: setVar('view', 'currency');
        JRequest :: setVar('c', 'currency');
        $this->display();
    }

    function savecurrency() { // currency
        $this->storecurrency('saveclose');
    }

    function savecurrencyandnew() { // saveandnew currency
        $this->storecurrency('saveandnew');
    }

    function savecurrencysave() { // save currency
        $this->storecurrency('save');
    }

    function storecurrency($callfrom) {
        $currency_model = $this->getModel('Currency');
        $return_value = $currency_model->storeCurrency();
        if ($return_value[0] == 1) {
            $msg = JText::_('CURRENCY_SAVED');
            if ($callfrom == 'saveclose') {
                $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
            } elseif ($callfrom == 'save') {
                $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=formcurrency&cid[]=' . $return_value[1];
            } elseif ($callfrom == 'saveandnew') {
                $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=formcurrency';
            }
        } else if ($return_value[0] == 2) {
            $msg = JText::_('ALL_FIELD_MUST_BE_ENTERD');
            $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=formcurrency';
        } else if ($return_value[0] == 3) {
            $msg = JText::_('CURRENCY_ALREADY_EXIST');
            $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=formcurrency';
        } else {
            $msg = JText::_('ERROR_SAVING_CURRENCY');
            $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
        }
        $this->setRedirect($link, $msg);
    }

    function removecurrency() { //Remove currency
        $currency_model = $this->getModel('Currency');
        $return_value = $currency_model->deleteCurrency();
        if ($return_value == 1) $msg = JText::_('CURRENCY_DELETED');
        else $msg = $return_value - 1 . ' ' . JText::_('ERROR_CURRENCY_COULD_NOT_DELETE');
        $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
        $this->setRedirect($link, $msg);
    }

    function cancelcurrency() { //cancel currency
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
        $this->setRedirect($link, $msg);
    }

    function makedefaultcurrency() {
        $currency_model = $this->getModel('Currency');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $currencyid = $cid[0];
        $return_value = $currency_model->makeDefaultCurrency($currencyid);
        if ($return_value == 1) $msg = JText :: _('CURRENCY_SET_DEFAULT');
        elseif ($return_value == 4) $msg = JText :: _('UNPUBLISHED_OBJECT_CANNOT_BE_DEFAULT');
        else $msg = JText :: _('ERROR_IN_CURRENCY_SET_DEFAULT');
        $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
        $this->setRedirect($link, $msg);
    }

    function publishcurrency() {
        $currency_model = $this->getModel('Currency');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $currency_model->changeStatusCurrency($cid, 1);
        if ($result == 3) $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1) $msg = JText::_('PUBLISHED');
        else $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
        $this->setRedirect($link, $msg);
    }

    function unpublishcurrency() {
        $currency_model = $this->getModel('currency');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $currency_model->changeStatusCurrency($cid, 0);
        if ($result == 3) $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1) $msg = JText::_('UNPUBLISHED');
        else $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency';
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'currency');
        $layoutName = JRequest :: getVar('layout', 'currency');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>