<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerCategory extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function publishcategory() {
        $category_model = $this->getModel('Category');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $category_model->changeStatusCategory($cid, 1);
        if ($result == 3)
            $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1)
            $msg = JText::_('PUBLISHED');
        else
            $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=categories';
        $this->setRedirect($link, $msg);
    }

    function unpublishcategory() {
        $category_model = $this->getModel('Category');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $category_model->changeStatusCategory($cid, 0);
        if ($result == 3)
            $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1)
            $msg = JText::_('UNPUBLISHED');
        else
            $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=categories';
        $this->setRedirect($link, $msg);
    }

    function editcategory() { //edit category and new category
        JRequest :: setVar('layout', 'formcategory');
        JRequest :: setVar('view', 'category');
        JRequest :: setVar('c', 'category');
        $this->display();
    }

    function savecategory() { // category
        $redirect = $this->storecategory('saveclose');
    }

    function savecategoryandnew() { // saveandnew category
        $redirect = $this->storecategory('saveandnew');
    }

    function savecategorysave() { // save category
        $redirect = $this->storecategory('save');
    }

    function storecategory($callfrom) {
        $category_model = $this->getModel('Category');
        $return_value = $category_model->storeCategory();
        if ($return_value[0] == 1) {
            $msg = JText::_('CATEGORY_SAVED');
            if ($callfrom == 'saveclose') {
                $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=categories';
            } elseif ($callfrom == 'save') {
                $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=formcategory&cid[]=' . $return_value[1];
            } elseif ($callfrom == 'saveandnew') {
                $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=formcategory';
            }
        } else if ($return_value[0] == 2) {
            $msg = JText::_('ALL_FIELD_MUST_BE_ENTERD');
            $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=formcategory';
        } else if ($return_value[0] == 3) {
            $msg = JText::_('CATEGORY_ALREADY_EXIST');
            $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=formcategory';
        } else {
            $msg = JText::_('ERROR_SAVING_CATEGORY');
            $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=categories';
        }
        $this->setRedirect($link, $msg);
    }

    function removecategory() { //Remove category
        $category_model = $this->getModel('Category');
        $return_value = $category_model->deleteCategory();
        if ($return_value == 1) $msg = JText::_('CATEGORY_DELETED');
        else $msg = $return_value - 1 . ' ' . JText::_('ERROR_CATEGORY_COULD_NOT_DELETE');
        $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=categories';
        $this->setRedirect($link, $msg);
    }

    function cancelcategory() { //cancelcategory
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=category&view=category&layout=categories';
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'category');
        $layoutName = JRequest :: getVar('layout', 'categories');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>