<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:  IT Fant
  Contact:  itfant.com , info@itfant.com
  Created on:   January, 2014
  Project:  IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerBrand extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function publishbrand() {
        $brand_model = $this->getModel('brand');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $brand_model->changeStatusBrand($cid, 1);
        if ($result == 3) $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1) $msg = JText::_('PUBLISHED');
        else $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&view=brand&layout=brands';
        $this->setRedirect($link, $msg);
    }

    function unpublishbrand() {
        $brand_model = $this->getModel('brand');
        $cid = JRequest::getVar('cid', array(), '', 'array');
        $result = $brand_model->changeStatusBrand($cid, 0);
        if ($result == 3) $msg = JText::_('DEFAULT_OBJECT_CANNOT_BE_UNPUBLISHED_OR_PUBLISHED');
        elseif ($result == 1) $msg = JText::_('UNPUBLISHED');
        else $msg = JText::_('ERROR_OCCURRED');
        $link = 'index.php?option=com_isproductlisting&view=brand&layout=brands';
        $this->setRedirect($link, $msg);
    }

    function editbrand() { //edit brands and new brands
        JRequest :: setVar('layout', 'formbrand');
        JRequest :: setVar('view', 'brand');
        JRequest :: setVar('c', 'brand');
        $this->display();
    }

    function savebrand() { // Brand
        $redirect = $this->storebrand('saveclose');
    }

    function savebrandandnew() { // saveandnew Brand
        $redirect = $this->storebrand('saveandnew');
    }

    function savebrandsave() {// save Brand
        $redirect = $this->storebrand('save');
    }

    function storebrand($callfrom) {
        $brand_model = $this->getModel('Brand');
        $return_value = $brand_model->storeBrand();
        if ($return_value[0] == 1) {
            $msg = JText::_('BRAND_SAVED');
            if ($callfrom == 'saveclose') {
                $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=brands';
            } elseif ($callfrom == 'save') {
                $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=formbrand&cid[]=' . $return_value[1];
            } elseif ($callfrom == 'saveandnew') {
                $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=formbrand';
            }
        } else if ($return_value[0] == 2) {
            $msg = JText::_('ALL_FIELD_MUST_BE_ENTERD');
            $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=formbrand';
        } else if ($return_value[0] == 3) {
            $msg = JText::_('BRAND_ALREADY_EXIST');
            $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=formbrand';
        } else {
            $msg = JText::_('ERROR_SAVING_BRAND');
            $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=brands';
        }
            $this->setRedirect($link, $msg);
    }

    function removebrand() { //Remove Brand
        $brand_model = $this->getModel('Brand');
        $return_value = $brand_model->deleteBrand();
        if ($return_value == 1) $msg = JText::_('BRAND_DELETED');
        else $msg = $return_value - 1 . ' ' . JText::_('ERROR_BRAND_COULD_NOT_DELETE');
        $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=brands';
        $this->setRedirect($link, $msg);
    }

    function cancelbrand() { //cancelbrand
        $msg = JText::_('OPERATION_CANCELED');
        $link = 'index.php?option=com_isproductlisting&c=brand&view=brand&layout=brands';
        $this->setRedirect($link, $msg);
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'brand');
        $layoutName = JRequest :: getVar('layout', 'brands');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>