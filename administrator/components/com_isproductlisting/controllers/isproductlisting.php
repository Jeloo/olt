<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ISProductlistingControllerISProductlisting extends ITController {

    function __construct() {
        parent :: __construct();
        $this->registerTask('add', 'edit');
    }

    function display($cachable = false, $urlparams = false) {
        $document = JFactory :: getDocument();
        $viewName = JRequest :: getVar('view', 'isproductlisting');
        $layoutName = JRequest :: getVar('layout', 'controlpanel');
        $viewType = $document->getType();
        $view = $this->getView($viewName, $viewType);
        $view->setLayout($layoutName);
        $view->display();
    }

}

?>