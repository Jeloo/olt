DROP TABLE IF EXISTS `#__isproductlisting_brands`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_categories`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_config`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_config` (
  `configname` varchar(50) NOT NULL,
  `configvalue` varchar(255) NOT NULL,
  `configfor` varchar(15) NOT NULL,
  PRIMARY KEY (`configname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `#__isproductlisting_config` (`configname`, `configvalue`, `configfor`) VALUES
('data_directory', 'productlisitng_data', 'default'),
('date_format', 'Y-m-d', 'default'),
('pricenotation', 'default', 'default'),
('pricedecimalpoint', '0', 'default'),
('productimagesmallsizewidth', '150', 'default'),
('productimagelargesizeheight', '400', 'default'),
('productimagesmallsizeheight', '150', 'default'),
('productimagelargesizewidth', '500', 'default'),
('offline', '0', 'default'),
('versioncode', '1.0.0', 'default'),
('title', 'Product Listing', 'default'),
('offline_text', 'We Are Down Please Visit Next Time', 'default'),
('showtitle', '1', 'default'),
('current_location', '1', 'default'),
('productfilterenable', '1', 'default'),
('showrssfeeding', '1', 'default'),
('search_pricerangestart', '0', 'default'),
('toplink_products', '1', 'default'),
('toplink_categories', '1', 'default'),
('toplink_subcategories', '1', 'default'),
('toplink_brands', '1', 'default'),
('toplink_search', '1', 'default'),
('toplink_show', '1', 'default'),
('show_filter', '1', 'filter'),
('show_title_filter', '1', 'filter'),
('show_category_filter', '1', 'filter'),
('show_subcategory_filter', '1', 'filter'),
('show_brand_filter', '1', 'filter'),
('show_price_filter', '1', 'filter'),
('showgoogleadsense', '1', 'default'),
('productlistshowadsense', '1', 'default'),
('ad_on_repitation', '1', 'default'),
('custom_css', '', 'default'),
('add_on_client', 'ca-pub-8827762976015158', 'default'),
('add_on_slot', '9560237528', 'default'),
('add_on_format', '728 x 90', 'default'),
('viewproductshowadsense', '0', 'default'),
('showbrandlogo', '0', 'default'),
('product_google_share', '1', 'socailsharing'),
('product_friendfeed_share', '1', 'socailsharing'),
('product_blog_share', '1', 'socailsharing'),
('product_linkedin_share', '1', 'socailsharing'),
('product_myspace_share', '1', 'socailsharing'),
('product_twiiter_share', '1', 'socailsharing'),
('product_yahoo_share', '1', 'socailsharing'),
('product_digg_share', '1', 'socailsharing'),
('product_fb_share', '1', 'socailsharing'),
('product_fb_comments', '1', 'socailsharing'),
('search_title', '1', 'search'),
('search_category', '1', 'search'),
('search_subcategory', '1', 'search'),
('search_brand', '1', 'search'),
('search_price', '1', 'search'),
('search_quantity', '1', 'search'),
('search_serailnumber', '1', 'search'),
('visitor_can_buy', '1', 'default'),
('cp_goldproduct', '1', 'link'),
('cp_products', '1', 'link'),
('cp_categories', '1', 'link'),
('cp_subcategories', '1', 'link'),
('cp_brands', '1', 'link'),
('cp_search', '1', 'link'),
('cp_featuredproduct', '1', 'link'),
('cp_shortlist', '1', 'link'),
('cp_compare', '1', 'link'),
('cp_cart', '1', 'link'),
('plad', '1', 'default'),
('rss_webmaster', '', 'rss'),
('rss_title', '', 'rss'),
('rss_description', '', 'rss'),
('rss_copyright', '', 'rss'),
('rss_editor', '', 'rss'),
('rss_ttl', '', 'rss'),
('mailfromaddress', 'info@itfant.com', 'email'),
('adminemailaddress', '', 'email'),
('mailfromname', 'ITFant Web Development', 'email'),
('purchaserequestadmin', '1', 'email'),
('purchaseverifiedadmin', '1', 'email'),
('purchaserequestuser', '1', 'email'),
('purchaseverifieduser', '1', 'email'),
('headingbar_show', '1', 'default'),
('topsection_show', '1', 'default'),
('footer', '1', 'default'),
('search_pricerangeend', '1000000', 'default'),
('search_pricegap', '1000', 'default'),
('toplink_controlpanel', '1', 'default');

DROP TABLE IF EXISTS `#__isproductlisting_currency`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `symbol` varchar(60) DEFAULT NULL,
  `code` varchar(10) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(1) DEFAULT NULL,
  `regin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
INSERT INTO `#__isproductlisting_currency` (`id`, `title`, `symbol`, `code`, `status`, `isdefault`, `regin`) VALUES
(1, 'Pakistani Rupee', 'Rs.', 'PKR', 1, 0, 1),
(2, 'US Doller', '$', 'USD', 1, 1, 0),
(3, 'Pound Sterling', '£', 'GBP', 1, 0, 0),
(4, 'Euro', '€', 'EUR', 0, 0, 0),
(5, 'Japen Yen', 'Yen', 'YN', 1, 0, NULL);

DROP TABLE IF EXISTS `#__isproductlisting_emailtemplates`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_emailtemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `templatefor` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

INSERT INTO `#__isproductlisting_emailtemplates` (`id`, `uid`, `templatefor`, `title`, `subject`, `body`, `status`, `created`) VALUES
(1, 587, 'purchase-requestadmin', NULL, 'IS Product Listing: New Cart {SITE_TITLE}', '<div style="background: #6DC6DD; height: 20px;">Â </div>\r\n<p style="color: #2191ad;">Hello Admin ,</p>\r\n<p style="color: #4f4f4f;">We receive new Cart from {SITE_TITLE}.</p>\r\n<div style="display: block; padding: 20px; color: #4f4f4f; background: #F5FEFF; border: 1px solid #B7EAF7;">\r\n<p>Name: {USER_NAME}</p>\r\n<p>Email Address: {USER_EMAIL}</p>\r\n<p>Shopping Cart ID: {SHOPPING_CART_ID}</p>\r\n<p>Cart Total: {CART_TOTAL_AMOUNT}</p>\r\n<p>Date: {DATE}</p>\r\n</div>\r\n<div style="margin-top: 10px; padding: 10px 20px; color: #000000; background: #FAF2F2; border: 1px solid #F7C1C1;">\r\n<p><span style="color: red;"><strong>*DO NOT REPLY TO THIS E-MAIL*</strong></span><br />This is an automated e-mail message sent from our support system. Do not reply to this e-mail as we wonot receive your reply!</p>\r\n</div>', 1, '2012-07-21 06:35:14'),
(2, 587, 'purchase-verifiedadmin', NULL, 'IS Product Listing: Payment Verified {SITE_TITLE}', '<div style="background: #6DC6DD; height: 20px;">\r\n<div style="background: #6DC6DD; height: 20px;">Â </div>\r\n<p style="color: #2191ad;">Hello Admin ,</p>\r\n<p style="color: #4f4f4f;">We receive payment Verification of Cart from {SITE_TITLE}.</p>\r\n<div style="display: block; padding: 20px; color: #4f4f4f; background: #F5FEFF; border: 1px solid #B7EAF7;">\r\n<p>Name: {USER_NAME}</p>\r\n<p>Email Address: {USER_EMAIL}</p>\r\n<p>Shopping Cart ID: {SHOPPING_CART_ID}</p>\r\n<p>Cart Total: {CART_TOTAL_AMOUNT}</p>\r\n<p>Date: {DATE}</p>\r\n<p>Verified Date: {VERIFIED_DATE}</p>\r\n</div>\r\n<div style="margin-top: 10px; padding: 10px 20px; color: #000000; background: #FAF2F2; border: 1px solid #F7C1C1;">\r\n<p><span style="color: red;"><strong>*DO NOT REPLY TO THIS E-MAIL*</strong></span><br />This is an automated e-mail message sent from our support system. Do not reply to this e-mail as we wonot receive your reply!</p>\r\n</div>\r\n</div>', 1, '2012-07-21 06:35:14'),
(3, 587, 'purchase-requestuser', NULL, 'IS Product Listing: New Cart {SITE_TITLE}', '<div style="background: #6DC6DD; height: 20px;">Â </div>\r\n<p style="color: #2191ad;">Dear {USER_NAME} ,</p>\r\n<p style="color: #4f4f4f;">Your Cart {SHOPPING_CART_ID} request has been successfully received by our system, we will inform you as we receive payment notification.</p>\r\n<div style="display: block; padding: 20px; color: #4f4f4f; background: #F5FEFF; border: 1px solid #B7EAF7;">\r\n<p>Name: {USER_NAME}</p>\r\n<p>Email Address: {USER_EMAIL}</p>\r\n<p>Shopping Cart ID: {SHOPPING_CART_ID}</p>\r\n<p>Cart Total: {CART_TOTAL_AMOUNT}</p>\r\n<p>Date: {DATE}</p>\r\n</div>\r\n<div style="margin-top: 10px; padding: 10px 20px; color: #000000; background: #FAF2F2; border: 1px solid #F7C1C1;">\r\n<p><span style="color: red;"><strong>*DO NOT REPLY TO THIS E-MAIL*</strong></span><br />This is an automated e-mail message sent from our support system. Do not reply to this e-mail as we wonot receive your reply!</p>\r\n</div>', 1, '2012-07-21 06:35:14'),
(4, 587, 'purchase-verifieduser', NULL, 'IS Product Listing: Payment Verified {SITE_TITLE}', '<div style="background: #6DC6DD; height: 20px;">Â </div>\r\n<p style="color: #2191ad;">Dear {USER_NAME} ,</p>\r\n<p style="color: #4f4f4f;">We will receive the payment notification against your Cart {SHOPPING_CART_ID}.</p>\r\n<div style="display: block; padding: 20px; color: #4f4f4f; background: #F5FEFF; border: 1px solid #B7EAF7;">\r\n<p>Name: {USER_NAME}</p>\r\n<p>Email Address: {USER_EMAIL}</p>\r\n<p>Shopping Cart ID: {SHOPPING_CART_ID}</p>\r\n<p>Cart Total: {CART_TOTAL_AMOUNT}</p>\r\n<p>Date: {DATE}</p>\r\n<p>Verified Date: {VERIFIED_DATE}</p>\r\n<p>Thank you for purchasing.</p>\r\n</div>\r\n<div style="margin-top: 10px; padding: 10px 20px; color: #000000; background: #FAF2F2; border: 1px solid #F7C1C1;">\r\n<p><span style="color: red;"><strong>*DO NOT REPLY TO THIS E-MAIL*</strong></span><br />This is an automated e-mail message sent from our support system. Do not reply to this e-mail as we wonot receive your reply!</p>\r\n</div>', 1, '2012-07-21 06:35:14'),
(5, 587, 'tell-friend', NULL, 'IS Product Listing: Product Information {SITE_TITLE}', '<div style="background: #6DC6DD; height: 20px;">Â </div>\r\n<p style="color: #2191ad;">Email From Your Friend : {SENDER_NAME} ,</p>\r\n<p style="color: #4f4f4f;">You will receive this email from {SITE_TITLE}.Ã‚Â </p>\r\n<div style="display: block; padding: 20px; color: #4f4f4f; background: #F5FEFF; border: 1px solid #B7EAF7;">\r\n<p style="color: #4f4f4f;">Title: {PRODUCT_TITLE}</p>\r\n<p style="color: #4f4f4f;">Brand: {BRAND}</p>\r\n<p style="color: #4f4f4f;">Category: {CATEGORY}</p>\r\n<p style="color: #4f4f4f;">Sub Category: {SUB_CATEGORY}</p>\r\n<p style="color: #4f4f4f;">Price: {PRICE}</p>\r\n<p><span style="color: #4f4f4f;">{CLICK_HERE_TO_VISIT}</span></p>\r\n<p style="color: #4f4f4f;">{MESSAGE}</p>\r\n</div>\r\n<div style="margin-top: 10px; padding: 10px 20px; color: #000000; background: #FAF2F2; border: 1px solid #F7C1C1;">\r\n<p><span style="color: red;"><strong>*DO NOT REPLY TO THIS E-MAIL*</strong></span><br />This is an automated e-mail message sent from our support system. Do not reply to this e-mail as we wonot receive your reply!</p>\r\n</div>', 1, '2012-07-21 06:35:14');

DROP TABLE IF EXISTS `#__isproductlisting_products`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `brandid` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `datasheet` text,
  `listproductexpirydate` datetime DEFAULT NULL,
  `isgold` tinyint(1) NOT NULL DEFAULT '0',
  `isfeatured` tinyint(1) NOT NULL DEFAULT '0',
  `hits` int(5) DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_product_alerts`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_product_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `categoryid` varchar(50) DEFAULT NULL,
  `subcategoryid` varchar(50) DEFAULT NULL,
  `brandid` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `alerttype` int(3) DEFAULT NULL,
  `lastmailsend` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_product_feedback`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_product_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `feed` text,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_product_images`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `image` varchar(150) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `isdefault` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_shortlist`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_shortlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `productid` int(11) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `#__isproductlisting_subcategories`;
CREATE TABLE IF NOT EXISTS `#__isproductlisting_subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;