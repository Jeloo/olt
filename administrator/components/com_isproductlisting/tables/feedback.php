<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
// our table class for the application data
class Tablefeedback extends JTable
{
	var $id=null;
	var $userid=null;
	var $productid=null;
	var $feed=null;
	var $status=null;
	var $created=null;
	
	function __construct($db)
	{
		parent::__construct( '#__isproductlisting_product_feedback', 'id' , $db );
	}

	/**
	 * Validation
	 *
	 * @return boolean True if buffer is valid
	 *
	 */
	 function check()
	 {
	 	return true;
	 }

}

?>
