<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
// our table class for the application data
class Tableproduct extends JTable
{
/** @var int Primary key */
	var $id=null;
	var $userid=null;
	var $title=null;
	var $categoryid=null;
	var $subcategoryid=null;
	var $brandid=null;
	var $price=null;
	var $description=null;
	var $datasheet=null;
	var $listproductexpirydate=null;
	var $isgold=null;
	var $isfeatured=null;
	var $hits=null;
	var $quantity=null;
	var $status=null;
	var $created=null;

        
        function __construct($db)
	{
		parent::__construct( '#__isproductlisting_products', 'id' , $db );
	}
	
	/** 
	 * Validation
	 * 
	 * @return boolean True if buffer is valid
	 * 
	 */
	 function check()
	 {
	 	return true;
	 }
	 	 
}

?>
