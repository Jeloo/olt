<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
// our table class for the application data
class Tableproductimages extends JTable
{
/** @var int Primary key */
	var $id=null;
	var $productid=null;
	var $image=null;
	var $status=null;
	var $created=null;
	var $isdefault=null;
        
        function __construct($db)
	{
		parent::__construct( '#__isproductlisting_product_images', 'id' , $db );
	}
	
	/** 
	 * Validation
	 * 
	 * @return boolean True if buffer is valid
	 * 
	 */
	 function check()
	 {
	 	return true;
	 }
	 	 
}

?>
