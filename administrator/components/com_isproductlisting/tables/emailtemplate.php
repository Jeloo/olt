<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
class TableEmailTemplate extends JTable
{

	var $id=null;
	var $uid=null;
	var $templatefor=null;
	var $title=null;
	var $subject=null;
	var $body=null;
	var $created=null;
	var $status=null;

	
	function __construct($db)
	{
		parent::__construct( '#__isproductlisting_emailtemplates', 'id' , $db );
	}
	
	/** 
	 * Validation
	 * 
	 * @return boolean True if buffer is valid
	 * 
	 */
	 function check()
	 {
		return true;
	 }
	 	 
}

?>
