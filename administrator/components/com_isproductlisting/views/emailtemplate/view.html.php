<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewEmailtemplate extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'emailtemplate') {          // email template
            $templatefor = JRequest::getVar('tf');
            switch ($templatefor) {
                case 'pd-prad' : $text = JText::_('PURCHASE_REQUEST_ADMIN_TEMPLATE'); break;
                case 'pd-vfad' : $text = JText::_('PURCHASE_VERIFIED_ADMIN_TEMPLATE'); break;
                case 'pd-prus' : $text = JText::_('PURCHASE_REQUEST_USER_TEMPLATE'); break;
                case 'pd-vfus' : $text = JText::_('PURCHASE_REQUEST_USER_TEMPLATE'); break;
                case 'tl-frnd' : $text = JText::_('SEND_TO_FRIEND'); break;
            }
            JToolBarHelper :: title(JText::_('EMAIL_TEMPLATES') . ' <small><small>[' . $text . '] </small></small>', 'emailtemplate');
            JToolBarHelper :: save('emailtemplate.saveemailtemplate');
            $template = $this->getITModel('emailtemplate')->getTemplate($templatefor);
            $this->assignRef('template', $template);
        }

        $this->assignRef('pagination', $pagination);
        $this->assignRef('option', $option);
        $this->assignRef('config', $config);
        $this->assignRef('uid', $uid);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('msg', $msg);
        parent :: display($tpl);
    }

}

?>
