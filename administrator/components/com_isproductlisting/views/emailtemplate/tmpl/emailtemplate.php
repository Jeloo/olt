<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
JHTML::_('behavior.calendar');
JHTML::_('behavior.formvalidation');

$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat, '-', 0);
$firstvalue = substr($dateformat, 0, $firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat, '-', $firstdash);
$secondvalue = substr($dateformat, $firstdash, $seconddash - $firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash, strlen($dateformat) - $seconddash);
$js_dateformat = '%' . $firstvalue . '-%' . $secondvalue . '-%' . $thirdvalue;
?>
<script language="javascript">
    // for joomla 1.6
    Joomla.submitbutton = function(task){
        if (task == ''){
            return false;
        }else{
            if (task == 'emailtemplate.saveemailtemplate'){
                returnvalue = validate_form(document.adminForm);
            }else returnvalue  = true;
            if (returnvalue){
                Joomla.submitform(task);
                return true;
            }else return false;
        }
    }
    function validate_form(f)
    {
        if (document.formvalidator.isValid(f)) {
            f.check.value='<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
        }
        else {
            alert('<?php echo JText::_('SOME_VALUES_ARE_NOT_ACCEPTABLE_PLEASE_RETRY'); ?>');
            return false;
        }
        return true;
    }
</script>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('EMAIL_TEMPLATES'); ?></div>
            <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" >
                <input type="hidden" name="check" value="post"/>
                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
                    <tr class="row1">
                        <td colspan="3"><label id="subjectmsg" for="subject" style="float:left;"><?php echo JText::_('SUBJECT'); ?></label>&nbsp;<font color="red">*</font>&nbsp;:&nbsp;&nbsp;&nbsp;
                            <input class="inputbox required" type="text" name="subject" id="subject" size="135" maxlength="255" value="<?php if (isset($this->template)) echo $this->template->subject; ?>" />
                        </td>
                    </tr>
                    <tr class="row2">
                        <td colspan="3"><label id="descriptionmsg" style="float:left;"for="body"><strong><?php echo JText::_('BODY'); ?></strong></label>&nbsp;<font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" width="600">
                            <?php
                            $editor = JFactory::getEditor();
                            if (isset($this->template))
                                echo $editor->display('body', $this->template->body, '550', '300', '60', '20', false);
                            else
                                echo $editor->display('body', '', '550', '300', '60', '20', false);
                            ?>	
                        </td>
                        <td width="35%" valign="top">
                            <table  cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">

                                <tr class="row1"><td> <strong><u><?php echo JText::_('PARAMETERS'); ?></u></strong></td></tr>
                                <?php if ($this->template->templatefor == 'purchase-requestadmin' ) { ?>
                                    <tr><td>{USER_NAME} :  <?php echo JText::_('USER_NAME'); ?></td></tr>
                                    <tr><td>{USER_EMAIL} :  <?php echo JText::_('USER_EMAIL_ADDRESS'); ?></td></tr>
                                    <tr><td>{SITE_TITLE} :  <?php echo JText::_('SITE_TITLE'); ?></td></tr>
                                    <tr><td>{SHOPPING_CART_ID} :  <?php echo JText::_('SHOPPING_CART_ID'); ?></td></tr>
                                    <tr><td>{CART_TOTAL_AMOUNT} :  <?php echo JText::_('CART_TOTAL_AMOUNT'); ?></td></tr>
                                    <tr><td>{DATE} :  <?php echo JText::_('DATE'); ?></td></tr>
                                <?php } elseif ($this->template->templatefor == 'purchase-verifiedadmin') { ?>
                                    <tr><td>{USER_NAME} :  <?php echo JText::_('USER_NAME'); ?></td></tr>
                                    <tr><td>{USER_EMAIL} :  <?php echo JText::_('USER_EMAIL_ADDRESS'); ?></td></tr>
                                    <tr><td>{SITE_TITLE} :  <?php echo JText::_('SITE_TITLE'); ?></td></tr>
                                    <tr><td>{SHOPPING_CART_ID} :  <?php echo JText::_('SHOPPING_CART_ID'); ?></td></tr>
                                    <tr><td>{CART_TOTAL_AMOUNT} :  <?php echo JText::_('CART_TOTAL_AMOUNT'); ?></td></tr>
                                    <tr><td>{VERIFIED_DATE} :  <?php echo JText::_('VERIFIED_DATE'); ?></td></tr>
                                    <tr><td>{DATE} :  <?php echo JText::_('DATE'); ?></td></tr>
                                <?php }elseif ($this->template->templatefor == 'purchase-requestuser' ) { ?>
                                    <tr><td>{USER_NAME} :  <?php echo JText::_('USER_NAME'); ?></td></tr>
                                    <tr><td>{USER_EMAIL} :  <?php echo JText::_('USER_EMAIL_ADDRESS'); ?></td></tr>
                                    <tr><td>{SITE_TITLE} :  <?php echo JText::_('SITE_TITLE'); ?></td></tr>
                                    <tr><td>{SHOPPING_CART_ID} :  <?php echo JText::_('SHOPPING_CART_ID'); ?></td></tr>
                                    <tr><td>{CART_TOTAL_AMOUNT} :  <?php echo JText::_('CART_TOTAL_AMOUNT'); ?></td></tr>
                                    <tr><td>{DATE} :  <?php echo JText::_('DATE'); ?></td></tr>
                                <?php } elseif ($this->template->templatefor == 'purchase-verifieduser') { ?>
                                    <tr><td>{USER_NAME} :  <?php echo JText::_('USER_NAME'); ?></td></tr>
                                    <tr><td>{USER_EMAIL} :  <?php echo JText::_('USER_EMAIL_ADDRESS'); ?></td></tr>
                                    <tr><td>{SITE_TITLE} :  <?php echo JText::_('SITE_TITLE'); ?></td></tr>
                                    <tr><td>{SHOPPING_CART_ID} :  <?php echo JText::_('SHOPPING_CART_ID'); ?></td></tr>
                                    <tr><td>{CART_TOTAL_AMOUNT} :  <?php echo JText::_('CART_TOTAL_AMOUNT'); ?></td></tr>
                                    <tr><td>{VERIFIED_DATE} :  <?php echo JText::_('VERIFIED_DATE'); ?></td></tr>
                                    <tr><td>{DATE} :  <?php echo JText::_('DATE'); ?></td></tr>
                                <?php } elseif ($this->template->templatefor == 'tell-friend') { ?>
                                    <tr><td>{SITE_TITLE} :  <?php echo JText::_('SITE_TITLE'); ?></td></tr>
                                    <tr><td>{SENDER_NAME} :  <?php echo JText::_('SENDER_NAME'); ?></td></tr>
                                    <tr><td>{PRODCUT_TITLE} :  <?php echo JText::_('PRODUCT_TITLE'); ?></td></tr>
                                    <tr><td>{BRAND} :  <?php echo JText::_('BRAND'); ?></td></tr>
                                    <tr><td>{CATEGORY} :  <?php echo JText::_('CATEGORY'); ?></td></tr>
                                    <tr><td>{SUB_CATEGORY} :  <?php echo JText::_('SUB_CATEGORY'); ?></td></tr>
                                    <tr><td>{PRICE} :  <?php echo JText::_('PRICE'); ?></td></tr>
                                    <tr><td>{MESSAGE} :  <?php echo JText::_('MESSAGE'); ?></td></tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" height="5"></td>
                    <tr>
                </table>


                <?php
                if (isset($this->template)) {
                    if (($this->template->created == '0000-00-00 00:00:00') || ($this->template->created == ''))
                        $curdate = date('Y-m-d H:i:s');
                    else
                        $curdate = $this->template->created;
                }else
                    $curdate = date('Y-m-d H:i:s');
                ?>
                <input type="hidden" name="created" value="<?php echo $curdate; ?>" />
                <input type="hidden" name="view" value="vehicles" />
                <input type="hidden" name="uid" value="<?php echo $this->uid; ?>" />
                <input type="hidden" name="id" value="<?php echo $this->template->id; ?>" />
                <input type="hidden" name="templatefor" value="<?php echo $this->template->templatefor; ?>" />
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                <input type="hidden" name="task" value="emailtemplate.saveemailtemplate" />
                <input type="hidden" name="Itemid" id="Itemid" value="<?php echo $this->Itemid; ?>" />



            </form>
    </div>>
</div>