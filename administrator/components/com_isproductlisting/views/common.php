<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$mainframe = JFactory::getApplication();
$user = JFactory::getUser();
$uid = $user->id;
$itemid = JRequest::getVar('Itemid');
$layout = $this->getLayout('layout');
$msg = JRequest :: getVar('msg');
$option = 'com_isproductlisting';
$isNew = true;
$config = array();
$session = JFactory::getSession();
if (!$session->has('configuration', 'isproductlisting')) {
    $config = $this->getITModel('configuration')->getConfiginArray('default');
    $session->set($config, NULL, 'isproductlisting');
} else {
    $config = $session->get('configuration', NULL, 'isproductlisting');
}
$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
$limitstart = $mainframe->getUserStateFromRequest($option . '.limitstart', 'limitstart', 0, 'int');
$this->assignRef('user', $user);
?>