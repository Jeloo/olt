<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
JHTML::_('behavior.calendar');

$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat, '-', 0);
$firstvalue = substr($dateformat, 0, $firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat, '-', $firstdash);
$secondvalue = substr($dateformat, $firstdash, $seconddash - $firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash, strlen($dateformat) - $seconddash);
$js_dateformat = '%' . $firstvalue . '-%' . $secondvalue . '-%' . $thirdvalue;
?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('PRODUCT_FEEDBACKS_APPROVAL'); ?></div>
            <form action="index.php" method="post" name="adminForm" id="adminForm">
                <table class="adminlist">
                    <thead>
                        <tr>
                            <th width="20">
                                <?php if (JVERSION < 3) { ?>
                                    <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->feedbacks); ?>);" />
                                <?php } else { ?>
                                    <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                                <?php } ?>
                            </th>
                            <th><?php echo JText::_('PRODUCT_TITLE'); ?></th>
                            <th class="title"><?php echo JText::_('FEED'); ?></th>
                            <th><?php echo JText::_('STATUS'); ?></th>
                            <th><?php echo JText::_('CREATED'); ?></th>
                            <th><?php echo JText::_('ACTIONS'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        jimport('joomla.filter.output');
                        $k = 0;
                        for ($i = 0, $n = count($this->feedbacks); $i < $n; $i++) {
                            $feed = $this->feedbacks[$i];
                            $checked = JHTML::_('grid.id', $i, $feed->id);
                            ?>
                            <tr valign="top" class="<?php echo "row$k"; ?>">

                                <td><?php echo $checked; ?></td>
                                <td style="text-align: center;"> <?php echo $feed->title; ?></td>
                                <td style="text-align: center;">
                                <?php
                                    $lenght = 50;
                                    $des = $feed->feed . " ";
                                    $des = substr($des, 0, $lenght);
                                    $des = substr($des, 0, strrpos($des, ' '));
                                    $des = $des . "...";
                                    echo $des;
                                ?>
                                </td>
                                <td style="text-align: center;">
                                <?php
                                    if ($feed->status == 1)
                                        echo "<font color='green'>" . JText::_('PUBLISHED') . "</font>";
                                    else
                                        echo "<font color='red'>" . JText::_('NOT_PUBLISHED') . "</font>";
                                ?>
                                </td>
                                <td style="text-align: center;"> <?php echo $feed->created; ?></td>
                                <td style="text-align: center;">
                                    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>', 'feedback.publishfeedback')">
                                        <img src="../components/com_isproductlisting/images/tick.png" width="16" height="16" border="0" alt="<?php echo JText::_('PUBLISH'); ?>" /></a>
                                    &nbsp;&nbsp; - &nbsp;&nbsp
                                    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>', 'feedback.unpublishfeedback')">
                                        <img src="../components/com_isproductlisting/images/publish_x.png" width="16" height="16" border="0" alt="<?php echo JText::_('UNPUBLISH'); ?>" /></a>
                                </td>


                            </tr>
                            <?php
                            $k = 1 - $k;
                        }
                        ?>

                        <tr><td colspan="14"><?php echo $this->pagination->getListFooter(); ?></td></tr>
                    </tbody>
                </table>
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                <input type="hidden" name="task" value="" />
                <input type="hidden" name="boxchecked" value="0" />
                <input type="hidden" name="layout" value="feedbackapprovalqueue" />  <!--for paging -->
                <input type="hidden" name="view" value="feedback" /> <!--for paging -->
                <input type="hidden" name="c" value="feedback" /> <!--for paging -->

            </form>
    </div>
</div>