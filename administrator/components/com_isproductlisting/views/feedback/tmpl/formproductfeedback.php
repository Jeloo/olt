<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
JHTML::_('behavior.calendar');

$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat,'-',0);
$firstvalue = substr($dateformat, 0,$firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat,'-',$firstdash);
$secondvalue = substr($dateformat, $firstdash,$seconddash-$firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash,strlen($dateformat)-$seconddash);
$js_dateformat = '%'.$firstvalue.'-%'.$secondvalue.'-%'.$thirdvalue;

?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('FORM_PRODUCT_FEEDBACKS'); ?></div>
            <form action="index.php" method="post" name="adminForm" id="adminForm" >
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminform">
                    <?php if ($this->msg != '') { ?>
                        <tr>
                            <td colspan="2" align="center"><font color="red"><strong><?php echo JText::_($this->msg); ?></strong></font></td>
                        </tr>
                        <tr><td colspan="2" height="10"></td></tr>	
                        <?php } ?>
                    <tr class="row1">
                        <td width="20%" valign="top"><label id="feedmsg" for="feed"><?php echo JText::_('FEED'); ?></label></td>
                        <td width="60%">
                            <?php 
                                    $editor = JFactory::getEditor();
                                    $feed = '';
                                    if(isset($this->feed->feed)) $feed = $this->feed->feed;
                                    echo $editor->display('feed', $feed, '550', '300', '60', '20', false);
                             ?>
                        </td>
                    </tr>
                    <tr class="row0">
                        <td valign="top"><label id="statusmsg" for="status"><?php echo JText::_('PUBLISHED'); ?></label></td>
                        <td><input type="checkbox" name="status" id="status" value="1" <?php if (isset($this->feed)) { if ($this->feed->status == '1') echo 'checked'; } ?>/></td>
                    </tr>
                    <tr><td colspan="2" height="10"></td></tr>
                    <tr class="row1">
                        <td  colspan="2" align="center">
                            <input type="submit" class="button" name="submit_app" onclick="return validate_form(document.adminForm)" value="<?php echo JText::_('SAVE'); ?>" />
                        </td>
                    </tr>

                </table>
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                <input type="hidden" name="task" value="feedback.saveproductfeedback" />
                <input type="hidden" name="id" value="<?php echo $this->feed->id; ?>" />
            </form>
    </div>
</div>
