<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewFeedback extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'formproductfeedback') { //  form product feed back
            $id = JRequest :: getVar('id', '0');
            $result = $this->getITModel('feedback')->getFeedbackById($id);
            if (isset($result->id)) if ($result->id) $isNew = false;
            $text = $isNew ? JText :: _('ADD') : JText :: _('EDIT');
            JToolBarHelper :: title(JText :: _('PRODUCT_FEEDBACK') . '<small><small> [' . $text . ']</small></small>', 'feedback');
            $this->assignRef('feed', $result);
            JToolBarHelper :: save('feedback.saveproductfeedback');
            if ($isNew)
                JToolBarHelper :: cancel('feedback.cancelfeedback');
            else
                JToolBarHelper :: cancel('feedback.cancelfeedback', 'Close');
        } elseif ($layout == 'productfeedbacks') {
            JToolBarHelper :: title(JText :: _('FEED_BACK_LIST'), 'feedback');
            $result = $this->getITModel('feedback')->getAllFeedBacks($limitstart, $limit);
            JToolBarHelper :: editList('feedback.editproductfeedback');
            JToolBarHelper :: deleteList('ARE_YOU_SURE_TO_DELETE', 'feedback.removeproductfeedback');
            $this->assignRef('feedbacks', $result[0]);
            $total = $result[1];
            $pagination = new JPagination($total, $limitstart, $limit);
        } elseif ($layout == 'feedbackapprovalqueue') {
            JToolBarHelper :: title(JText :: _('FEED_BACK_APPROVAL_QUEUE'), 'feedbackapprovalqueue');
            $result = $this->getITModel('feedback')->getAllFeedBacksApproval($limitstart, $limit);
            JToolBarHelper :: editList('feedback.editproductfeedback');
            JToolBarHelper :: deleteList('ARE_YOU_SURE_TO_DELETE', 'feedback.removeproductfeedback');
            $this->assignRef('feedbacks', $result[0]);
            $total = $result[1];
            $pagination = new JPagination($total, $limitstart, $limit);
        }

        $this->assignRef('pagination', $pagination);
        $this->assignRef('option', $option);
        $this->assignRef('config', $config);
        $this->assignRef('uid', $uid);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('msg', $msg);
        parent :: display($tpl);
    }

}

?>
