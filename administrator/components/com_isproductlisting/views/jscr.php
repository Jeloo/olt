<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
?>
<div id="footer_wrapper">
    <img src="administrator/components/com_isproductlisting/include/images/footerlogo.png" />
    <span class="footer_text">Copyright 2014, <a href="http://itfant.com" target="_blank" >ITFant</a></span>
</div>
