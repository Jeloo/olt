<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewCategory extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'categories') { // Categories
            $searchtitle = $mainframe->getUserStateFromRequest($option . 'filter_title', 'filter_title', '', 'string');
            $statusid = $mainframe->getUserStateFromRequest($option . 'filter_status', 'filter_status', '', 'int');
            $result = $this->getITModel('category')->getAllCategories($searchtitle, $statusid, $limit, $limitstart);
            $total = $result[1];
            if ($total <= $limitstart) $limitstart = 0;
            $pagination = new JPagination($total, $limitstart, $limit);
            $this->assignRef('categories', $result[0]);
            $this->assignRef('pagination', $pagination);
            $this->assignRef('lists', $result[2]);

            JToolBarHelper :: title(JText :: _('CATEGORIES'), 'categories');
            JToolBarHelper :: addNew('category.editcategory');
            JToolBarHelper :: editList('category.editcategory');
            JToolBarHelper :: deleteList(JText::_('ARE_YOU_SURE_TO_DELETE'), 'category.removecategory');
            JHTML::_('behavior.formvalidation');
        }elseif ($layout == 'formcategory') { // form category
            $cids = JRequest :: getVar('cid', array(0), '', 'array');
            $c_id = $cids[0];
            $result = $this->getITModel('category')->getCategoryForForm($c_id);
            $this->assignRef('categoryid', $c_id);
            $this->assignRef('category', $result[0]);

            if (isset($result[0]->id)) $isNew = false;
            $text = $isNew ? JText :: _('ADD') : JText :: _('EDIT');
            JToolBarHelper :: title(JText :: _('CATEGORIES') . '<small><small> [' . $text . ']</small></small>', 'categories');
            JToolBarHelper :: save('category.savecategory', 'SAVE');
            if ($isNew) JToolBarHelper :: cancel('category.cancelcategory'); 
            else JToolBarHelper :: cancel('category.cancelcategory', 'Close');
            JHTML::_('behavior.formvalidation');
        }

        $this->assignRef('pagination', $pagination);
        $this->assignRef('option', $option);
        $this->assignRef('config', $config);
        $this->assignRef('uid', $uid);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('msg', $msg);
        parent :: display($tpl);
    }

}

?>
