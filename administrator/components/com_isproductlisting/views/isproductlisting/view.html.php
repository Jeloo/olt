<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewIsproductlisting extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'controlpanel') { // control panel
            JToolBarHelper :: title(JText :: _('CONTROL_PANEL'), 'controlpanel');
            $ck = $this->getITModel('configuration')->getCheckCronKey();
            if ($ck == false) {
                $this->getITModel('configuration')->genearateCronKey();
            }
            $getck = $this->getITModel('configuration')->getCronKey(md5(date('Y-m-d')));
            $this->assignRef('ck', $getck);
        } elseif ($layout == 'info') {          // employer package info
            JToolBarHelper :: title(JText::_('INFORMATION'), 'info');
        }
        $this->assignRef('pagination', $pagination);
        $this->assignRef('theme', $theme);
        $this->assignRef('config', $config);
        $this->assignRef('option', $option);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('uid', $uid);
        $this->assignRef('socailsharing', $socailsharing);
        parent :: display($tpl);
    }

}

?>
