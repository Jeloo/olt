<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
global $mainframe;
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');

?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('PRO_VERSION'); ?></div>
                        PRO VERSION
    </div>
</div>
		
