<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
global $mainframe;
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');

?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('CONTROL_PANEL'); ?></div>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=brand&view=brand&layout=brands">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/brandlogo.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('BRANDS'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=product&view=product&layout=products">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/products.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('PRODUCTS'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=product&view=product&layout=productsearch">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/searchproducts.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('SEARCH_PRODUCTS'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=category&view=category&layout=categories">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/categories.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('CATEGORIES'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/subcategories.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('SUB_CATEGORIES'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=configuration&view=configuration&layout=themes">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/themes.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('THEMES'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=configuration&view=configuration&layout=configurations">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/configuration.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('CONFIGURATION'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/paymentmethodconfiguration.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('PAYMENT_METHODCONFIGURATION'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/paymenthistory.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('PAYMENT_HISTORY'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/footer.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('REMOVE_FOOTER'); ?></span>
            </div>
        </a>
         <a class="cp_icon_anchor" href="">
            <div id="cp_icon_wrapper">
                                                <?php
                    $url = 'http://www.itfant.com/productlisting/getlatestversion.php';
                    $pvalue = "dt=".date('Y-m-d');
                    if  (in_array  ('curl', get_loaded_extensions())) {
                            $ch = curl_init();
                            curl_setopt($ch,CURLOPT_URL,$url);
                            curl_setopt($ch,CURLOPT_POST,8);
                            curl_setopt($ch,CURLOPT_POSTFIELDS,$pvalue);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
                            $curl_errno = curl_errno($ch);
                            $curl_error = curl_error($ch);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            if($result == $this->config['versioncode']){ ?>
                                <img src="components/com_isproductlisting/include/images/systemupdated.png" height="56" width="56" title="<?php echo JText::_('YOUR_SYSTEM_IS_UP_TO_DATE'); ?>">
                                <span class="icon_text" ><?php echo JText::_('YOUR_SYSTEM_IS_UP_TO_DATE'); ?></span>
                            <?php	
                            }elseif($result){ ?>
                                <img src="components/com_isproductlisting/include/images/systemupdated.png" height="56" width="56" title="<?php echo JText::_('NEW_VERSION_AVAILABLE'); ?>">
                                <span class="icon_text"><?php echo JText::_('NEW_VERSION_AVAILABLE'); ?></span>
                            <?php			
                            }else{ ?>
                                <img src="components/com_isproductlisting/include/images/systemupdated.png" height="56" width="56" title="<?php echo JText::_('UNABLE_CONNECT_TO_SERVER'); ?>">
                                <span class="icon_text"><?php echo JText::_('UNABLE_TO_CONNECT'); ?></span>
                            <?php			
                            }
                    }else{ ?>
                            <img src="components/com_isproductlisting/include/images/systemupdated.png" height="56" width="56" title="<?php echo JText::_('UNABLE_CONNECT_TO_SERVER'); ?>">
                            <span class="icon_text"><?php echo JText::_('UNABLE_TO_CONNECT'); ?></span>
                                        <?php } ?>
            </div>
        </a>
        <a class="cp_icon_anchor" href="http://forum.itfant.com/index.php" target="_blank">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/forum.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('FORUM'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="http://doc.itfant.com/" target="_blank">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/documentation.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('DOCUMENTATION'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="http://support.itfant.com/" target="_blank">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/supportticket.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('SUPPORT_TICKET'); ?></span>
            </div>
        </a>
        <a class="cp_icon_anchor" href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=info">
            <div id="cp_icon_wrapper">
                <img src="components/com_isproductlisting/include/images/about.png" width="56px" height="56px" />
                <span class="icon_text"><?php echo JText::_('ABOUT'); ?></span>
            </div>
        </a>
    </div>
</div>
		
