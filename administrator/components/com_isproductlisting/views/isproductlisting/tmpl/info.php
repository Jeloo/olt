<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('INFORMATION'); ?></div>
        <div id="product_listing_info">
            <span id="product_listing_subheading"><?php echo JText::_('COMPONENT_DETAILS'); ?></span>
            <span id="product_listing_data">
                <span id="product_listing_data_left"><?php echo JText::_('CREATED_BY'); ?></span>
                <span id="product_listing_data_right"><?php echo JText::_('Shoaib Rehmat Ali'); ?></span>
            </span>
            <span id="product_listing_data">
                <span id="product_listing_data_left"><?php echo JText::_('COMPANY'); ?></span>
                <span id="product_listing_data_right"><?php echo JText::_('IT Fant'); ?></span>
            </span>
            <span id="product_listing_data">
                <span id="product_listing_data_left"><?php echo JText::_('PROJECT_CODE'); ?></span>
                <span id="product_listing_data_right"><?php echo JText::_('com_isproductlisting'); ?></span>
            </span>
            <span id="product_listing_data">
                <span id="product_listing_data_left"><?php echo JText::_('VERSION'); ?></span>
                <span id="product_listing_data_right"><?php echo JText::_('1.0.1 -r'); ?></span>
            </span>
            <span id="product_listing_data">
                <span id="product_listing_data_left"><?php echo JText::_('DESCRIPTION'); ?></span>
                <span id="product_listing_data_right"><?php echo JText::_('A component for selling and purchasing products.'); ?></span>
            </span>
            <span id="product_listing_data_web"><?php echo '<a href="http://itfant.com" target="_blank" >' . JText::_('itfant.com') . '</a>'; ?></span>
        </div>
    </div>
</div>