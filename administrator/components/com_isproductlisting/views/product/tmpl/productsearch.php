<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
$document->addScript('components/com_isproductlisting/include/js/jquery.js');
if($this->config['date_format']=='m/d/Y') $dash = '/';else $dash = '-';
$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat,$dash,0);
$firstvalue = substr($dateformat, 0,$firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat,$dash,$firstdash);
$secondvalue = substr($dateformat, $firstdash,$seconddash-$firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash,strlen($dateformat)-$seconddash);
$js_dateformat = '%'.$firstvalue.$dash.'%'.$secondvalue.$dash.'%'.$thirdvalue;
?>
<script type="text/javascript">
    function getSubCategory(objcategory){
        jQuery("#subcategory").html('<img src="components/com_isproductlisting/include/images/waiting.gif" />');
        jQuery.post('index.php?option=com_isproductlisting&c=isproductlisting&task=subcategory.getsubcategorybycategoryid',{categoryid:objcategory.value},function(data){
            if(data){
                jQuery("#subcategory").html(data);
            }
        });
    }
</script>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('PRODUCTS_SEARCH'); ?></div>
            <form action="<?php echo JRoute::_('index.php?option=com_isproductlisting&c=product&view=product&layout=productsearch_results'); ?>"  method="post" name="adminForm" id="adminForm"   >
                <table cellpadding="8" cellspacing="0" border="0" width="100%">                           <!--Main Table Start-->
                                <tr class="<?php echo $td[$k];$k = 1 - $k; ?>">
                                    <td width="25%" valign="top"><label id="titlemsg" for="title"><?php echo JText::_('TITLE'); ?></label></td>
                                    <td><input  class="inputbox" type="text" id="title" name="title" value=""/></td>
                                </tr>
                                <tr class="<?php echo $td[$k];$k = 1 - $k; ?>">
                                    <td width="25%" valign="top" ><label id="categoryidmsg" for="categoryid"><?php echo JText::_('CATEGORY'); ?></label></td>
                                    <td><?php echo $this->lists['categoryid']; ?></td>
                                </tr>
                                <tr class="<?php echo $td[$k];$k = 1 - $k; ?>">
                                    <td valign="top" ><label id="subcategoryidmsg" for="subcategoryid"><?php echo JText::_('SUB_CATEGORY'); ?></label></td>
                                    <td id="subcategory"><?php echo $this->lists['subcategoryid']; ?></td>
                                </tr>
                                <tr class="<?php echo $td[$k];$k = 1 - $k; ?>">
                                    <td valign="top" ><label id="brandidmsg" for="brandid"><?php echo JText::_('BRAND'); ?></label></td>
                                    <td><?php echo $this->lists['brandid']; ?></td>
                                </tr>
                                <tr class="<?php echo $td[$k];$k = 1 - $k; ?>">
                                    <td valign="top" ><label id="pricemsg" for="price"><?php echo JText::_('PRICE'); ?></label>&nbsp;</td>
                                    <td><input type="text" name="price" size="40" id="price"  value="" /></td>
                                </tr>
                                <tr class="<?php echo $td[$k];$k = 1 - $k; ?>">
                                    <td valign="top" ><label id="descriptionmsg" for="description"><?php echo JText::_('DESCRIPTION'); ?></label>&nbsp;</td>
                                    <td>
                                        <?php
                                            $editor = JFactory::getEditor();
                                            echo $editor->display('description', '', '550', '300', '60', '20', false);
                                        ?>
                                    </td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="listproductexpirydatemsg" for="listproductexpirydate"><?php echo JText::_('LIST_PRODUCT_EXPIRY_DATE'); ?></label></td>
                                    <td width="60%"><?php echo JHTML::_('calendar', '','listproductexpirydate', 'listproductexpirydate',$js_dateformat,array('class'=>'inputbox', 'size'=>'10',  'maxlength'=>'19')); ?></td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="statusmsg" for="status"><?php echo JText::_('STATUS'); ?></label></td>
                                    <td width="60%"><?php echo $this->lists['status'];?></td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="createdmsg" for="created"><?php echo JText::_('CREATED'); ?></label></td>
                                    <td width="60%"><?php echo JHTML::_('calendar', '','created', 'created',$js_dateformat,array('class'=>'inputbox', 'size'=>'10',  'maxlength'=>'19')); ?></td>
                                </tr>
                    <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                    <input type="hidden" name="isproductsearch" value="1" />
                    <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" class="button" onclick="return formSubmit();" value="<?php echo JText::_('SEARCH_PRODUCT'); ?>" />
                        </td>
                    </tr>

                </table>
            </form>
    </div>
</div>