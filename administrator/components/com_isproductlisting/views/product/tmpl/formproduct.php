<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
$document->addScript('components/com_isproductlisting/include/js/jquery.js');
JHTMLBehavior::formvalidation();
$k = 0;
if($this->config['date_format']=='m/d/Y') $dash = '/';else $dash = '-';
$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat,$dash,0);
$firstvalue = substr($dateformat, 0,$firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat,$dash,$firstdash);
$secondvalue = substr($dateformat, $firstdash,$seconddash-$firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash,strlen($dateformat)-$seconddash);
$js_dateformat = '%'.$firstvalue.$dash.'%'.$secondvalue.$dash.'%'.$thirdvalue;
?>

<script type="text/javascript">
    // for joomla 1.6
    Joomla.submitbutton = function(task){
        if (task == ''){
            return false;
        }else{
            if (task == 'product.saveproduct'){
                returnvalue = validate_form(document.adminForm);
            }else returnvalue  = true;
            if (returnvalue){
                Joomla.submitform(task);
                return true;
            }else return false;
        }
    }
    function validate_form(f){
        if (document.formvalidator.isValid(f)) {
            f.check.value='<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
        }
        else {
            alert('<?php echo JText::_('SOME_VALUES_ARE_NOT_ACCEPTABLE_PLEASE_RETRY');?>');
            return false;
        }
        return true;
    }
    function getSubCategory(objcategory){
        jQuery("#subcategory").html('<img src="components/com_isproductlisting/include/images/waiting.gif" />');
        jQuery.post('index.php?option=com_isproductlisting&c=isproductlisting&task=subcategory.getsubcategorybycategoryid',{categoryid:objcategory.value},function(data){
            if(data){
                jQuery("#subcategory").html(data);
            }
        });
    }
</script>

<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('FORM_PRODUCT'); ?></div>
            <form action="index.php" method="POST" name="adminForm" id="adminForm" enctype="multipart/form-data">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminform">
                    <?php if ($this->msg != '') { ?>
                        <tr>
                            <td colspan="2" align="center"><font color="red"><strong><?php echo JText::_($this->msg); ?></strong></font></td>
                        </tr>
                    <?php } ?>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="titleymsg" for="title"><?php echo JText::_('TITLE'); ?>&nbsp;<font color="red">*</font></label></td>
                                    <td width="60%"><input class="inputbox required" type="text" id="title" name="title" size="40" maxlength="255" value="<?php if (isset($this->product)) echo $this->product->title; ?>" />
                                    </td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="imageymsg" for="image"><?php echo JText::_('CATEGORY'); ?></label></td>
                                    <td width="60%"><?php echo $this->lists['categoryid']; ?></td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td valign="top"><label id="statusmsg" for="status"><?php echo JText::_('SUB_CATEGORY'); ?></label></td>
                                    <td width="60%" id="subcategory"><?php echo $this->lists['subcategoryid']; ?></td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td valign="top"><label id="statusmsg" for="status"><?php echo JText::_('BRAND'); ?></label></td>
                                    <td width="60%"><?php echo $this->lists['brandid']; ?></td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="pricemsg" for="price"><?php echo JText::_('PRICE'); ?></label></td>
                                    <td width="60%"><input class="inputbox" type="text" id="price" name="price" size="40" maxlength="255" value="<?php if (isset($this->product)) echo $this->product->price; ?>" />
                                    </td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="quantitymsg" for="quantity"><?php echo JText::_('QUANTITY'); ?></label></td>
                                    <td width="60%"><input class="inputbox" type="text" id="quantity" name="quantity" size="40" maxlength="255" value="<?php if (isset($this->product)) echo $this->product->quantity; ?>" />
                                    </td>
                                </tr>                                
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="descriptionmsg" for="description"><?php echo JText::_('DESCRIPTION'); ?></label></td>
                                    <td width="60%">
                                        <?php
                                            $editor = JFactory::getEditor();
                                            $description = '';
                                            if(isset($this->product->description)) $description = $this->product->description;
                                            echo $editor->display('description', $description, '550', '300', '60', '20', false);
                                        ?>
                                    </td>
                                </tr>
                                <tr class="row<?php echo $k; $k = 1- $k; ?>">
                                    <td colspan="2">
                                        <div class="it_data_sheet_heading"><?php echo JText::_('DATA_SHEET'); ?></div>
                                        <div class="it_data_sheet_heading"><a href="" id="it_add_more_field_set"><?php echo JText::_('ADD_MORE_FIELD_SET'); ?></a></div>
                                    </td>
                                </tr>
                                <tr class="row<?php echo $k; $k = 1- $k; ?>">
                                    <td id="it_data_sheet_label" width="20%" valign="top">
                                        <span class="it_data_sheet_title"><?php echo JText::_('FIELD_TITLE'); ?></span>
                                        <?php if(isset($this->product->datasheet) && !empty($this->product->datasheet)){
                                            $array = json_decode($this->product->datasheet);
                                            foreach($array AS $title){
                                                echo '<input type="text" class="it_fant_title" name="field_title[]" value="'.$title->title.'" />';
                                            }
                                        }else{ ?>
                                        <input type="text" class="it_fant_title" name="field_title[]" value="" />
                                        <?php } ?>
                                    </td>
                                    <td id="it_data_sheet_value" width="20%" valign="top">
                                        <span class="it_data_sheet_label"><?php echo JText::_('FIELD_VALUE'); ?></span>
                                        <?php if(isset($this->product->datasheet) && !empty($this->product->datasheet)){
                                            $array = json_decode($this->product->datasheet);
                                            foreach($array AS $value){
                                                echo '<input type="text" class="it_fant_value" name="field_value[]" value="'.$value->value.'" />';
                                            }
                                        }else{ ?>
                                        <input type="text" class="it_fant_value" name="field_value[]" value="" />
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="listproductexpirydatemsg" for="listproductexpirydate"><?php echo JText::_('LIST_PRODUCT_EXPIRY_DATE'); ?></label></td>
                                    <td width="60%"><?php $listproductexpirydate = isset($this->product->listproductexpirydate) ? $this->product->listproductexpirydate: date('Y-m-d H:i:s'); echo JHTML::_('calendar', date($this->config['date_format'],  strtotime($listproductexpirydate)),'listproductexpirydate', 'listproductexpirydate',$js_dateformat,array('class'=>'inputbox', 'size'=>'10',  'maxlength'=>'19')); ?></td>
                                </tr>
                                <tr class="row<?php echo $k;$k = 1-$k;?>">
                                    <td width="20%" valign="top"><label id="statusmsg" for="status"><?php echo JText::_('STATUS'); ?></label></td>
                                    <td width="60%"><?php echo $this->lists['status']; ?></td>
                                </tr>
                   <tr><td colspan="2" height="10"></td></tr>
                    <tr class="row<?php echo $k;$k = 1-$k;?>">
                        <td  colspan="2" align="center">
                            <input type="submit" class="button" name="submit_app" onclick="return validate_form(document.adminForm)" value="<?php echo JText::_('SAVE'); ?>" />
                        </td>
                    </tr>

                </table>
                <input type="hidden" name="id" value="<?php if (isset($this->product->id)) echo $this->product->id; ?>" />
                <input type="hidden" name="check" value="" />
                <input type="hidden" name="userid" value="<?php echo $this->user->id;?>" />
                <input type="hidden" name="task" value="product.saveproduct" />
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
            </form>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
       jQuery("a#it_add_more_field_set").click(function(e){
          e.preventDefault();
          var fieldtitlehtml = '<input type="text" class="it_fant_title" name="field_title[]" value="" />';
          var fieldvaluehtml = '<input type="text" class="it_fant_value" name="field_value[]" value="" />';
          jQuery("td#it_data_sheet_label").append(fieldtitlehtml);
          jQuery("td#it_data_sheet_value").append(fieldvaluehtml);
       }); 
    });
</script>