<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
if(JVERSION < 3)
    $document->addScript('components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addScript('components/com_isproductlisting/include/js/jquery-form.js');
JHTMLBehavior::formvalidation();
$k = 0;
?>
<style>
#progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
#bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
#percent { position: absolute;display:inline-block; top:3px; left:48%; }
div.imageWrap{float:left;background:#E5EFFD;border:1px solid #B2BBD0;border-radius:4px;padding:5px 5px 10px 5px;margin: 10px 0px 0px 10px;}
div.imageWrap span.links{display:block;padding:5px 5px 0px 5px;}
</style>
<script>
jQuery(document).ready(function()
{
 
    var options = { 
    beforeSend: function() 
    {
        jQuery("#progress").show();
        //clear everything
        jQuery("#bar").width('0%');
        jQuery("#message").html("");
        jQuery("#percent").html("0%");
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        jQuery("#bar").width(percentComplete+'%');
        jQuery("#percent").html(percentComplete+'%');
 
    },
    success: function() 
    {
        jQuery("#bar").width('100%');
        jQuery("#percent").html('100%');
 
    },
    complete: function(response) 
    {
//        jQuery("#message").html("<font color='green'>"+response.responseText+"</font>");
        var result = jQuery.parseJSON(response.responseText);
        jQuery.each(result,function(key,val){
            var object = jQuery(document.createElement("div")).attr({'class': 'imageWrap'});
            jQuery(object).append('<img src="'+val.image+'" />');
            jQuery(object).append('<span class="links"><a href="index.php?option=com_isproductlisting&task=product.makeproductimagedefault&imageid='+val.imageid+'&productid='+val.productid+'" ><?php echo JText::_('MAKE_IMAGE_DEFAULT');?></a>');
            jQuery(object).append('<span class="links"><a href="index.php?option=com_isproductlisting&task=product.removeproductimage&imageid='+val.imageid+'&productid='+val.productid+'" ><?php echo JText::_('REMOVE_IMAGE');?></a>');
            jQuery("#is_product_images").append(object);
        });
    },
    error: function()
    {
        jQuery("#message").html("<font color='red'> ERROR: unable to upload files</font>");
 
    }
 
}; 
     jQuery("form#adminForm").ajaxForm(options);
 
});
 
</script>

<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('FORM_PRODUCT_IMAGE'); ?></div>
            <form action="index.php" method="POST" name="adminForm" id="adminForm" enctype="multipart/form-data">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminform">
                    <?php if ($this->msg != '') { ?>
                        <tr>
                            <td colspan="2" align="center"><font color="red"><strong><?php echo JText::_($this->msg); ?></strong></font></td>
                        </tr>
                    <?php } ?>
                    <tr class="row<?php echo $k;$k = 1-$k;?>">
                        <td width="20%" valign="top"><label id="filemsg" for="file"><?php echo JText::_('IMAGES'); ?>&nbsp;<font color="red">*</font></label></td>
                        <td width="60%"><input class="inputbox required" type="file" multiple="multiple" id="image" name="image[]"  />
                        </td>
                    </tr>
                    <tr><td colspan="2" height="10"></td></tr>
                    <tr class="row<?php echo $k;$k = 1-$k;?>">
                        <td  colspan="2" align="center">
                            <input type="submit" class="button" name="submit_app"  value="<?php echo JText::_('SAVE'); ?>" />
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="productid" value="<?php echo $this->productid; ?>" />
                <input type="hidden" name="check" value="" />
                <input type="hidden" name="task" value="product.saveproductimages" />
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
            </form>
            <div id="progress">
                    <div id="bar"></div>
                    <div id="percent">0%</div >
            </div>
            <div id="message"></div>
            <div id="is_product_images">
                <?php 
                    if(!empty($this->productimages))
                        foreach($this->productimages AS $image){ ?>
                            <div class="imageWrap">
                                <img src="<?php echo JURI::root().$this->config['data_directory'];?>/products/product_<?php echo $this->productid;?>/thumbnail/medium-<?php echo $image->image;?>" />
                                <?php if($image->isdefault == 0){ ?>
                                <span class="links"><a href="index.php?option=com_isproductlisting&task=product.makeproductimagedefault&imageid=<?php echo $image->id;?>&productid=<?php echo $image->productid; ?>"><?php echo JText::_('MAKE_IMAGE_DEFAULT'); ?></a></span>
                                <?php }else{ ?>
                                <span class="links"><?php echo JText::_('DEFAULT_IMAGE');?></span>
                                <?php } ?>
                                <span class="links"><a href="index.php?option=com_isproductlisting&task=product.removeproductimage&imageid=<?php echo $image->id;?>&productid=<?php echo $image->productid; ?>"><?php echo JText::_('REMOVE_IMAGE'); ?></a></span>
                            </div>
                <?php } ?>
            </div>
    </div>
</div>