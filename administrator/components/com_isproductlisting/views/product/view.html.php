<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewProduct extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'formproduct') { // form product
            $cids = JRequest :: getVar('cid', array(0), '', 'array');
            $c_id = $cids[0];
            $result = $this->getITModel('product')->getProductForForm($c_id);
            $this->assignRef('productid', $c_id);
            $this->assignRef('product', $result[0]);
            $this->assignRef('lists', $result[1]);
            $this->assignRef('fieldordering', $result[2]);

            if (isset($result[0]->id))
                $isNew = false;
            $text = $isNew ? JText :: _('ADD') : JText :: _('EDIT');
            JToolBarHelper :: title(JText :: _('PRODUCT') . '<small><small> [' . $text . ']</small></small>', 'products');
            JToolBarHelper :: save('product.saveproduct', 'SAVE');
            if ($isNew)
                JToolBarHelper :: cancel('product.cancelproduct');
            else
                JToolBarHelper :: cancel('product.cancelproduct', 'Close');
            JHTML::_('behavior.formvalidation');
        }elseif ($layout == 'formproductimage') { // form product image
            $productid = JRequest::getVar('product');
            $result = ITModel::getITModel('productimage')->getProductImagesForForm($productid);
            $this->assignRef('productid', $productid);
            $this->assignRef('productimages', $result);

            JToolBarHelper :: title(JText :: _('PRODUCT_IMAGES'), 'products');
            JToolBarHelper :: save('product.saveproductimages', 'SAVE');
            JToolBarHelper :: cancel('product.cancelproduct');
            JHTML::_('behavior.formvalidation');
        } elseif ($layout == 'products') { //products
            JToolBarHelper :: title(JText :: _('PRODUCTS'), 'products');
            $searchtitle = JRequest::getVar('filter_title');
            $searchserialnumber = JRequest::getVar('filter_serialnumber');
            $searchcategoryid = $mainframe->getUserStateFromRequest($option . 'filter_categoryid', 'filter_categoryid', '', 'int');
            $searchsubcategoryid = $mainframe->getUserStateFromRequest($option . 'filter_subcategoryid', 'filter_subcategoryid', '', 'int');
            $searchbrandid = $mainframe->getUserStateFromRequest($option . 'filter_brandid', 'filter_brandid', '', 'int');
            $statusid = $mainframe->getUserStateFromRequest($option . 'filter_av_statusid', 'filter_av_statusid', '', 'int');
            $result = $this->getITModel('product')->getAllProduct($searchtitle, $searchserialnumber, $searchcategoryid, $searchsubcategoryid, $searchbrandid, $statusid, $limitstart, $limit);

            $total = $result[1];
            if ($total <= $limitstart)
                $limitstart = 0;
            $pagination = new JPagination($total, $limitstart, $limit);
            JToolBarHelper :: addNew('product.editproduct');
            JToolBarHelper :: editList('product.editproduct');
            JToolBarHelper :: deleteList('ARE_YOU_SURE_TO_DELETE', 'product.removeproduct');
            $this->assignRef('products', $result[0]);
            $this->assignRef('lists', $result[2]);
        }elseif ($layout == 'productqueue') { //products
            JToolBarHelper :: title(JText :: _('PRODUCTS_QUEUE'), 'productqueue');
            $searchtitle = JRequest::getVar('filter_title');
            $searchserialnumber = JRequest::getVar('filter_serialnumber');
            $searchcategoryid = $mainframe->getUserStateFromRequest($option . 'filter_categoryid', 'filter_categoryid', '', 'int');
            $searchsubcategoryid = $mainframe->getUserStateFromRequest($option . 'filter_subcategoryid', 'filter_subcategoryid', '', 'int');
            $searchbrandid = $mainframe->getUserStateFromRequest($option . 'filter_brandid', 'filter_brandid', '', 'int');
            $statusid = $mainframe->getUserStateFromRequest($option . 'filter_av_statusid', 'filter_av_statusid', '', 'int');
            $result = $this->getITModel('product')->getAllProduct($searchtitle, $searchserialnumber, $searchcategoryid, $searchsubcategoryid, $searchbrandid, $statusid, $limitstart, $limit);

            $total = $result[1];
            if ($total <= $limitstart)
                $limitstart = 0;
            $pagination = new JPagination($total, $limitstart, $limit);
            JToolBarHelper :: addNew('product.editproduct');
            JToolBarHelper :: editList('product.editproduct');
            JToolBarHelper :: deleteList('ARE_YOU_SURE_TO_DELETE', 'product.removeproduct');
            $this->assignRef('products', $result[0]);
            $this->assignRef('lists', $result[2]);
        }elseif ($layout == 'productsearch') { // Search product
            JToolBarHelper :: title(JText :: _('PRODUCT_SEARCH'), 'productsearch');
            $result = $this->getITModel('product')->getSearchOptions();
            $this->assignRef('lists', $result[2]);
            $this->assignRef('fieldorderings', $result[3]);
        } elseif ($layout == 'productsearch_results') { // product search results
            $mainframe = JFactory::getApplication();
            $data['title'] = $mainframe->getUserStateFromRequest($option . 'title', 'title');
            $data['categoryid'] = $mainframe->getUserStateFromRequest($option . 'categoryid', 'categoryid');
            $data['subcategoryid'] = $mainframe->getUserStateFromRequest($option . 'subcategoryid', 'subcategoryid');
            $data['brandid'] = $mainframe->getUserStateFromRequest($option . 'brandid', 'brandid');
            $data['price'] = $mainframe->getUserStateFromRequest($option . 'price', 'price');
            $data['serialnumber'] = $mainframe->getUserStateFromRequest($option . 'serialnumber', 'serialnumber');
            $data['howtouse'] = $mainframe->getUserStateFromRequest($option . 'howtouse', 'howtouse');
            $data['shortsummary'] = $mainframe->getUserStateFromRequest($option . 'shortsummary', 'shortsummary');
            $data['ingredients'] = $mainframe->getUserStateFromRequest($option . 'ingredients', 'ingredients');
            $data['productexpirydate'] = $mainframe->getUserStateFromRequest($option . 'productexpirydate', 'productexpirydate');
            $data['listproductexpirydate'] = $mainframe->getUserStateFromRequest($option . 'listproductexpirydate', 'listproductexpirydate');
            $data['isgold'] = $mainframe->getUserStateFromRequest($option . 'isgold', 'isgold');
            $data['isfeatured'] = $mainframe->getUserStateFromRequest($option . 'isfeatured', 'isfeatured');
            $data['status'] = $mainframe->getUserStateFromRequest($option . 'status', 'status');
            $data['created'] = $mainframe->getUserStateFromRequest($option . 'created', 'created');
            $result = $this->getITModel('product')->getProductSearchResults($data, $limitstart, $limit);
            $total = $result[1];
            $this->assignRef('products', $result[0]);
            if ($total <= $limitstart)
                $limitstart = 0;
            $pagination = new JPagination($total, $limitstart, $limit);
            JToolBarHelper :: title(JText :: _('PRODUCT_SEARCH_RESULTS'), 'productsearchresult');
            JToolBarHelper :: addNew('product.editproduct');
            JToolBarHelper :: editList('product.editproduct');
            JToolBarHelper :: deleteList('ARE_YOU_SURE_TO_DELETE', 'product.removeproduct');
        }
        $this->assignRef('pagination', $pagination);
        $this->assignRef('option', $option);
        $this->assignRef('config', $config);
        $this->assignRef('uid', $uid);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('msg', $msg);
        parent :: display($tpl);
    }

}

?>
