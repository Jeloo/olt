<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

global $mainframe;
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/zebra_accordion.css');
if(JVERSION < 3){
    $document->addScript('components/com_isproductlisting/include/js/jquery.js');
}else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');
}
$document->addScript('components/com_isproductlisting/include/js/zebra_accordion.js');
$layout = $this->getLayout();
switch($layout){
    case 'controlpanel':
    case 'brands':
    case 'formbrand':
    case 'categories':
    case 'formcategory':
    case 'subcategories':
    case 'formsubcategory':
    case 'currency':
    case 'formcurrency':
    case 'info':
        $number = 0;
        break;
    case 'configurations':
    case 'themes':
        $number = 1;
        break;
    case 'products':
    case 'productqueue':
    case 'productsearch':
    case 'formproductimage':
    case 'formproduct':
        $number = 2;
        break;
    case 'productfeedbacks':
    case 'feedbackapprovalqueue':
        $number = 4;
        break;
    case 'emailtemplate':
        $number = 5;
        break;
    default:
        $number = 0;
        break;
}
?>
<script type="text/javascript">
    jQuery(document).ready(function() {
      new jQuery.Zebra_Accordion(jQuery('.Zebra_Accordion'), {
    //'collapsible':  true,
    'show':<?php echo $number; ?>
  });
    });    
</script>

<div>
    <img src="components/com_isproductlisting/include/images/isproductlisting.png" width="175">
</div>
<dl class="Zebra_Accordion">
    <dt><?php echo JText::_('ADMIN'); ?></dt>
    <dd>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=controlpanel"><?php echo JText::_('CONTROL_PANEL'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=brand&view=brand&layout=brands"><?php echo JText::_('BRANDS'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=category&view=category&layout=categories"><?php echo JText::_('CATEGORIES'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=subcategory&view=subcategory&layout=subcategories"><?php echo JText::_('SUB_CATEGORIES'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=currency&view=currency&layout=currency"><?php echo JText::_('CURRENCY'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('UPDATE_ACTIVATE'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=info"><?php echo JText::_('INFORMATION'); ?></a>
    </dd>
    <dt><?php echo JText::_('CONFIGURATIONS'); ?></dt>
    <dd>
        <a href="index.php?option=com_isproductlisting&c=configuration&view=configuration&layout=configurations"><?php echo JText::_('CONFIGURATIONS'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=configuration&view=configuration&layout=themes"><?php echo JText::_('THEMES'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('PAYMENT_METHODS_CONFIGURATION'); ?></a>
    </dd>
    <dt><?php echo JText::_('PRODUCTS'); ?></dt>
    <dd>
        <a href="index.php?option=com_isproductlisting&c=product&view=product&layout=products"><?php echo JText::_('PRODUCTS'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=product&view=product&layout=productqueue"><?php echo JText::_('PRODUCT_APPROVAL_QUEUE'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=product&view=product&layout=productsearch"><?php echo JText::_('PRODUCT_SEARCH'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('GOLD_PRODUCTS'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('FEATURED_PRODUCTS'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('IMPORT_PRODUCT_DATA'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('EXPORT_PRODUCT_DATA'); ?></a>
    </dd>
    <dt><?php echo JText::_('PAYMENTS'); ?></dt>
    <dd>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('PAYMENT_HISTORY'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=isproductlisting&view=isproductlisting&layout=proversion"><?php echo JText::_('PAYMENT_REPORT'); ?></a>
    </dd>
    <dt><?php echo JText::_('PRODUCT_FEED_BACK'); ?></dt>
    <dd>
        <a href="index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=productfeedbacks"><?php echo JText::_('FEED_BACKS'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=feedback&view=feedback&layout=feedbackapprovalqueue"><?php echo JText::_('FEED_BACKS_APPROVAL_QUEUE'); ?></a>
    </dd>
    <dt><?php echo JText::_('EMAIL_TEMPLATES'); ?></dt>
    <dd>
        <a href="index.php?option=com_isproductlisting&c=emailtemplate&view=emailtemplate&layout=emailtemplate&tf=pd-prad"><?php echo JText::_('PURCHASE_REQUEST_ADMIN'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=emailtemplate&view=emailtemplate&layout=emailtemplate&tf=pd-vfad"><?php echo JText::_('PAYMENT_VERIFIED_ADMIN'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=emailtemplate&view=emailtemplate&layout=emailtemplate&tf=pd-prus"><?php echo JText::_('PURCHASE_REQUEST_USER'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=emailtemplate&view=emailtemplate&layout=emailtemplate&tf=pd-vfus"><?php echo JText::_('PAYMENT_VERIFIED_USER'); ?></a>
        <a href="index.php?option=com_isproductlisting&c=emailtemplate&view=emailtemplate&layout=emailtemplate&tf=tl-frnd"><?php echo JText::_('SEND_TO_FRIEND'); ?></a>
    </dd>
</dl>


