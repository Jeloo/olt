<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     IT Fant
  + Contact:        itfant.com , info@itfant.com
 * Created on:  January, 2014
  ^
  + Project:        IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
if(JVERSION < 3)
    $document->addScript('components/com_isproductlisting/include/js/jquery.js');
else{
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');    
}
$document->addScript('components/com_isproductlisting/include/js/colpick.js');
$document->addStyleSheet('components/com_isproductlisting/include/css/colpick.css');
?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>      
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('THEMES'); ?></div>
            <form action="index.php" method="POST" name="adminForm" id="adminForm">
                <div class="it_theme_field_wrapper">
                    <div class="it_theme_field_title">
                        <?php echo JText::_('COLOR1'); ?>
                        <br/>
                        <small><?php echo JText::_('TOP_PANEL_PAGINATION_PRODUCT_SHARE_PANEL_AND_POPUP_BOXES_BACKGROUND');?></small>
                    </div>
                    <div class="it_theme_field_value">
                        <input type="text" id="color1" name="color1" class="themebox" value="<?php echo $this->theme['color1']; ?>"  style="background:<?php echo $this->theme['color1']; ?>;" />
                    </div>                    
                </div>
                <div class="it_theme_field_wrapper">
                    <div class="it_theme_field_title">
                        <?php echo JText::_('COLOR2'); ?>
                        <br/>
                        <small><?php echo JText::_('BUTTONS_AND_HEADINGS_BACKGROUND_BORDER_COLOR_OF_BOXES');?></small>
                    </div>
                    <div class="it_theme_field_value">
                        <input type="text" id="color2" name="color2" class="themebox" value="<?php echo $this->theme['color2']; ?>" style="background:<?php echo $this->theme['color2']; ?>;" />
                    </div>                    
                </div>
                <div class="it_theme_field_wrapper">
                    <div class="it_theme_field_title">
                        <?php echo JText::_('COLOR3'); ?>
                        <br/>
                        <small><?php echo JText::_('BUTTONS_HOVER_BACKGROUND_PRICE_BOX_BORDER_AND_TEXT_COLOR');?></small>
                    </div>
                    <div class="it_theme_field_value">
                        <input type="text" id="color3" name="color3" class="themebox" value="<?php echo $this->theme['color3']; ?>"  style="background:<?php echo $this->theme['color3']; ?>;" />
                    </div>                    
                </div>
                <div class="it_theme_field_wrapper">
                    <div class="it_theme_field_title">
                        <?php echo JText::_('COLOR4'); ?>
                        <br/>
                        <small><?php echo JText::_('TEXT_COLOR_FOR_ALL_HEADINGS_AND_BUTTONS');?></small>
                    </div>
                    <div class="it_theme_field_value">
                        <input type="text" id="color4" name="color4" class="themebox" value="<?php echo $this->theme['color4']; ?>"  style="background:<?php echo $this->theme['color4']; ?>;" />
                    </div>                    
                </div>
                <div class="it_theme_field_wrapper">
                    <div class="it_theme_field_title">
                        <?php echo JText::_('COLOR5'); ?>
                        <br/>
                        <small><?php echo JText::_('ALL_TEXT_COLOR_EXPECT_HEADINGS_AND_BUTTONS');?></small>
                    </div>
                    <div class="it_theme_field_value">
                        <input type="text" id="color5" name="color5" class="themebox" value="<?php echo $this->theme['color5']; ?>"  style="background:<?php echo $this->theme['color5']; ?>;" />
                    </div>                    
                </div>
                <div class="it_theme_field_button_wrapper">
                    <input type="submit" name="submit" value="<?php echo JText::_('SAVE_THEME'); ?>" />
                </div>
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                <input type="hidden" name="task" value="configuration.savetheme" />
                <input type="hidden" name="boxchecked" value="0" />
            </form>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#color1').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                color:'<?php echo $this->theme['color1']; ?>',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                        jQuery(el).css('background','#'+hex);
                        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                        if(!bySetColor) jQuery(el).val('#'+hex);
                }
        }).keyup(function(){
                jQuery(this).colpickSetColor(this.value);
        });
        jQuery('#color2').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                color:'<?php echo $this->theme['color2']; ?>',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                        jQuery(el).css('background','#'+hex);
                        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                        if(!bySetColor) jQuery(el).val('#'+hex);
                }
        }).keyup(function(){
                jQuery(this).colpickSetColor(this.value);
        });
        jQuery('#color3').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                color:'<?php echo $this->theme['color3']; ?>',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                        jQuery(el).css('background','#'+hex);
                        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                        if(!bySetColor) jQuery(el).val('#'+hex);
                }
        }).keyup(function(){
                jQuery(this).colpickSetColor(this.value);
        });
        jQuery('#color4').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                color:'<?php echo $this->theme['color4']; ?>',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                        jQuery(el).css('background','#'+hex);
                        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                        if(!bySetColor) jQuery(el).val('#'+hex);
                }
        }).keyup(function(){
                jQuery(this).colpickSetColor(this.value);
        });
        jQuery('#color5').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                color:'<?php echo $this->theme['color5']; ?>',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                        jQuery(el).css('background','#'+hex);
                        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                        if(!bySetColor) jQuery(el).val('#'+hex);
                }
        }).keyup(function(){
                jQuery(this).colpickSetColor(this.value);
        });
    });
</script>