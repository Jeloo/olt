<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addScript('components/com_isproductlisting/include/js/jquery.js');
$document->addScript('components/com_isproductlisting/include/js/jquery_idTabs.js');
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');

$showhide = array(
    '0' => array('value' => 1, 'text' => JText::_('SHOW')),
    '1' => array('value' => 0, 'text' => JText::_('HIDE'))
);
$date_format = array(
    '0' => array('value' => 'd-m-Y', 'text' => JText::_('DD_MM_YYYY')),
    '1' => array('value' => 'm-d-Y', 'text' => JText::_('MM_DD_YYYY')),
    '2' => array('value' => 'Y-m-d', 'text' => JText::_('YYYY_MM_DD'))
);
$yesno = array(
    '0' => array('value' => 1, 'text' => JText::_('IS_YES')),
    '1' => array('value' => 0, 'text' => JText::_('IS_NO')),);
$reviewstatus = array(
    '0' => array('value' => 1, 'text' => JText::_('APPROVE')),
    '1' => array('value' => 0, 'text' => JText::_('UNAPPROVE'))
);
$pricenotation = array(
    '0' => array('value' => 'default', 'text' => JText::_('DEFAULT')),
    '1' => array('value' => 'french', 'text' => JText::_('FRENCH'))
);
$offline = JHTML::_('select.genericList', $yesno, 'offline', 'class="inputbox" ' . '', 'value', 'text', $this->configs['offline']);
$big_field_width = 40;
$med_field_width = 25;
$sml_field_width = 15;
?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('CONFIGURATIONS'); ?></div>
            <form action="index.php" method="POST" name="adminForm" id="adminForm" enctype="multipart/form-data" >
                <div id="tabs_wrapper" class="tabs_wrapper">
                    <div class="idTabs">
                        <span><a class="selected" href="#site_setting"><?php echo JText::_('SITE_SETTINGS'); ?></a></span> 
                        <span><a  href="#search_setting"><?php echo JText::_('PRODUCT_SEARCH_SETTINGS'); ?></a></span> 
                        <span><a  href="#sharing_setting"><?php echo JText::_('PRODUCT_SHARING_SETTINGS'); ?></a></span> 
                        <span><a  href="#filter_setting"><?php echo JText::_('FILTER'); ?></a></span> 
                        <span><a  href="#email_setting"><?php echo JText::_('EMAIL'); ?></a></span> 
                        <span><a  href="#links_setting"><?php echo JText::_('LINKS'); ?></a></span> 
                        <span><a  href="#rss_setting"><?php echo JText::_('RSS_SETTING'); ?></a></span> 
                        <span><a  href="#google_setting"><?php echo JText::_('GOOGLE_ADSENSE'); ?></a></span> 
                    </div>
                    <div id="site_setting">
                        <fieldset>
                            <legend><?php echo JText::_('SITE_SETTINGS'); ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" >
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('TITLE'); ?></td>
                                    <td  width="25%"><input type="text" name="title" value="<?php echo $this->configs['title']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /></td>
                                    <td  class="key"><?php echo JText::_('SHOW_TITLE'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'showtitle', 'class="inputbox" ' . '', 'value', 'text', $this->configs['showtitle']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('DATE_FORMAT'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $date_format, 'date_format', 'class="inputbox" ' . '', 'value', 'text', $this->configs['date_format']); ?></td>
                                    <td  class="key"><?php echo JText::_('SHOW_TOP_SECTION'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'topsection_show', 'class="inputbox" ' . '', 'value', 'text', $this->configs['topsection_show']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('SHOW_CURRENT_LOCATION'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'current_location', 'class="inputbox" ' . '', 'value', 'text', $this->configs['current_location']); ?></td>
                                    <td  class="key"><?php echo JText::_('SHOW_HEADING_BAR'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'headingbar_show', 'class="inputbox" ' . '', 'value', 'text', $this->configs['headingbar_show']); ?></td>
                                </tr>
                                <tr>
                                    <td class="key" ><?php echo JText::_('OFFLINE'); ?></td>
                                    <td ><?php echo $offline; ?></td>
                                    <td class="key" ><?php echo JText::_('OFFLINE_MESSAGE'); ?></td>
                                    <td><textarea name="offline_text" cols="25" rows="3" class="inputbox"><?php echo $this->configs['offline_text']; ?></textarea> </td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('PRICE_NOTATION'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $pricenotation, 'pricenotation', 'class="inputbox" ' . '', 'value', 'text', $this->configs['pricenotation']); ?></td>
                                    <td  class="key"><?php echo JText::_('PRICE_DECIMAL_POINT'); ?></td>
                                    <td ><input id="pricedecimalpoint" name="pricedecimalpoint" value="<?php echo $this->configs['pricedecimalpoint']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('PRODUCT_IMAGE_SMALL_WIDTH'); ?></td>
                                    <td ><input id="productimagesmallsizewidth" name="productimagesmallsizewidth" value="<?php echo $this->configs['productimagesmallsizewidth']; ?>" /></td>
                                    <td  class="key"><?php echo JText::_('PRODUCT_IMAGE_SMALL_HEIGHT'); ?></td>
                                    <td ><input id="productimagesmallsizeheight" name="productimagesmallsizeheight" value="<?php echo $this->configs['productimagesmallsizeheight']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('PRODUCT_IMAGE_LARGE_WIDTH'); ?></td>
                                    <td ><input id="productimagelargesizewidth" name="productimagelargesizewidth" value="<?php echo $this->configs['productimagelargesizewidth']; ?>" /></td>
                                    <td  class="key"><?php echo JText::_('PRODUCT_IMAGE_LARGE_HEIGHT'); ?></td>
                                    <td ><input id="productimagelargesizeheight" name="productimagelargesizeheight" value="<?php echo $this->configs['productimagelargesizeheight']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('DATA_DIRECTORY'); ?></td>
                                    <td ><input id="data_directory" name="data_directory" value="<?php echo $this->configs['data_directory']; ?>" /></td>
                                    <td  class="key"><?php echo JText::_('SHOW_BRAND_LOGO'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'showbrandlogo', 'class="inputbox" ' . '', 'value', 'text', $this->configs['showbrandlogo']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key"><?php echo JText::_('VISITOR_CAN_BUY'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'visitor_can_buy', 'class="inputbox" ' . '', 'value', 'text', $this->configs['visitor_can_buy']); ?></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="search_setting">
                        <fieldset>
                            <legend><?php echo JText::_('PRODUCT_SEARCH_SETTINGS'); ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" >
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('TITLE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'search_title', 'class="inputbox" ' . '', 'value', 'text', $this->configs['search_title']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'search_category', 'class="inputbox" ' . '', 'value', 'text', $this->configs['search_category']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('SUB_CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'search_subcategory', 'class="inputbox" ' . '', 'value', 'text', $this->configs['search_subcategory']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('BRAND'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'search_brand', 'class="inputbox" ' . '', 'value', 'text', $this->configs['search_brand']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('PRICE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'search_price', 'class="inputbox" ' . '', 'value', 'text', $this->configs['search_price']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('QUANTITY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'search_quantity', 'class="inputbox" ' . '', 'value', 'text', $this->configs['search_quantity']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('PRICE_RANGE_START'); ?></td>
                                    <td  width="25%"><input type="text" name="search_pricerangestart" id="search_pricerangestart" value="<?php echo $this->configs['search_pricerangestart']; ?>" /></td>
                                    <td  class="key" width="25%"><?php echo JText::_('PRICE_RANGE_END'); ?></td>
                                    <td  width="25%"><input type="text" name="search_pricerangeend" id="search_pricerangeend" value="<?php echo $this->configs['search_pricerangeend']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('PRICE_RANGE_GAP'); ?></td>
                                    <td  width="25%"><input type="text" name="search_pricegap" id="search_pricegap" value="<?php echo $this->configs['search_pricegap']; ?>" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="filter_setting">
                        <fieldset>
                            <legend><?php echo JText::_('FILTER_SETTING'); ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" >
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('FILTER'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'show_filter', 'class="inputbox" ' . '', 'value', 'text', $this->configs['show_filter']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('SHOW_TITLE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'show_title_filter', 'class="inputbox" ' . '', 'value', 'text', $this->configs['show_title_filter']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('SHOW_CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'show_category_filter', 'class="inputbox" ' . '', 'value', 'text', $this->configs['show_category_filter']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('SHOW_SUB_CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'show_subcategory_filter', 'class="inputbox" ' . '', 'value', 'text', $this->configs['show_subcategory_filter']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('SHOW_BRAND'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'show_brand_filter', 'class="inputbox" ' . '', 'value', 'text', $this->configs['show_brand_filter']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('SHOW_PRICE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'show_price_filter', 'class="inputbox" ' . '', 'value', 'text', $this->configs['show_price_filter']); ?></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="email_setting">
                        <fieldset>
                            <legend><?php echo JText::_('EMAIL_SETTINGS'); ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" >
                                <tr >
                                    <td class="key" width="25%"><?php echo JText::_('MAIL_FROM_ADDRESS'); ?></td>
                                    <td width="25%"><input type="text" name="mailfromaddress" value="<?php echo $this->configs['mailfromaddress']; ?>" class="inputbox" size="<?php echo $big_field_width; ?>"/> </td>
                                </tr>
                                <tr >
                                    <td class="key"><?php echo JText::_('MAIL_ADMIN_ADDRESS'); ?></td>
                                    <td><input type="text" name="adminemailaddress" value="<?php echo $this->configs['adminemailaddress']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" /> </td>
                                </tr>
                                <tr>
                                    <td class="key"><?php echo JText::_('MAIL_FROM_NAME'); ?></td>
                                    <td><input type="text" name="mailfromname" value="<?php echo $this->configs['mailfromname']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" /> </td>
                                </tr>
                                <tr>
                                    <td class="key"><?php echo JText::_('EMAIL_TO_ADMIN_NEW_PAYMENT_REQUEST_SEND'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'purchaserequestadmin', 'class="inputbox" ' . '', 'value', 'text', $this->configs['purchaserequestadmin']); ?></td>
                                </tr>
                                <tr>
                                    <td class="key"><?php echo JText::_('EMAIL_USER_NEW_PAYMENT_REQUEST_SEND'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'purchaserequestuser', 'class="inputbox" ' . '', 'value', 'text', $this->configs['purchaserequestuser']); ?></td>
                                </tr>
                                <tr>
                                    <td class="key"><?php echo JText::_('EMAIL_TO_ADMIN_PAYMENT_RECEVIED_VERIFIED'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'purchaseverifiedadmin', 'class="inputbox" ' . '', 'value', 'text', $this->configs['purchaseverifiedadmin']); ?></td>
                                </tr>
                                <tr>
                                    <td class="key"><?php echo JText::_('EMAIL_TO_USER_PAYMENT_RECEVIED_VERIFIED'); ?></td>
                                    <td ><?php echo JHTML::_('select.genericList', $yesno, 'purchaseverifieduser', 'class="inputbox" ' . '', 'value', 'text', $this->configs['purchaseverifieduser']); ?></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="links_setting">
                        <fieldset>
                            <legend><?php echo JText::_('TOP_LINKS') ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" >
                                <tr >
                                    <td class="key" width="25%"><?php echo JText::_('SHOW_TOP_LINKS'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_show', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_show']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('CONTROL_PANEL'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_controlpanel', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_controlpanel']); ?></td>
                                </tr>
                                <tr >
                                    <td class="key" width="25%"><?php echo JText::_('PRODUCTS'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_products', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_products']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('BRANDS'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_brands', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_brands']); ?></td>
                                </tr>
                                <tr >
                                    <td  width="25%" class="key"><?php echo JText::_('CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_categories', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_categories']); ?></td>
                                    <td class="key" width="25%"><?php echo JText::_('SUB_CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_subcategories', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_subcategories']); ?></td>
                                </tr>
                                <tr >
                                    <td  width="25%" class="key"><?php echo JText::_('SEARCH'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'toplink_search', 'class="inputbox" ' . '', 'value', 'text', $this->configs['toplink_search']); ?></td>
                                    <td  width="25%" class="key"></td>
                                    <td  width="25%"></td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend><?php echo JText::_('CONTROL_PANEL_LINKS') ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" >
                                <tr >
                                    <td class="key" width="25%"><?php echo JText::_('GOLD_PRODUCT'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_goldproduct', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_goldproduct']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('FEATURED_PRODUCT'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_featuredproduct', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_featuredproduct']); ?></td>
                                </tr>
                                <tr >
                                    <td class="key" width="25%"><?php echo JText::_('PRODUCT'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_products', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_products']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('BRAND'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_brands', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_brands']); ?></td>
                                </tr>
                                <tr >
                                    <td class="key" width="25%"><?php echo JText::_('CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_categories', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_categories']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('SUB_CATEGORY'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_subcategories', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_subcategories']); ?></td>
                                </tr>
                                <tr >
                                    <td  width="25%" class="key"><?php echo JText::_('SEARCH'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_search', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_search']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('SHORTLIST'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_shortlist', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_shortlist']); ?></td>
                                </tr>
                                <tr >
                                    <td  width="25%" class="key"><?php echo JText::_('COMAPRE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_compare', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_compare']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('CART'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'cp_cart', 'class="inputbox" ' . '', 'value', 'text', $this->configs['cp_cart']); ?></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="rss_setting">
                        <fieldset>
                            <legend><?php echo JText::_('RSS_SETTING'); ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" id="jobsrss">
                                <tr>
                                    <td  width="25%" class="key"><?php echo JText::_('SHOW_RSS_FEED'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'showrssfeeding', 'class="inputbox" ' . '', 'value', 'text', $this->configs['showrssfeeding']); ?></td>
                                    <td width="25%" class="key"><?php echo JText::_('TITLE'); ?></td>
                                    <td  ><input type="text" name="rss_title" value="<?php echo $this->configs['rss_title']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('MUST_PROVIDE_TITLE_FOR_PRODUCT_FEED'); ?></small></td>
                                </tr>
                                <tr>
                                    <td width="25%" class="key"><?php echo JText::_('DESCRIPTION'); ?></td>
                                    <td><textarea name="rss_description" cols="25" rows="3" class="inputbox"><?php echo $this->configs['rss_description']; ?></textarea><br clear="all">
                                        <small><?php echo JText::_('MUST_PROVIDE_DESCRIPTION_FOR_PRODUCT_FEED'); ?></small></td>
                                    <td width="25%" class="key"><?php echo JText::_('COPYRIGHT'); ?></td>
                                    <td  ><input type="text" name="rss_copyright" value="<?php echo $this->configs['rss_copyright']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('LEAVE_BLANK_IF_NOT_SHOW'); ?></small></td>
                                </tr>
                                <tr>
                                    <td width="25%" class="key"><?php echo JText::_('WEBMASTER'); ?></td>
                                    <td><input type="text" name="rss_webmaster" value="<?php echo $this->configs['rss_webmaster']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('LEAVE_BLANK_IF_NOT_SHOW_WEBMASTER_USED_FOR_TECHNICAL_ISSUE'); ?></small></td>
                                    <td width="25%" class="key"><?php echo JText::_('EDITOR'); ?></td>
                                    <td  ><input type="text" name="rss_editor" value="<?php echo $this->configs['rss_editor']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('LEAVE_BLANK_IF_NOT_SHOW_EDITOR_USED_FOR_FEED_CONTENT_ISSUE'); ?></small></td>
                                </tr>
                                <tr>
                                    <td width="25%" class="key"><?php echo JText::_('TIME_TO_LIVE'); ?></td>
                                    <td  ><input type="text" name="rss_ttl" value="<?php echo $this->configs['rss_ttl']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('TIME_TO_LIVE_FOR_PRODUCT_FEED'); ?></small></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="google_setting">
                        <fieldset>
                            <legend><?php echo JText::_('GOOGLE_ADSENSE') ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" id="jobsrss">
                                <tr>
                                    <td  width="25%" class="key"><?php echo JText::_('SHOW_GOOGLE_ADSENSE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'showgoogleadsense', 'class="inputbox" ' . '', 'value', 'text', $this->configs['showgoogleadsense']); ?></td>
                                    <td width="25%" class="key" ><?php echo JText::_('CUSTOM_CSS'); ?></td>
                                    <td><textarea  name="custom_css" cols="25" rows="3" class="inputbox"><?php echo $this->configs['custom_css']; ?></textarea> </td>
                                </tr>
                                <tr>
                                    <td width="25%" class="key"><?php echo JText::_('ADSENSE_REPEAT'); ?></td>
                                    <td  ><input type="text" name="ad_on_repitation" value="<?php echo $this->configs['ad_on_repitation']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('SHOW_ADSENSE_AFTER_NUMBER_OF_PRODUCT'); ?></small></td>
                                    <td width="25%" class="key" ><?php echo JText::_('CLIENT'); ?></td>
                                    <td  ><input type="text" name="add_on_client" value="<?php echo $this->configs['add_on_client']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                </tr>
                                <tr>
                                    <td width="25%" class="key"><?php echo JText::_('SLOT'); ?></td>
                                    <td  ><input type="text" name="add_on_slot" value="<?php echo $this->configs['add_on_slot']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                    <td width="25%" class="key"><?php echo JText::_('FORMAT'); ?></td>
                                    <td  ><input type="text" name="add_on_format" value="<?php echo $this->configs['add_on_format']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /><br clear="all">
                                        <small><?php echo JText::_('EX. 728 * 90'); ?></small></td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend><?php echo JText::_('GOOGLE_ADSENSE_SHOW_HIDE') ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" id="jobsrss">
                                <tr>
                                    <td  width="25%" class="key"><?php echo JText::_('PRODUCT_LISTING'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'productlistshowadsense', 'class="inputbox" ' . '', 'value', 'text', $this->configs['productlistshowadsense']); ?></td>
                                    <td  width="25%" class="key"><?php echo JText::_('PRODUCT_DETAIL'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'viewproductshowadsense', 'class="inputbox" ' . '', 'value', 'text', $this->configs['viewproductshowadsense']); ?></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="sharing_setting">
                        <fieldset>
                            <legend><?php echo JText::_('PRODUCT_SHARING_SETTINGS') ?></legend>
                            <table cellpadding="5" cellspacing="1" border="0" width="100%" class="admintable" id="jobsrss">
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('FACEBOOK'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_fb_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_fb_share']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('FACEBOOK_COMMENT'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_fb_comments', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_fb_comments']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('GOOGLE_SHARE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_google_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_google_share']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('TWITTER'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_twiiter_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_twiiter_share']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('FRIEND_FEED'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_friendfeed_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_friendfeed_share']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('BLOG'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_blog_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_blog_share']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('LINKEDIN'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_linkedin_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_linkedin_share']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('MY_SPACE'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_myspace_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_myspace_share']); ?></td>
                                </tr>
                                <tr>
                                    <td  class="key" width="25%"><?php echo JText::_('YAHOO'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_yahoo_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_yahoo_share']); ?></td>
                                    <td  class="key" width="25%"><?php echo JText::_('DIGG'); ?></td>
                                    <td  width="25%"><?php echo JHTML::_('select.genericList', $showhide, 'product_digg_share', 'class="inputbox" ' . '', 'value', 'text', $this->configs['product_digg_share']); ?></td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <input type="hidden" name="task" value="configuration.saveconf" />
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
            </form>
    </div>
</div>