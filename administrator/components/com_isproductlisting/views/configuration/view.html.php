<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewConfiguration extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'configurations') {
            JToolBarHelper :: title(JText::_('CONFIGURATIONS'), 'configurations');
            JToolBarHelper :: save('configuration.saveconf');
            $result = $this->getITModel('configuration')->getConfigurationsForForm();
            $this->assignRef('configs', $result);
        } elseif ($layout == 'themes') {
            $theme = $this->getITModel('configuration')->getThemeCofiguration();
            JToolBarHelper :: title(JText::_('THEMES'), 'themes');
            JToolBarHelper :: cancel();
            $this->assignRef('theme',$theme);
        }

        $this->assignRef('pagination', $pagination);
        $this->assignRef('option', $option);
        $this->assignRef('config', $config);
        $this->assignRef('uid', $uid);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('msg', $msg);
        parent :: display($tpl);
    }

}

?>
