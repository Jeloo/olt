<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class ISProductlistingViewCurrency extends ITView {

    function display($tpl = null) {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/views/common.php';
        $viewtype = 'html';
        if ($layout == 'currency') { // Currency
            $searchtitle = $mainframe->getUserStateFromRequest($option . 'filter_title', 'filter_title', '', 'string');
            $statusid = $mainframe->getUserStateFromRequest($option . 'filter_status', 'filter_status', '', 'int');
            $result = $this->getITModel('currency')->getAllCurrency($searchtitle, $statusid, $limit, $limitstart);
            $total = $result[1];
            if ($total <= $limitstart) $limitstart = 0;
            $pagination = new JPagination($total, $limitstart, $limit);
            $this->assignRef('currency', $result[0]);
            $this->assignRef('pagination', $pagination);
            $this->assignRef('lists', $result[2]);

            JToolBarHelper :: title(JText :: _('CURRENCY'), 'currency');
            JToolBarHelper :: addNew('currency.editcurrency');
            JToolBarHelper :: editList('currency.editcurrency');
            JToolBarHelper :: deleteList(JText::_('ARE_YOU_SURE_TO_DELETE'), 'currency.removecurrency');
            JHTML::_('behavior.formvalidation');
        }elseif ($layout == 'formcurrency') { // form currency
            $cids = JRequest :: getVar('cid', array(0), '', 'array');
            $c_id = $cids[0];
            $result = $this->getITModel('currency')->getCurrencyForForm($c_id);
            $this->assignRef('currencyid', $c_id);
            $this->assignRef('currency', $result[0]);
            $this->assignRef('lists', $result[1]);

            if (isset($result[0]->id)) $isNew = false;
            $text = $isNew ? JText :: _('ADD') : JText :: _('EDIT');
            JToolBarHelper :: title(JText :: _('CURRENCY') . '<small><small> [' . $text . ']</small></small>', 'currency');
            JToolBarHelper :: save('currency.savecurrency', 'SAVE');
            if ($isNew) JToolBarHelper :: cancel('currency.cancelcurrency'); 
            else JToolBarHelper :: cancel('currency.cancelcurrency', 'Close');
            JHTML::_('behavior.formvalidation');
        }

        $this->assignRef('pagination', $pagination);
        $this->assignRef('option', $option);
        $this->assignRef('config', $config);
        $this->assignRef('uid', $uid);
        $this->assignRef('items', $items);
        $this->assignRef('viewtype', $viewtype);
        $this->assignRef('msg', $msg);
        parent :: display($tpl);
    }

}

?>
