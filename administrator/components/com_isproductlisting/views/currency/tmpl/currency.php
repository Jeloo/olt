<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document =  JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
?>
<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('CURRENCY'); ?></div>
        <form action="index.php" method="post" name="adminForm" id="adminForm">
            <table class="adminlist" border="0">
                <thead>
                    <tr>
                        <th width="20">
                            <?php if(JVERSION < 3){ ?>
                            <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->currency); ?>);" />
                            <?php }else{ ?>
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                            <?php } ?>
                        </th>
                        <th width="60%" class="title"><?php echo JText::_('TITLE'); ?></th>
                        <th align="left"><?php echo JText::_('SYMBOL'); ?></th>
                        <th align="left"><?php echo JText::_('CODE'); ?></th>
                        <th><?php echo JText::_('DEFAULT'); ?></th>
                        <th><?php echo JText::_('PUBLISHED'); ?></th>
                    </tr>
                </thead>
                <?php
                jimport('joomla.filter.output');
                $k = 0;
                for ($i = 0, $n = count($this->currency); $i < $n; $i++) {
                    $row = $this->currency[$i];
                    $checked = JHTML::_('grid.id', $i, $row->id);
                    $link = JFilterOutput::ampReplace('index.php?option=' . $this->option . '&task=currency.editcurrency&cid[]=' . $row->id);
                    ?>
                    <tr valign="top" class="<?php echo "row$k"; ?>">
                        <td>
                            <?php echo $checked; ?>
                        </td>
                        <td>
                            <a href="<?php echo $link; ?>">
                                <?php echo $row->title; ?></a>
                        </td>
                        <td><?php echo $row->symbol; ?></td>
                        <td><?php echo $row->code; ?></td>
                        <td align="center">
                            <?php if ($row->isdefault == 1) { ?>
                                    <img src="../components/com_isproductlisting/images/default.png" width="16" height="16" border="0" alt="<?php echo JText::_('DEFAULT'); ?>" />
                            <?php } else { ?>
                                <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','currency.makedefaultcurrency')">
                                    <img src="../components/com_isproductlisting/images/notdefault.png" width="16" height="16" border="0" alt="<?php echo JText::_('NOT_DEFAULT'); ?>" /></a>
                            <?php } ?>
                        </td>
                        <td align="center">
                            <?php if ($row->status == 1) { ?>
                                <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','currency.unpublishcurrency')">
                                    <img src="../components/com_isproductlisting/images/tick.png" width="16" height="16" border="0" alt="<?php echo JText::_('PUBLISH'); ?>" /></a>
                            <?php } else { ?>
                                <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','currency.publishcurrency')">
                                    <img src="../components/com_isproductlisting/images/publish_x.png" width="16" height="16" border="0" alt="<?php echo JText::_('UNPUBLISH'); ?>" /></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $k = 1 - $k;
                }
                ?>
                <tr>
                    <td colspan="9">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
            <input type="hidden" name="c" value="currency" />
            <input type="hidden" name="view" value="currency" />
            <input type="hidden" name="layout" value="currency" />
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="boxchecked" value="0" />
        </form>
    </div>
</div>