<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_isproductlisting/include/css/admin.css');
JHTMLBehavior::formvalidation();
$k = 0;
?>

<script type="text/javascript">
    // for joomla 1.6
    Joomla.submitbutton = function(task){
        if (task == ''){
            return false;
        }else{
            if (task == 'currency.savecurrency'){
                returnvalue = validate_form(document.adminForm);
            }else returnvalue  = true;
            if (returnvalue){
                Joomla.submitform(task);
                return true;
            }else return false;
        }
    }
    function validate_form(f){
        if (document.formvalidator.isValid(f)) {
            f.check.value='<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
        }
        else {
            alert('<?php echo JText::_('SOME_VALUES_ARE_NOT_ACCEPTABLE_PLEASE_RETRY');?>');
            return false;
        }
        return true;
    }
</script>

<div id="product_listing_wrapper">
    <div class="product_listing_menu">
            <?php include_once('components/com_isproductlisting/views/menu.php'); ?>
    </div>		
    <div class="product_listing_data">
        <div class="product_listing_heading"><?php echo JText::_('FORM_CURRENCY'); ?></div>
        <form action="index.php" method="POST" name="adminForm" id="adminForm" >
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminform">
                <?php
                if ($this->msg != '') {
                    ?>
                    <tr>
                        <td colspan="2" align="center"><font color="red"><strong><?php echo JText::_($this->msg); ?></strong></font></td>
                    </tr>
                    <tr><td colspan="2" height="10"></td></tr>	
                    <?php
                }
                ?>
                <tr class="row1">
                    <td width="20%" valign="top"><label id="namemsg" for="name"><?php echo JText::_('TITLE'); ?>&nbsp;<font color="red">*</font></label></td>
                    <td width="60%"><input class="inputbox required" type="text" id="name" name="title" size="40" maxlength="255" value="<?php if (isset($this->currency)) echo $this->currency->title; ?>" />
                    </td>
                </tr>
                <tr class="row0">
                    <td width="20%" valign="top"><label id="symbolmsg" for="symbol"><?php echo JText::_('SYMBOL'); ?>&nbsp;<font color="red">*</font></label></td>
                    <td width="60%"><input class="inputbox required" type="text" id="symbol" name="symbol" size="40" maxlength="255" value="<?php if (isset($this->currency)) echo $this->currency->symbol; ?>" />
                    </td>
                </tr>
                <tr class="row1">
                    <td width="20%" valign="top"><label id="codemsg" for="code"><?php echo JText::_('CODE'); ?>&nbsp;<font color="red">*</font></label></td>
                    <td width="60%"><input class="inputbox required" type="text" id="code" name="code" size="40" maxlength="255" value="<?php if (isset($this->currency)) echo $this->currency->code; ?>" />
                    </td>
                </tr>
                <tr>
                <tr class="row0">
                    <td valign="top"><label id="statusmsg" for="status"><?php echo JText::_('PUBLISHED'); ?></label></td>
                    <td><input type="checkbox" name="status" id="status" value="1" <?php if (isset($this->currency)) {
                    if ($this->currency->status == '1') echo 'checked';
                } ?>/>
                    </td>
                </tr>
                <tr><td colspan="2" height="10"></td></tr>
                <tr class="row1">
                    <td  colspan="2" align="center">
                        <input type="submit" class="button" name="submit_app" onclick="return validate_form(document.adminForm)" value="<?php echo JText::_('SAVE'); ?>" />
                    </td>
                </tr>
            </table>
            <input type="hidden" name="id" value="<?php if (isset($this->currency)) echo $this->currency->id; ?>" />
            <input type="hidden" name="check" value="" />
            <input type="hidden" name="task" value="currency.savecurrency" />
            <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
        </form>
    </div>
</div>