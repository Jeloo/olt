<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelCurrency extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function getAllCurrency($searchtitle, $statusid, $limit, $limitstart) {  // get all currency
        $db = $this->getDBO();
        $result = array();
        $status [] = array('value' => '', 'text' => JText::_('STATUS'));
        $status [] = array('value' => 1, 'text' => JText::_('APPROVED'));
        $status [] = array('value' => -1, 'text' => JText::_('REJECTED'));

        $lists['title'] = $searchtitle;
        $lists['status'] = JHTML::_('select.genericList', $status, 'filter_statusid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $statusid);

        $wherequery = "";
        if ($searchtitle)
            $wherequery .= " AND currency.title LIKE '%" . str_replace("'", "", $db->quote($searchtitle)) . "%'";
        if ($statusid)
            $wherequery .=" AND currency.status = " . $statusid;

        $query = "SELECT COUNT(currency.id) FROM #__isproductlisting_currency AS currency WHERE currency.status = currency.status";
        $query .= $wherequery;

        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total <= $limitstart)
            $limitstart = 0;
        $query = "SELECT currency.* FROM `#__isproductlisting_currency` AS currency WHERE currency.status = currency.status";
        $query .= $wherequery;
        $query .=" ORDER BY currency.title DESC ";
        $db->setQuery($query, $limitstart, $limit);
        $result = $db->loadObjectList();
        $result[0] = $result;
        $result[1] = $total;
        $result[2] = $lists;
        return $result;
    }

    function changeStatusCurrency($cid, $status) { //changeStatus currency
        if (is_array($cid) == false) return false;
        if (is_numeric($status) == false) return false;
        foreach ($cid AS $id) {
            $canchangestatus = ITModel::getITModel('common')->canPublishUnpublish($id, 'currency');
            if ($canchangestatus) {
                $row = $this->getTable('currency');
                $row->id = $id;
                $row->status = $status;
                if (!$row->store()) {
                    $this->setError($this->_db->getErrorMsg());
                    $returnvalue = 0;
                } else $returnvalue = 1;
            } else $returnvalue = 3;
        }
        return $returnvalue;
    }

    function makeDefaultCurrency($id) {
        if (!is_numeric($id)) return false;
        $canmakedefault = ITModel::getITModel('common')->canMakeDefault($id, 'currency');
        if ($canmakedefault) {
            $db = $this->getDBO();
            $query = "UPDATE `#__isproductlisting_currency` AS currency SET currency.isdefault= 0";
            $db->setQuery($query);
            if (!$db->query()) {
                $returnvalue = 0;
            }
            $row = $this->getTable('currency');
            $row->id = $id;
            $row->isdefault = 1;
            $row->status = 1;
            if (!$row->store()) {
                $this->setError($this->_db->getErrorMsg());
                echo $this->_db->getErrorMsg();
                $returnvalue = 0;
            } else $returnvalue = 1;
        } else $returnvalue = 4;
        return $returnvalue;
    }

    function getCurrencyForForm($id) {// Currency Form Data
        $currencies = null;
        if ($id) {
            if (!is_numeric($id))
                return false;
            $db = $this->getDBO();
            $query = "SELECT currency.* FROM `#__isproductlisting_currency` AS currency WHERE currency.id = " . $id;
            $db->setQuery($query);
            $currencies = $db->loadObject();
        }

        $result[0] = $currencies;
        return $result;
    }

    function storeCurrency() {//store Currency
        $row = $this->getTable('currency');
        $result = array();
        $data = JRequest :: get('post');
        if ($data['id'] == '') { // only for new
            $result1 = $this->isCurrencyExist($data['title']);
            if ($result1 == 1) {
                $result[0] = 3;
                return $result;
            }
            $data['created'] = date("Y-m-d H:i:s");
        }
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }

        $result[0] = true;
        $result[1] = $row->id;
        return $result;
    }

    function isCurrencyExist($title) { //is currency exist
        $db = JFactory::getDBO();
        $query = "SELECT COUNT(currency.id) FROM #__isproductlisting_currency AS currency WHERE currency.title = " . $db->Quote($title);
        $db->setQuery($query);
        $result = $db->loadResult();
        if ($result == 0)
            return 0;
        else
            return 1;
    }

    function deleteCurrency() { //delete currency
        $cids = JRequest :: getVar('cid', array(0), 'post', 'array');
        $row = $this->getTable('currency');
        $deleteall = 1;
        foreach ($cids as $cid) {
            if ($this->currencyCanDelete($cid) == true) {
                if (!$row->delete($cid)) {
                    $this->setError($row->getErrorMsg());
                    return false;
                }
            } else
                $deleteall++;
        }
        return $deleteall;
    }

    function currencyCanDelete($id) { //currency CanDelete
        if (!is_numeric($id))
            return false;
        $db = $this->getDBO();
        $query = "SELECT COUNT(currency.id) FROM `#__isproductlisting_currency` AS currency WHERE currency.isdefault = 1 AND currency.id = " . $id;
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total > 0)
            return false;
        else
            return true;
    }

}
