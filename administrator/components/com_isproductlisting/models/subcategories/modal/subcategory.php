<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
 + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
 ^
 + Project: 		IS Product listing
 ^
 */
defined('JPATH_BASE') or die;

/**
 * Supports a modal article picker.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since		1.6
 */
class JFormFieldModal_Subcategory extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Modal_Subcategory';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		$db	= JFactory::getDBO();
		$query = "SELECT id,title FROM `#__isproductlisting_subcategories` WHERE status = 1 ORDER BY title";
		$db->setQuery($query);
		$subcategories = $db->loadObjectList();

		if ($error = $db->getErrorMsg()) {
			JError::raiseWarning(500, $error);
		}
		// The current user display field.
		$html[] = '<div class="fltlft">';
		$html[] = '  <select name="'.$this->name.'" id="'.$this->name.'" data-id="subcategory">';
		$html[] = '  <option value="">'.JText::_('SELECT_SUBCATEGORY').'</option>';
                foreach($subcategories AS $subcat){
                    if($this->value == $subcat->id){
                        $html[] = '  <option value="'.$subcat->id.'" selected="selected">'.$subcat->title.'</option>';
                    }else{
                        $html[] = '  <option value="'.$subcat->id.'">'.$subcat->title.'</option>';
                    }
                        
                }
		$html[] = '  </select>';
		$html[] = '</div>';
                /*
		// The user select button.
		$html[] = '<div class="button2-left">';
		$html[] = '  <div class="blank">';
		$html[] = '	<a class="modal" title="'.JText::_('COM_CONTENT_CHANGE_ARTICLE').'"  href="'.$link.'&amp;'.JSession::getFormToken().'=1" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('COM_CONTENT_CHANGE_ARTICLE_BUTTON').'</a>';
		$html[] = '  </div>';
		$html[] = '</div>';
                */
		// The active article id field.
		if (0 == (int)$this->value) {
			$value = '';
		} else {
			$value = (int)$this->value;
		}

		// class='required' for client side validation
		$class = '';
		if ($this->required) {
			$class = ' class="required"';
		}

		//$html[] = '<input type="hidden" id="'.$this->id.'_id"'.$class.' name="'.$this->name.'" value="'.$value.'" />';

		return implode("\n", $html);
	}
}
