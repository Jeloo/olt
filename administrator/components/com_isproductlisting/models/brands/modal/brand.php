<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
defined('JPATH_BASE') or die;

/**
 * Supports a modal article picker.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since		1.6
 */
class JFormFieldModal_Brand extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Modal_Brand';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		/*
                // Load the modal behavior script.
		JHtml::_('behavior.modal', 'a.modal');
                
		// Build the script.
		$script = array();
		$script[] = '	function jSelectArticle_'.$this->id.'(id, title, catid, object) {';
		$script[] = '		document.id("'.$this->id.'_id").value = id;';
		$script[] = '		document.id("'.$this->id.'_name").value = title;';
		$script[] = '		SqueezeBox.close();';
		$script[] = '	}';

		// Add the script to the document head.
		JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));


		// Setup variables for display.
		$html	= array();
		$link	= 'index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_'.$this->id;
                */
		$db	= JFactory::getDBO();
                $query = "SELECT id,title FROM `#__isproductlisting_brands` WHERE status = 1 ORDER BY title";
		$db->setQuery($query);
		$brands = $db->loadObjectList();

		if ($error = $db->getErrorMsg()) {
			JError::raiseWarning(500, $error);
		}
                /*
		if (empty($title)) {
			$title = JText::_('SELECT_A_BRAND');
		}
		$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
                */
		// The current user display field.
		$html[] = '<div class="fltlft">';
		//$html[] = '  <input type="text" id="'.$this->id.'_name" value="'.$title.'" disabled="disabled" size="35" />';
		$html[] = '  <select name="'.$this->name.'" id="'.$this->name.'">';
		$html[] = '  <option value="">'.JText::_('SELECT_BRAND').'</option>';
                foreach($brands AS $brand){
                    if($this->value == $brand->id){
                        $html[] = '  <option value="'.$brand->id.'" selected="selected">'.$brand->title.'</option>';
                    }else{
                        $html[] = '  <option value="'.$brand->id.'">'.$brand->title.'</option>';
                    }
                        
                }
		$html[] = '  </select>';
		$html[] = '</div>';
                /*
		// The user select button.
		$html[] = '<div class="button2-left">';
		$html[] = '  <div class="blank">';
		$html[] = '	<a class="modal" title="'.JText::_('COM_CONTENT_CHANGE_ARTICLE').'"  href="'.$link.'&amp;'.JSession::getFormToken().'=1" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('COM_CONTENT_CHANGE_ARTICLE_BUTTON').'</a>';
		$html[] = '  </div>';
		$html[] = '</div>';
                */
		// The active article id field.
		if (0 == (int)$this->value) {
			$value = '';
		} else {
			$value = (int)$this->value;
		}

		// class='required' for client side validation
		$class = '';
		if ($this->required) {
			$class = ' class="required"';
		}

		//$html[] = '<input type="hidden" id="'.$this->id.'_id"'.$class.' name="'.$this->name.'" value="'.$value.'" />';

		return implode("\n", $html);
	}
}
