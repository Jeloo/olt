<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelEmailtemplate extends ITModel {

    function __construct() {
        parent :: __construct();
    }
    function getTemplate($tempfor) {
        $db = JFactory :: getDBO();
        switch ($tempfor) {
            case 'pd-prad' : $tempatefor = 'purchase-requestadmin'; break;
            case 'pd-vfad' : $tempatefor = 'purchase-verifiedadmin'; break;
            case 'pd-prus' : $tempatefor = 'purchase-requestuser'; break;
            case 'pd-vfus' : $tempatefor = 'purchase-verifieduser'; break;
            case 'tl-frnd' : $tempatefor = 'tell-friend'; break;
        }
        $query = "SELECT * FROM `#__isproductlisting_emailtemplates` WHERE templatefor = " . $db->quote($tempatefor);
        //echo $query;
        $db->setQuery($query);
        $template = $db->loadObject();
        return $template;
    }

    function storeEmailTemplate() {
        $row = $this->getTable('emailtemplate');

        $data = JRequest :: get('post');
        $data['body'] = JRequest::getVar('body', '', 'post', 'string', JREQUEST_ALLOWRAW);

        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        return true;
    }

}
