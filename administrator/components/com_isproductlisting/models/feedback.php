<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelFeedback extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function getAllFeedBacks($limitstart, $limit) { //get all FeedBack products
        $db = JFactory :: getDBO();
        $result = array();
        $query = "SELECT COUNT(product.id)
            FROM #__isproductlisting_product_feedback As feedback
            JOIN #__isproductlisting_products as product ON feedback.productid = product.id 
            WHERE feedback.status <> 0 ";
        $db->setQuery($query);
        $total = $db->loadResult();

        if ($total <= $limitstart)
            $limitstart = 0;
        $query = "SELECT feedback.*,product.title  
            FROM #__isproductlisting_product_feedback as feedback
            JOIN #__isproductlisting_products as product ON product.id = feedback.productid
            WHERE feedback.status <> 0 ORDER BY feedback.id DESC";
        //echo $query;
        $db->setQuery($query, $limitstart, $limit);
        $result2 = $db->loadObjectList();

        $result[0] = $result2;
        $result[1] = $total;
        return $result;
    }

    function getAllFeedBacksApproval($limitstart, $limit) { //get all FeedBack products
        $db = JFactory :: getDBO();
        $result = array();
        $query = "SELECT COUNT(product.id)
            FROM #__isproductlisting_product_feedback As feedback
            JOIN #__isproductlisting_products as product ON feedback.productid = product.id 
            WHERE feedback.status = 0 ";
        $db->setQuery($query);
        $total = $db->loadResult();

        if ($total <= $limitstart)
            $limitstart = 0;
        $query = "SELECT feedback.*,product.title  
            FROM #__isproductlisting_product_feedback as feedback
            JOIN #__isproductlisting_products as product ON product.id = feedback.productid
            WHERE feedback.status = 0 ORDER BY feedback.id DESC";
        //echo $query;
        $db->setQuery($query, $limitstart, $limit);
        $result2 = $db->loadObjectList();

        $result[0] = $result2;
        $result[1] = $total;
        return $result;
    }

    function getFeedbackById($id) {
        if (!is_numeric($id))
            return false;
        $db = $this->getDBO();
        $query = "SELECT * FROM `#__isproductlisting_product_feedback` WHERE id = " . $id;
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    function storeProductFeedBack() {
        $data = JRequest :: get('post');

        $row = $this->getTable('feedback');
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return 2;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            echo $this->_db->getErrorMsg();
            return false;
        }
        return TRUE;
    }

    function deleteProductFeedBack($id) { //delete product review
        if (!is_numeric($id))
            return false;
        $row = $this->getTable('feedback');
        if (!$row->delete($id)) {
            $this->setError($row->getErrorMsg());
            return false;
        }
        return true;
    }

    function feedbackPublished($field_id, $value) {
        if (is_array($field_id) == false)
            return false;
        $db = JFactory::getDBO();
        foreach ($field_id AS $id) {
            $query = " UPDATE #__isproductlisting_product_feedback SET status = " . $value . " WHERE id = " . $field_id;
            $db->setQuery($query);
            if (!$db->query()) {
                return false;
            }
        }
        return true;
    }

}
