<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelProductimage extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function getProductImagesForForm($productid) {
        if (!is_numeric($productid))
            return false;
        $db = JFactory::getDbo();
        $query = "SELECT * FROM `#__isproductlisting_product_images` WHERE productid = " . $productid . " ORDER BY isdefault DESC";
        $db->setQuery($query);
        $images = $db->loadObjectList();
        return $images;
    }

    function storeProductImage($productid) {
        $files = array();
        foreach ($_FILES['image'] as $k => $l) {
            foreach ($l as $i => $v) {
                if (!array_key_exists($i, $files))
                    $files[$i] = array();
                $files[$i][$k] = $v;
            }
        }
        foreach($files AS $file) {
            $data['productid'] = $productid;
            $data['image'] = $file['name'];
            $data['status'] = 1;
            $data['created'] = date('Y-m-d H:i:s');
            $row = $this->getTable('productimages');
            if (!$row->bind($data)) {
                $this->setError($this->_db->getErrorMsg());
                $result = false;
                return $result;
            }
            if (!$row->check()) {
                $this->setError($this->_db->getErrorMsg());
                $result = false;
                return $result;
            }
            if (!$row->store()) {
                $this->setError($this->_db->getErrorMsg());
                $result = false;
                return $result;
            }
            $imageid = $row->id;
            $imageresult = $this->uploadProductImage($productid, $imageid, $file);
            $row->image = $imageresult;
            if (!$row->store()) {
                $this->setError($this->_db->getErrorMsg());
                $result = false;
                return $result;
            }
            $configs = $this->getITModel('configuration')->getConfiginArray('default');
            $imageurl[$i]['image'] = JURI::root() . $configs['data_directory'] . "/products/product_" . $productid . "/thumbnail/medium-" . $imageresult;
            $imageurl[$i]['imageid'] = $imageid;
            $imageurl[$i]['productid'] = $productid;
            $this->makeProductImageDefault($imageid, $productid);
        }
        return json_encode($imageurl);
    }

    function uploadProductImage($productid, $imageid, $file) {
        $configs = $this->getITModel('configuration')->getConfiginArray('default');
        $datadirectory = $configs['data_directory'];
        $str = JPATH_BASE;
        $base = substr($str, 0, strlen($str) - 14); //remove administrator
        $basepath = $base . "/" . $datadirectory;
        if (!file_exists($basepath)) { // create user directory
            ITModel::getITModel('common')->makeDir($basepath);
        }
        $isupload = false;
        $path = $basepath . '/products';
        if (!file_exists($path)) { // create user directory
            ITModel::getITModel('common')->makeDir($path);
        }
        $path = $path . '/product_' . $productid;
        if (!file_exists($path)) { // create user directory
            ITModel::getITModel('common')->makeDir($path);
        }
        if ($file['size'] > 0) {
            $file_name = $imageid . $file['name']; // file name
            $ext = $this->getITModel('common')->getExtension($file_name);
            $ext = strtolower($ext);
            if (($ext != "gif") && ($ext != "jpg") && ($ext != "jpeg") && ($ext != "png"))
                return 6; //file type mistmathc
            $userpath = $path;
            $isupload = true;
        }

        if ($isupload) {
            $fileOutOrg = $userpath . '/thumbnail';
            if (!file_exists($fileOutOrg)) { // create user directory
                ITModel::getITModel('common')->makeDir($fileOutOrg);
            }
            $fileOut = $fileOutOrg;
            $filenameuploaded = ITModel::getITModel('common')->createThumbnail(null,$file_name,$fileOut,500,500,$file,0);
            $orgname = $file_name;
            $file_name = "zoom-" . $orgname;
            ITModel::getITModel('common')->createThumbnail(null,$file_name,$fileOut,600,800,$file,0);
            $file_name = "medium-" . $orgname;
            ITModel::getITModel('common')->createThumbnail(null,$file_name,$fileOut,$configs['productimagesmallsizewidth'],$configs['productimagesmallsizeheight'],$file,0);
            $file_name = "/large-" . $orgname;
            ITModel::getITModel('common')->createThumbnail(null,$file_name,$fileOut,$configs['productimagelargesizewidth'],$configs['productimagelargesizeheight'],$file,1);
            return $filenameuploaded;
        }
        return false;
    }

    function removeProductImage($imageid, $productid) {
        if (!is_numeric($imageid))
            return false;
        if (!is_numeric($productid))
            return false;
        $configs = ITModel::getITModel('configuration')->getConfiginArray('default');
        $datadirectory = $configs['data_directory'];
        $str = JPATH_BASE;
        $base = substr($str, 0, strlen($str) - 14); //remove administrator
        $basepath = $base . "/" . $datadirectory;
        $row = $this->getTable('productimages');
        $row->load($imageid);
        $filelocation = $basepath . "/products/product_$productid/" . $row->image;
        if (file_exists($filelocation))
            unlink($filelocation);
        $filelocation = $basepath . "/products/product_$productid/thumbnail/medium-" . $row->image;
        if (file_exists($filelocation))
            unlink($filelocation);
        $filelocation = $basepath . "/products/product_$productid/thumbnail/large-" . $row->image;
        if (file_exists($filelocation))
            unlink($filelocation);
        $filelocation = $basepath . "/products/product_$productid/thumbnail/zoom-" . $row->image;
        if (file_exists($filelocation))
            unlink($filelocation);
        if (!$row->delete()) {
            return false;
        }
        return true;
    }

    function makeimagedefault($imageid, $productid){
        if (!is_numeric($imageid))
            return false;
        if (!is_numeric($productid))
            return false;
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(id) `#__isproductlisting_product_images` WHERE isdefault = 1 AND productid = " . $productid;
        $db->setQuery($query);
        $result = $db->loadResult();
        if($result == 0){
            $row = $this->getTable('productimages');
            $row->load($imageid);
            $row->isdefault = 1;
            if (!$row->store())
                return false;
        }
        return true;
    }

    function makeProductImageDefault($imageid, $productid) {
        if (!is_numeric($imageid))
            return false;
        if (!is_numeric($productid))
            return false;
        $db = JFactory::getDbo();
        $query = "UPDATE `#__isproductlisting_product_images` SET isdefault = 0 WHERE productid = " . $productid;
        $db->setQuery($query);
        if (!$db->query())
            return false;
        $row = $this->getTable('productimages');
        $row->load($imageid);
        $row->isdefault = 1;
        if (!$row->store())
            return false;
        return true;
    }

    function deleteProductImages($productid) {
        if (!is_numeric($productid))
            return false;
        $db = JFactory::getDbo();
        $query = "DELETE FROM `#__isproductlisting_product_images` WHERE productid = " . $productid;
        $db->setQuery($query);
        $db->query();
    }

}
