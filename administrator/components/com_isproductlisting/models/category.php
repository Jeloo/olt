<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelCategory extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function getAllCategories($searchtitle, $statusid, $limit, $limitstart) {  // get all categories
        $db = $this->getDBO();
        $result = array();
        $status [] = array('value' => '', 'text' => JText::_('STATUS'));
        $status [] = array('value' => 1, 'text' => JText::_('APPROVED'));
        $status [] = array('value' => -1, 'text' => JText::_('REJECTED'));

        $lists['title'] = $searchtitle;
        $lists['status'] = JHTML::_('select.genericList', $status, 'filter_statusid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $statusid);

        $wherequery = "";
        if ($searchtitle)
            $wherequery .= " AND category.title LIKE '%" . str_replace("'", "", $db->quote($searchtitle)) . "%'";
        if ($statusid)
            $wherequery .=" AND category.status = " . $statusid;

        $query = "SELECT COUNT(category.id) FROM #__isproductlisting_categories AS category WHERE category.status = category.status";
        $query .= $wherequery;
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total <= $limitstart)
            $limitstart = 0;
        $query = "SELECT category.* FROM `#__isproductlisting_categories` AS category WHERE category.status = category.status";
        $query .= $wherequery;
        $query .=" ORDER BY category.created DESC ";
        $db->setQuery($query, $limitstart, $limit);
        $result = $db->loadObjectList();
        $result[0] = $result;
        $result[1] = $total;
        $result[2] = $lists;
        return $result;
    }

    function changeStatusCategory($cid, $status) {
        if (is_array($cid) == false)
            return false;
        if (is_numeric($status) == false)
            return false;
        foreach ($cid AS $id) {
            $canchangestatus = ITModel::getITModel('common')->canPublishUnpublish($id, 'categories');
            if ($canchangestatus) {
                $row = $this->getTable('categories');
                $row->id = $id;
                $row->status = $status;
                if (!$row->store()) {
                    $this->setError($this->_db->getErrorMsg());
                    $returnvalue = 0;
                } else $returnvalue = 1;
            } else $returnvalue = 3;
        }
        return $returnvalue;
    }

    function getCategoryForForm($id) {// Category Form Data
        if (is_numeric($id) == false)
            return false;
        $db = $this->getDBO();
        $query = "SELECT category.* FROM `#__isproductlisting_categories` AS category WHERE category.id = " . $id;
        $db->setQuery($query);
        $vehicletype = $db->loadObject();
        $result[0] = $vehicletype;
        return $result;
    }

    function storeCategory() {//store Category
        $row = $this->getTable('categories');
        $result = array();
        $data = JRequest :: get('post');
        if ($data['id'] == '') { // only for new
            $result1 = $this->isCategoryExist($data['title']);
            if ($result1 == 1) {
                $result[0] = 3;
                return $result;
            }
            $data['created'] = date("Y-m-d H:i:s");
        }
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }

        if ($_FILES['image']['size'] > 0) {
            $imageresult = $this->uploadCategoryLogo($row->id);
            if ($imageresult != false) {
                $db = JFactory::getDbo();
                $query = "UPDATE `#__isproductlisting_categories` SET image = " . $db->quote($imageresult) . " WHERE id = " . $row->id;
                $db->setQuery($query);
                $db->query();
            }
        }
        $result[0] = true;
        $result[1] = $row->id;
        return $result;
    }

    function uploadCategoryLogo($id) {
        $configs = ITModel::getITModel('configuration')->getConfiginArray('default');
        $datadirectory = $configs['data_directory'];
        $str = JPATH_BASE;
        $base = substr($str, 0, strlen($str) - 14); //remove administrator
        $basepath = $base . "/" . $datadirectory;
        if (!file_exists($basepath)) { // create user directory
            ITModel::getITModel('common')->makeDir($basepath);
        }
        $isupload = false;
        $path = $basepath . '/categories';
        if (!file_exists($path)) { // create user directory
            ITModel::getITModel('common')->makeDir($path);
        }
        $path = $path . '/category_' . $id;
        if (!file_exists($path)) { // create user directory
            ITModel::getITModel('common')->makeDir($path);
        }
        if ($_FILES['image']['size'] > 0) {
            $file_name = $_FILES['image']['name']; // file name
            $ext = ITModel::getITModel('common')->getExtension($file_name);
            $ext = strtolower($ext);
            if (($ext != "gif") && ($ext != "jpg") && ($ext != "jpeg") && ($ext != "png"))
                return 6; //file type mistmathc
            $userpath = $path;
            $isupload = true;
        }

        if ($isupload) {
            $fileOut = $userpath . '/logo';
            if (!file_exists($fileOut)) { // create user directory
                ITModel::getITModel('common')->makeDir($fileOut);
            }
            $output = ITModel::getITModel('common')->createThumbnail('image',$file_name,$fileOut,300,300);
            return $output;
        }
        return false;
    }

    function isCategoryExist($title) { //is category exist
        $db = JFactory::getDBO();
        $query = "SELECT COUNT(category.id) FROM #__isproductlisting_categories AS category WHERE category.title = " . $db->Quote($title);
        $db->setQuery($query);
        $result = $db->loadResult();
        if ($result == 0)
            return 0;
        else
            return 1;
    }

    function deleteCategory() { //delete category
        $cids = JRequest :: getVar('cid', array(0), 'post', 'array');
        $row = $this->getTable('categories');
        $deleteall = 1;
        foreach ($cids as $cid) {
            if ($this->categoryCanDelete($cid) == true) {
                if (!$row->delete($cid)) {
                    $this->setError($row->getErrorMsg());
                    return false;
                }
            } else
                $deleteall++;
        }
        return $deleteall;
    }

    function categoryCanDelete($id) { //category CanDelete
        if (!is_numeric($id))
            return false;
        $db = $this->getDBO();
        $query = "SELECT
                    (SELECT COUNT(product.id) FROM `#__isproductlisting_products` AS product WHERE product.brandid = " . $id . ")
                    + (SELECT COUNT(id) FROM `#__isproductlisting_subcategories` WHERE categoryid = " . $id . ")
                    AS total";
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total > 0)
            return false;
        else
            return true;
    }

    function getAllCategoriesForCombo($title) {
        $categories[] = array('value' => '', 'text' => $title);
        $db = JFactory::getDbo();
        $query = "SELECT id,title FROM `#__isproductlisting_categories` WHERE status = 1";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        foreach ($result AS $res) {
            $categories[] = array('value' => $res->id, 'text' => $res->title);
        }
        return $categories;
    }

}
