<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelCommon extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function canMakeDefault($id, $tablename) {
        if (!is_numeric($id))
            return false;
        $tablename = '#__isproductlisting_' . $tablename;
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(id) FROM `" . $tablename . "` WHERE id = " . $id . " AND status = 1 ";
        $db->setQuery($query);
        $result = $db->loadResult();
        if ($result > 0)
            return true;
        else
            return false;
    }

    function canPublishUnpublish($id, $tablename) {
        if (!is_numeric($id))
            return false;
        $tablename = '#__isproductlisting_' . $tablename;
        $db = JFactory::getDbo();
        switch ($tablename) {
            case '#__isproductlisting_brands':
                $default = "";
                $wherequery = ' brandid = ' . $id;
                break;
            case '#__isproductlisting_categories':
                $default = "(SELECT COUNT(subcat.id) FROM `" . $tablename . "` AS category
                                JOIN `#__isproductlisting_subcategories` AS subcat ON subcat.categoryid = " . $id . "
                                WHERE category.id = " . $id . " )  + ";
                $wherequery = ' categoryid = ' . $id;
                break;
            case '#__isproductlisting_subcategories':
                $default = "";
                $wherequery = ' subcategoryid = ' . $id;
                break;
            case '#__isproductlisting_products':
                $default = "";
                $wherequery = ' id = ' . $id;
                break;
            case '#__isproductlisting_currency':
                $default = "";
                $wherequery = ' 1 = 2';
                break;
        }
        $query = "SELECT " . $default . " (SELECT COUNT(id) FROM `#__isproductlisting_products` WHERE ";
        $query .= $wherequery;
        $query .= " ) AS total";

        $db->setQuery($query);

        $result = $db->loadResult();

        if ($tablename == '#__isproductlisting_products')
            return ($result == 1) ? true : false;
        if ($result > 0)
            return false;
        else
            return true;
    }

    function createThumbnail($fieldname,$filename,$filedesitnation,$width,$height,$file = null,$clean = null) {
        require_once 'components/com_isproductlisting/include/lib/class.upload.php';
        if($fieldname != null)
            $handle = new upload($_FILES[$fieldname]);
        else
            $handle = new upload($file);
        if ($handle->uploaded) {
            $handle->file_new_name_body   = $filename;
            $handle->image_resize         = true;
            $handle->image_x              = $width;
            $handle->image_y              = $height;
            $handle->image_ratio_fill     = true;
            $handle->process($filedesitnation);
            if ($handle->processed) {
                if($fieldname != null || $clean == 1)
                    $handle->clean();
                return $handle->file_dst_name;
            } else {
                //echo 'error : ' . $handle->error;
                return false;
            }
        }
    }

    function makeDir($path) {
        if (!file_exists($path)) {
            mkdir($path, 0755);
            $ourFileName = $path . '/index.html';
            $ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
            fclose($ourFileHandle);
        }
        /* code for delete all files except in array
          else{
          $leave_files = array('index.html');
          foreach(glob("$path/*") as $file) {
          if( !in_array(basename($file), $leave_files) )
          unlink($file);
          }
          }
         */
    }

    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

}
