<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelProduct extends ITModel {

    function __construct() {
        parent :: __construct();
        $user = JFactory::getUser();
    }

    function getProductForForm($id) {// Product Form Data
        $categories = ITModel::getITModel('category')->getAllCategoriesForCombo(JText::_('CATEGORIES'));
        $subcategories = ITModel::getITModel('subcategory')->getAllSubCategoriesForCombo(JText::_('SUB_CATEGORIES'));
        $brands = ITModel::getITModel('brand')->getAllBrandsForCombo(JText::_('BRANDS'));
        $status = array(array('value' => '1', 'text' => JText::_('PUBLISHED')), array('value' => '2', 'text' => JText::_('UNPUBLISHED')));
        $lists['categoryid'] = JHTML::_('select.genericList', $categories, 'categoryid', 'class="inputbox" ' . 'onChange="getSubCategory(this);"', 'value', 'text', '');
        $lists['subcategoryid'] = JHTML::_('select.genericList', $subcategories, 'subcategoryid', 'class="inputbox" ' . '', 'value', 'text', '');
        $lists['brandid'] = JHTML::_('select.genericList', $brands, 'brandid', 'class="inputbox" ' . '', 'value', 'text', '');
        $lists['status'] = JHTML::_('select.genericList', $status, 'status', 'class="inputbox" ' . '', 'value', 'text', '');
        $product = null;
        if ($id) {
            if (!is_numeric($id))
                return false;
            $db = $this->getDBO();
            $query = "SELECT product.* FROM `#__isproductlisting_products` AS product WHERE product.id = " . $id;
            $db->setQuery($query);
            $product = $db->loadObject();
            $subcategories = ITModel::getITModel('subcategory')->getAllSubCategoriesForCombo(JText::_('SUB_CATEGORIES'), $product->categoryid);
            $lists['categoryid'] = JHTML::_('select.genericList', $categories, 'categoryid', 'class="inputbox" ' . 'onChange="getSubCategory(this);"', 'value', 'text', $product->categoryid);
            $lists['subcategoryid'] = JHTML::_('select.genericList', $subcategories, 'subcategoryid', 'class="inputbox" ' . '', 'value', 'text', $product->subcategoryid);
            $lists['brandid'] = JHTML::_('select.genericList', $brands, 'brandid', 'class="inputbox" ' . '', 'value', 'text', $product->brandid);
            $lists['status'] = JHTML::_('select.genericList', $status, 'status', 'class="inputbox" ' . '', 'value', 'text', $product->status);
        }

        $result[0] = $product;
        $result[1] = $lists;
        return $result;
    }

    function storeProduct() {//store Product
        $row = $this->getTable('product');
        $result = array();
        $data = JRequest :: get('post');
        if ($data['id'] == '') { // only for new
            $result1 = $this->isProductExist($data['title'], $data['categoryid'], $data['subcategoryid'], $data['brandid']);
            if ($result1 == 1) {
                $result[0] = 3;
                return $result;
            }
            $data['created'] = date("Y-m-d H:i:s");
        }
        $data['isgold'] = isset($data['isgold']) ? $data['isgold'] : 0;
        $data['isfeatured'] = isset($data['isfeatured']) ? $data['isfeatured'] : 0;
        for($i = 0;$i < count($data['field_title']);$i++){
            $field_title = $data['field_title'][$i];
            $field_value = $data['field_value'][$i];
            $data_sheet[] = array('title'=>$field_title,'value'=>$field_value);
        }
        $data['datasheet'] = json_encode($data_sheet);
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        $result[0] = true;
        $result[1] = $row->id;
        return $result;
    }

    function isProductExist($title, $categoryid, $subcategoryid, $brandid) { //is product exist
        $db = JFactory::getDBO();
        $query = "SELECT COUNT(product.id) FROM #__isproductlisting_products AS product WHERE product.title = " . $db->Quote($title);
        if ($categoryid)
            $wherequery = " AND product.categoryid = " . $categoryid;
        if ($subcategoryid)
            $wherequery = " AND product.subcategoryid = " . $subcategoryid;
        if ($brandid)
            $wherequery = " AND product.brandid = " . $brandid;
        $db->setQuery($query);
        $result = $db->loadResult();
        if ($result == 0)
            return 0;
        else
            return 1;
    }

    function deleteProduct() { //delete product
        $cids = JRequest :: getVar('cid', array(0), 'post', 'array');
        $row = $this->getTable('product');
        $deleteall = 1;
        foreach ($cids as $cid) {
            if ($this->productCanDelete($cid) == true) {
                if (!$row->delete($cid)) {
                    $this->setError($row->getErrorMsg());
                    return false;
                } else {
                    $this->deleteProductImages($cid);
                }
            } else
                $deleteall++;
        }
        return $deleteall;
    }

    function productCanDelete($id) { //product CanDelete
        if (!is_numeric($id))
            return false;
        $db = $this->getDBO();
        $query = "SELECT COUNT(product.id) FROM `#__isproductlisting_products` AS product WHERE product.id = " . $id;
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total != 1)
            return false;
        else
            return true;
    }

    function getAllProduct($searchtitle, $searchserialnumber, $searchcategoryid, $searchsubcategoryid, $searchbrandid, $statusid, $limitstart, $limit) {  // get all products
        $db = $this->getDBO();
        $result = array();
        $status [] = array('value' => '', 'text' => JText::_('STATUS'));
        $status [] = array('value' => 1, 'text' => JText::_('APPROVED'));
        $status [] = array('value' => -1, 'text' => JText::_('REJECTED'));
        $categories = ITModel::getITModel('category')->getAllCategoriesForCombo(JText::_('CATEGORIES'));
        $subcategories = ITModel::getITModel('subcategory')->getAllSubCategoriesForCombo(JText::_('SUB_CATEGORIES'));
        $brands = ITModel::getITModel('brand')->getAllBrandsForCombo(JText::_('BRANDS'));
        $lists['title'] = $searchtitle;
        $lists['status'] = JHTML::_('select.genericList', $status, 'filter_statusid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $statusid);
        $lists['categoryid'] = JHTML::_('select.genericList', $categories, 'filter_categoryid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $searchcategoryid);
        $lists['subcategoryid'] = JHTML::_('select.genericList', $subcategories, 'filter_subcategoryid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $searchsubcategoryid);
        $lists['brandid'] = JHTML::_('select.genericList', $brands, 'filter_brandid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $searchbrandid);

        $wherequery = "";
        if ($searchtitle)
            $wherequery .= " AND product.title LIKE '%" . str_replace("'", "", $db->quote($searchtitle)) . "%'";
        if ($searchserialnumber)
            $wherequery .= " AND product.serialnumber =  " . $db->quote($searchserialnumber);
        if ($searchcategoryid) {
            if (!is_numeric($searchcategoryid))
                return false;
            $wherequery .= " AND product.categoryid = " . $searchcategoryid;
        }
        if ($searchsubcategoryid) {
            if (!is_numeric($searchsubcategoryid))
                return false;
            $wherequery .= " AND product.subcategoryid = " . $searchsubcategoryid;
        }
        if ($statusid)
            $wherequery .=" AND product.status = " . $statusid;

        $query = "SELECT COUNT(product.id) FROM #__isproductlisting_products AS product WHERE product.status = product.status";
        $query .= $wherequery;

        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total <= $limitstart)
            $limitstart = 0;
        $query = "SELECT product.*,category.title AS cattitle, subcategory.title AS subcattitle, brand.title AS brandtitle
                    FROM `#__isproductlisting_products` AS product
                    LEFT JOIN `#__isproductlisting_categories` AS category ON category.id = product.categoryid   
                    LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid   
                    LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                    WHERE product.status = product.status";
        $query .= $wherequery;
        $query .=" ORDER BY product.created DESC ";
        $db->setQuery($query, $limitstart, $limit);
        $result = $db->loadObjectList();
        $result[0] = $result;
        $result[1] = $total;
        $result[2] = $lists;
        return $result;
    }

    function getSearchOptions() { //get search option
        $db = $this->getDBO();
        $categoryid = ITModel::getITModel('category')->getAllCategoriesForCombo(JText::_('ALL'));
        $subcategoryid = ITModel::getITModel('subcategory')->getAllSubCategoriesForCombo(JText::_('ALL'));
        $brandid = ITModel::getITModel('brand')->getAllBrandsForCombo(JText::_('ALL'));
        $status = array(
            '0' => array('value' => '', 'text' => JText::_('ALL')),
            '1' => array('value' => 1, 'text' => JText::_('PUBLISHED')),
            '2' => array('value' => 2, 'text' => JText::_('UNPUBLISHED')));

        $lists['categoryid'] = JHTML::_('select.genericList', $categoryid, 'categoryid', 'class="inputbox" ' . 'onChange="getSubCategory(this);"', 'value', 'text', '');
        $lists['subcategoryid'] = JHTML::_('select.genericList', $subcategoryid, 'subcategoryid', 'class="inputbox " ' . '', 'value', 'text', '');
        $lists['brandid'] = JHTML::_('select.genericList', $brandid, 'brandid', 'class="inputbox" ' . '', 'value', 'text', '');
        $lists['status'] = JHTML::_('select.genericlist', $status, 'status', 'class="inputbox" ' . '', 'value', 'text', '');
        $result[2] = $lists;
        return $result;
    }

    function getProductSearchResults($searchdata, $limitstart, $limit) { //get product search result
        $db = $this->getDBO();
        $wherequery = "";
        if ($searchdata['categoryid'] != '')
            if (is_numeric($searchdata['categoryid']) == false)
                return false;
            else
                $wherequery .= " AND categoryid = " . $searchdata['categoryid'];
        if ($searchdata['subcategoryid'] != '')
            if (is_numeric($searchdata['subcategoryid']) == false)
                return false;
            else
                $wherequery .= " AND subcategoryid = " . $searchdata['subcategoryid'];
        if ($searchdata['brandid'] != '')
            if (is_numeric($searchdata['brandid']) == false)
                return false;
            else
                $wherequery .= " AND brandid = " . $searchdata['brandid'];
        if ($searchdata['price'] != '')
            if (is_numeric($searchdata['price']) == false)
                return false;
            else
                $wherequery .= " AND price = " . $searchdata['price'];
        if ($searchdata['serialnumber'] != '')
            $wherequery .= " AND serialnumber = " . $db->quote($searchdata['price']);
        if ($searchdata['howtouse'] != '')
            $wherequery .= " AND howtouse LIKE '%" . str_replace("'", "", $db->quote($searchdata['howtouse'])) . "%'";
        if ($searchdata['shortsummary'] != '')
            $wherequery .= " AND shortsummary LIKE '%" . str_replace("'", "", $db->quote($searchdata['shortsummary'])) . "%'";
        if ($searchdata['ingredients'] != '')
            $wherequery .= " AND ingredients LIKE '%" . str_replace("'", "", $db->quote($searchdata['ingredients'])) . "%'";
        if ($searchdata['productexpirydate'] != '')
            $wherequery .= " AND productexpirydate = " . $db->quote($searchdata['productexpirydate']);
        if ($searchdata['listproductexpirydate'] != '')
            $wherequery .= " AND listproductexpirydate = " . $db->quote($searchdata['listproductexpirydate']);
        if ($searchdata['isgold'] != '')
            $wherequery .= " AND isgold = " . $db->quote($searchdata['isgold']);
        if ($searchdata['status'] != '')
            $wherequery .= " AND isfeatured = " . $db->quote($searchdata['isfeatured']);
        if ($searchdata['status'] != '')
            $wherequery .= " AND status = " . $db->quote($searchdata['status']);
        if ($searchdata['created'] != '')
            $wherequery .= " AND created = " . $db->quote($searchdata['created']);

        $query = "SELECT count(product.id) FROM `#__isproductlisting_products` AS product
                WHERE product.status = product.status ";
        $query .= $wherequery;
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total <= $limitstart)
            $limitstart = 0;

        $query = "SELECT product.*,category.title as cattitle, subcategory.title as subcattitle, brand.title as brandtitle
                    FROM `#__isproductlisting_products` AS product
                    LEFT JOIN `#__isproductlisting_categories` AS category ON category.id= product.categoryid
                    LEFT JOIN `#__isproductlisting_subcategories` AS subcategory ON subcategory.id = product.subcategoryid
                    LEFT JOIN `#__isproductlisting_brands` AS brand ON brand.id = product.brandid
                    WHERE product.status = product.status ";
        $query .= $wherequery;

        $db->setQuery($query, $limitstart, $limit);
        $products = $db->loadObjectList();
        $result[0] = $products;
        $result[1] = $total;

        return $result;
    }

    function changeStatusProduct($cid, $status) {            //changeStatusProduct
        if (is_array($cid) == false)
            return false;
        if (is_numeric($status) == false)
            return false;
        foreach ($cid AS $id) {
            $canchangestatus = ITModel::getITModel("common")->canPublishUnpublish($id, 'products');
            if ($canchangestatus) {
                $row = $this->getTable('product');
                $row->id = $id;
                $row->status = $status;
                if (!$row->store()) {
                    $this->setError($this->_db->getErrorMsg());
                    $returnvalue = 0;
                } else
                    $returnvalue = 1;
            } else
                $returnvalue = 3;
        }
        return $returnvalue;
    }

}
?>

