<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ISProductlistingModelBrand extends ITModel {

    function __construct() {
        parent :: __construct();
    }

    function getBrandForForm($id) {// Brand Form Data
        if (is_numeric($id) == false)
            return false;
        $db = $this->getDBO();
        $query = "SELECT brand.* FROM `#__isproductlisting_brands` AS brand WHERE brand.id = " . $id;
        $db->setQuery($query);
        $vehicletype = $db->loadObject();
        $result[0] = $vehicletype;
        return $result;
    }

    function getAllBrands($searchtitle, $statusid, $limit, $limitstart) {  // get all brands
        $db = $this->getDBO();
        $result = array();
        $status [] = array('value' => '', 'text' => JText::_('STATUS'));
        $status [] = array('value' => 1, 'text' => JText::_('APPROVED'));
        $status [] = array('value' => -1, 'text' => JText::_('REJECTED'));

        $lists['title'] = $searchtitle;
        $lists['status'] = JHTML::_('select.genericList', $status, 'filter_statusid', 'class="inputbox" ' . 'onChange="this.form.submit();"', 'value', 'text', $statusid);

        $wherequery = "";
        if ($searchtitle)
            $wherequery .= " AND brand.title LIKE '%" . str_replace("'", "", $db->quote($searchtitle)) . "%'";
        if ($statusid)
            $wherequery .=" AND brand.status = " . $statusid;

        $query = "SELECT COUNT(brand.id) FROM #__isproductlisting_brands AS brand WHERE brand.status = brand.status ";
        $query .= $wherequery;

        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total <= $limitstart)
            $limitstart = 0;
        $query = "SELECT brand.* FROM `#__isproductlisting_brands` AS brand WHERE brand.status = brand.status";
        $query .= $wherequery;
        $query .=" ORDER BY brand.created DESC ";
        $db->setQuery($query, $limitstart, $limit);
        $result = $db->loadObjectList();
        $result[0] = $result;
        $result[1] = $total;
        $result[2] = $lists;
        return $result;
    }

    function changeStatusBrand($cid, $status) {
        if (is_array($cid) == false)
            return false;
        if (is_numeric($status) == false)
            return false;
        foreach ($cid AS $id) {
            $canchangestatus = ITModel::getITModel('common')->canPublishUnpublish($id, 'brands');
            if ($canchangestatus) {
                $row = $this->getTable('brands');
                $row->id = $id;
                $row->status = $status;
                if (!$row->store()) {
                    $this->setError($this->_db->getErrorMsg());
                    $returnvalue = 0;
                } else
                    $returnvalue = 1;
            } else
                $returnvalue = 3;
        }
        return $returnvalue;
    }

    function getAllBrandsForCombo($title) {
        $brands[] = array('value' => '', 'text' => $title);
        $db = JFactory::getDbo();
        $query = "SELECT id,title FROM `#__isproductlisting_brands` WHERE status = 1";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        foreach ($result AS $res) {
            $brands[] = array('value' => $res->id, 'text' => $res->title);
        }
        return $brands;
    }

    function isBrandExist($title) { //is brand exist
        $db = JFactory::getDBO();
        $query = "SELECT COUNT(brand.id) FROM #__isproductlisting_brands AS brand WHERE brand.title = " . $db->Quote($title);
        $db->setQuery($query);
        $result = $db->loadResult();
        if ($result == 0)
            return 0;
        else
            return 1;
    }

    function storeBrand() {//store Brand
        $row = $this->getTable('brands');
        $result = array();
        $data = JRequest :: get('post');
        if ($data['id'] == '') { // only for new
            $result1 = $this->isBrandExist($data['title']);
            if ($result1 == 1) {
                $result[0] = 3;
                return $result;
            }
            $data['created'] = date("Y-m-d H:i:s");
        }
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            $result[0] = false;
            return $result;
        }

        if ($_FILES['image']['size'] > 0) {
            $imageresult = $this->uploadBrandLogo($row->id);
            if ($imageresult != false) {
                $db = JFactory::getDbo();
                $query = "UPDATE `#__isproductlisting_brands` SET image = " . $db->quote($imageresult) . " WHERE id = " . $row->id;
                $db->setQuery($query);
                $db->query();
            }
        }
        $result[0] = true;
        $result[1] = $row->id;
        return $result;
    }

    function uploadBrandLogo($id) {
        $configs = ITModel::getITModel('configuration')->getConfiginArray('default');
        $datadirectory = $configs['data_directory'];
        $str = JPATH_BASE;
        $base = substr($str, 0, strlen($str) - 14); //remove administrator
        $basepath = $base . "/" . $datadirectory;
        if (!file_exists($basepath)) { // create user directory
            ITModel::getITModel('common')->makeDir($basepath);
        }
        $isupload = false;
        $path = $basepath . '/brands';
        if (!file_exists($path)) { // create user directory
            ITModel::getITModel('common')->makeDir($path);
        }
        $path = $path . '/brand_' . $id;
        if (!file_exists($path)) { // create user directory
            ITModel::getITModel('common')->makeDir($path);
        }
        if ($_FILES['image']['size'] > 0) {
            $file_name = $_FILES['image']['name']; // file name
            $ext = ITModel::getITModel('common')->getExtension($file_name);
            $ext = strtolower($ext);
            if (($ext != "gif") && ($ext != "jpg") && ($ext != "jpeg") && ($ext != "png"))
                return 6; //file type mistmathc
            $userpath = $path;
            $isupload = true;
        }

        if ($isupload) {
            $fileOut = $userpath . '/logo';
            if (!file_exists($fileOut)) { // create user directory
                ITModel::getITModel('common')->makeDir($fileOut);
            }
            $output = ITModel::getITModel('common')->createThumbnail('image',$file_name,$fileOut,300,300);
            return $output;
        }
        return false;
    }

    function deleteBrand() { //delete brand
        $cids = JRequest :: getVar('cid', array(0), 'post', 'array');
        $row = $this->getTable('brands');
        $deleteall = 1;
        foreach ($cids as $cid) {
            if ($this->brandCanDelete($cid) == true) {
                if (!$row->delete($cid)) {
                    $this->setError($row->getErrorMsg());
                    return false;
                }
            } else
                $deleteall++;
        }
        return $deleteall;
    }

    function brandCanDelete($id) { //brand CanDelete
        if (!is_numeric($id))
            return false;
        $db = $this->getDBO();
        $query = "SELECT COUNT(product.id) AS total FROM `#__isproductlisting_products` AS product WHERE product.brandid = " . $id;
        $db->setQuery($query);
        $total = $db->loadResult();
        if ($total > 0)
            return false;
        else
            return true;
    }

}
