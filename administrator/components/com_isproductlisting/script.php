<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');

class com_ISProductlistingInstallerScript
{
	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent) 
	{
		// $parent is the class calling this method
		//$parent->getParent()->setRedirectURL('index.php?option=com_jsdocumentation');
	}

	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
		// $parent is the class calling this method
		echo '<p>' . JText::_('ISPRODUCTLISTING_UNINSTALL_TEXT') . '</p>';
	}

	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent) 
	{
		// $parent is the class calling this method
		
	}

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
	}
	private function updateISProductlisting()
	{
	}

	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		
		jimport('joomla.installer.helper');
		$installer = new JInstaller();
		
		echo "<br /><br /><font color='green'><strong>Installing modules</strong></font>";
		$ext_module_path = JPATH_ADMINISTRATOR.'/components/com_isproductlisting/extensions/modules/';
		$extensions = array( 'mod_isproductlistingslideshow.zip'=>'Slide Show of Product Listing',
			'mod_hotproducts.zip'=>'Hot Products Listing Module',
			'mod_productcategories.zip'=>'Product Categories Listing Module',
			'mod_productbrands.zip'=>'Product Brands Module'
		 );
			 
		 foreach( $extensions as $ext => $extname ){
			  $package = JInstallerHelper::unpack( $ext_module_path.$ext );
			  if( $installer->install( $package['dir'] ) ){
				echo "<br /><font color='green'>$extname successfully installed.</font>";
			  }else{
				echo "<br /><font color='red'>ERROR: Could not install the $extname. Please install manually.</font>";
			  }
			JInstallerHelper::cleanupInstall( $ext_module_path.$ext, $package['dir'] ); 
		}

		echo "<br /><br /><font color='green'><strong>Installing plugins</strong></font>";
		$ext_plugin_path = JPATH_ADMINISTRATOR.'/components/com_isproductlisting/extensions/plugins/';
		$extensions = array( 'plg_productcategories.zip'=>'Product Categories Plugin',
			'plg_productbrands.zip'=>'Product Brands Plugin'
		 );
		   
		 foreach( $extensions as $ext => $extname ){
			  $package = JInstallerHelper::unpack( $ext_plugin_path.$ext );
			  if( $installer->install( $package['dir'] ) ){
				echo "<br /><font color='green'>$extname successfully installed.</font>";
			  }else{
				echo "<br /><font color='red'>ERROR: Could not install the $extname. Please install manually.</font>";
			  }
			JInstallerHelper::cleanupInstall( $ext_plugin_path.$ext, $package['dir'] ); 
		}
	}
}

