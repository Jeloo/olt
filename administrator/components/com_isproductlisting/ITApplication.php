<?php

/*
  @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
  @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  Company:		IT Fant
  Contact:		itfant.com , info@itfant.com
  Created on:	January, 2014
  Project: 		IS Product listing
 */
defined('_JEXEC') or die('Restricted access');
if (!defined('JVERSION')) {
    $jversion = new JVersion;
    $jversion = $jversion->getShortVersion();
    $jversion = substr($jversion, 0, 3);
    define('JVERSION', $jversion);
}

if (JVERSION < 3) {
jimport('joomla.application.component.controller');
jimport('joomla.application.component.model');
jimport('joomla.application.component.view');

    abstract class ITController extends JController {

        function __construct($config = array()) {
            parent::__construct($config);
        }

    }

    abstract class ITModel extends JModel {

        function __construct($config = array()) {
            parent::__construct($config);
        }

        static function getITModel($model) {
            require_once 'components/com_isproductlisting/models/' . $model . '.php';
            $model = 'ISProductListingModel' . ucfirst($model);
            $object = new $model;
            return $object;
        }

    }

    abstract class ITView extends JView {

        function __construct($config = array()) {
            parent::__construct($config);
        }

        static function getITModel($model) {
            require_once 'components/com_isproductlisting/models/' . $model . '.php';
            $model = 'ISProductListingModel' . ucfirst($model);
            $object = new $model;
            return $object;
        }

    }

} else {

    abstract class ITController extends JControllerLegacy {

        function __construct($config = array()) {
            parent::__construct($config);
        }

    }

    abstract class ITModel extends JModelLegacy {

        function __construct($config = array()) {
            parent::__construct($config);
        }

        static function getITModel($model) {
            require_once 'components/com_isproductlisting/models/' . $model . '.php';
            $model = 'ISProductListingModel' . ucfirst($model);
            $object = new $model;
            return $object;
        }

    }

    abstract class ITView extends JViewLegacy {

        function __construct($config = array()) {
            parent::__construct($config);
        }

        static function getITModel($model) {
            require_once 'components/com_isproductlisting/models/' . $model . '.php';
            $model = 'ISProductListingModel' . ucfirst($model);
            $object = new $model;
            return $object;
        }

    }

}
?>