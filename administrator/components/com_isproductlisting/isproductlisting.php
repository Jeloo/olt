<?php

/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
require_once (JPATH_COMPONENT . '/ITApplication.php');
require_once (JPATH_COMPONENT . '/controller.php');
$task = JRequest::getVar('task');
$c = '';
if (strstr($task, '.')) {
    $array = explode('.', $task);
    $c = $array[0];
    $task = $array[1];
} else {
    $task = JRequest::getVar('task', 'display');
    $c = JRequest::getVar('c', 'isproductlisting');
}
if ($c != '') {
    $path = JPATH_COMPONENT . '/controllers/' . $c . '.php';
    jimport('joomla.filesystem.file');
    if (JFile :: exists($path)) {
        require_once ($path);
    } else {
        JError :: raiseError('500', JText :: _('Unknown controller: <br>' . $c . ':' . $path));
    }
}
$c = 'ISProductlistingController'.  ucfirst($c);
$controller = new $c ();
$controller->execute($task);
$controller->redirect();
?>