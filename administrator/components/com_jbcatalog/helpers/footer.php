<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die;


class FooterCHelper
{
    public static function footHTML(){
        ?>
        <div style="clear:both; margin:20px; text-align:right;">
            Post a rating and a review at the Joomla! Extensions Directory 
            <a style="margin-right: 15px;" href="http://extensions.joomla.org/extensions/directory-a-documentation/directory/26397" target="_blank"><img src="components/com_jbcatalog/images/joomla.png" /></a>
            Follow us on Twitter 
            <a style="margin-right: 15px;" href="https://twitter.com/beardev" target="_blank"><img src="components/com_jbcatalog/images/twitter.png" style="height:21px;" /></a>
            Become a Fun on Facebook 
            <a href="https://www.facebook.com/pages/BearDev/130697180026" target="_blank"><img src="components/com_jbcatalog/images/facebook.jpg" style="height:21px;" /></a>
        </div>
        <?php
    }
}