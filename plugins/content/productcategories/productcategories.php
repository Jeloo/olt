<?php
/**
 * @Copyright Copyright (C) 2014 ... Shoaib Rehmat Ali
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		IT Fant
  + Contact:		itfant.com , info@itfant.com
 * Created on:	January, 2014
  ^
  + Project: 		IS Product listing
  ^
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

//The Content plugin Loadmodule
class plgContentproductcategories extends JPlugin {


		public function onPrepareContent( &$row, &$params, $page=0 )
        {
                if ( JString::strpos( $row->text, 'productcategories' ) === false ) {
                        return true;
                }

              // expression to search for
                $regex = '/{productcategories\s*.*?}/i';
                if ( !$this->params->get( 'enabled', 1 ) ) {
                        $row->text = preg_replace( $regex, '', $row->text );
                        return true;
                }
                preg_match_all( $regex, $row->text, $matches );
                $count = count( $matches[0] );
                if ( $count ) {
                        // Get plugin parameters
                        $style = $this->params->def( 'style', -2 );
                        $this->_process( $row, $matches, $count, $regex, $style );
                }
        }
          //joomla 1.6
    public function onContentPrepare($context, &$row, &$params, $page=0) {
        if (JString::strpos($row->text, 'productcategories') === false) {
            return true;
        }

        // expression to search for
        $regex = '/{productcategories\s*.*?}/i';
        if (!$this->params->get('enabled', 1)) {
            $row->text = preg_replace($regex, '', $row->text);
            return true;
        }
        preg_match_all($regex, $row->text, $matches);
        $count = count($matches[0]);
        if ($count) {
            // Get plugin parameters
            $style = $this->params->def('style', -2);
            $this->_process($row, $matches, $count, $regex, $style);
        }
    }

    protected function _process(&$row, &$matches, $count, $regex, $style) {
        for ($i = 0; $i < $count; $i++) {
            $load = str_replace('productcategories', '', $matches[0][$i]);
            $load = str_replace('{', '', $load);
            $load = str_replace('}', '', $load);
            $load = trim($load);
            $modules = $this->_load($load, $style);
            $row->text = preg_replace('{' . $matches[0][$i] . '}', $modules, $row->text);
        }
        $row->text = preg_replace($regex, '', $row->text);
    }

    protected function _load($position, $style=-2) {
		$title = $this->params->get('title');
		$showtitle = $this->params->get('showtitle');
		$listingstyle = $this->params->get('listingstyle');
		$showcategoryhaveproduct = $this->params->get('showcategoryhaveproduct');
		$defaultheight = $this->params->get('defaultheight');
		$moduleclass_sfx = $this->params->get('moduleclass_sfx');

		$sliding= $this->params->get('sliding','1');
		$consecutivesliding= $this->params->get('consecutivesliding','3');
		$noofrecord= $this->params->get('noofrecord');
		if($noofrecord>100) $noofrecord=100;
		$itemid = $this->params->get('itemid');
		if($itemid) $itemid= $this->params->get('itemid');
		else $itemid =  JRequest::getVar('Itemid');

		$curdate = date('Y-m-d H:i:s');


		$componentAdminPath = JPATH_ADMINISTRATOR . '/components/com_isproductlisting';
		$componentPath =  'components/com_isproductlisting';
		$trclass=array('odd','even');

		require_once $componentPath . '/ITApplication.php';
		require_once $componentPath . '/models/modplug.php';
		$model = new ISProductlistingModelModplug();
		$config = $model->getConfig('default');

		$lang = JFactory :: getLanguage();
		$lang->load('com_isproductlisting');
		$categories = $model->mpGetCategories($showcategoryhaveproduct,$noofrecord);
		$config = $model->getConfig('default');
		$document = JFactory::getDocument();
		$document->addStyleSheet('components/com_isproductlisting/css/default.css');
		$document->addStyleDeclaration(ITModel::getITModel('theme')->getThemeColor());
		$document->addScript('administrator/components/com_isproductlisting/include/js/jquery.js');
		$finalcontent = "";
		if ($categories){ 
			   $contents = '<div id="is_modWrapperDiv" class="is_modWrapperDiv productbrand" style="height:'.$defaultheight.'px;">';
				$top="";
				if ($showtitle == 1){
							$divclass = (!empty($moduleclass_sfx) || $moduleclass_sfx != '') ? "class=\"$moduleclass_sfx\"":"id=\"is_modheadingWrapper\" class=\"is_modheadingWrapper\"";
							$top .= '<div '.$divclass.' >
										<h3>
											<span id="is_modtitle">'.$title.'</span>
										</h3>
									</div>';
				}	
				$i=1;
                $datacontent = '<div id="is_moddatadiv" class="productcategory">';
                foreach ($categories as $category) {
                    $datacontent .= " <span class=\"is_modnumberlistspan\" ><a href=\"index.php?option=com_isproductlisting&view=product&layout=listproduct&cl=2&catid=$category->categoryid&Itemid=$itemid\">$category->categorytitle</a><span class=\"is_modnumberspan\" >$category->totalproduct</span></span>";
                }
				$datacontent .= '</div>';
				if ($sliding == 1) {
					if($listingstyle==2){
						$tcontents = '<table cellpadding="0" cellspacing="0" border="1" width="100%" id="is_modTable" class="is_modTable"> <tr>';
						$scontents="";
						for ($a = 0; $a < $consecutivesliding; $a++){
							$scontents .= '<td>'.$datacontent.'</td>';
						}                                                                            
						$datacontent = $tcontents.$scontents.'</tr></table>';
						$contents .= $top .'<marquee id="is_marqueemodproductcategory" style="height:'.$defaultheight.'px;" direction="left"  scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">' .$scontents . '</marquee></div>';
						$finalcontent = $contents;
					}elseif($listingstyle==1){
						$scontents = "";
						for ($a = 0; $a < $consecutivesliding; $a++){
								$scontents .= $datacontent;
						}
						$contents .= $top . '<marquee id="is_marqueemodproductcategory" style="height:'.$defaultheight.'px;" direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">'.$scontents.'</marquee></div>';
						$finalcontent = $contents;
					}
				}elseif($sliding == 0){
						$finalcontent = $contents.$top.$datacontent."</div>";
				}
				$finalcontent .=  '
				<script>
					jQuery(document).ready(function(){
						var div = jQuery("div#is_modWrapperDiv.productbrand div");
						var marqueeHeight = jQuery("marquee#is_marqueemodproductcategory").height() - div.height();
						if(jQuery.isNumeric(marqueeHeight))
							jQuery("div#is_modWrapperDiv marquee#is_marqueemodproductcategory").css({height:marqueeHeight});
					});
				</script>';
		} 
		return $finalcontent;
	}

} ?>


